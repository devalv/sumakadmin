import React, { Component } from 'react';
import '../utils/Style.css';
import PageTitle from './PageTitle';
import CircularProgress from 'material-ui/CircularProgress';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import IconButton from 'material-ui/IconButton';
import Dialogo from '../utils/Dialogo';
import {Table,TableBody,TableHeader,TableHeaderColumn,TableRow,TableRowColumn} from 'material-ui/Table';
import {campeonatosRef, formatDate, isNumber} from '../utils/Controller';
import {green400} from 'material-ui/styles/colors'
import {Toolbar, ToolbarGroup,  ToolbarTitle} from 'material-ui/Toolbar';
import 'react-table/react-table.css';
import ReactTable from "react-table";
import {DatePicker} from 'material-ui';

export default class Jornadas extends Component {
    constructor(props){
        super(props);
        this.state={
            jornadas: [],
            current:null,
            isEditing:false,
            currentNumero:0,
            currentDesde:'',
            currentHasta:'',
            key:null,
            addingNewone:false,
            title:"Jornadas",
            isLoading:true,
            authorized: false,
        };
    }

    componentWillMount() {
        this.startOver();
    }
    
    startOver = () => {
        const rol = sessionStorage.getItem('rol');
        if(rol === 'sancionador' || rol === 'superadmin' || rol === 'admin') {
            const keyCampeonato = this.props.match.params.keyCampeonato;
            if(keyCampeonato) {
                campeonatosRef.child(keyCampeonato+'/jornadas').on('value', snapshot=>{
                    if (snapshot.toJSON()) {
                        const jornadas = Object.values(snapshot.toJSON());
                        if (jornadas) {
                            this.setState({jornadas, isLoading: false, authorized: true});
                        }
                    } else {
                        this.setState({jornadas: [], isLoading: false, authorized: true})
                    }
                })
            }
        }
    }
   
    back = () => {
        this.setState({
            jornadas: [],
            current:null,
            isEditing:false,
            currentNumero:0,
            currentDesde:'',
            currentHasta:'',
            key:null,
            addingNewone:false,
            title:"Jornadas",
            isLoading:true,
            authorized: false,
        });
        this.startOver();
    }
    
    handleEdit = (current,key) =>{
        this.setState({
            current,
            isEditing:true,
            key,
            title:"Editar jornadas",
            currentNumero:current.numero?current.numero:0,
            currentDesde:current.desde?new Date(current.desde):formatDate(new Date()),
            currentHasta:current.hasta?new Date(current.hasta):formatDate(new Date())
        })
    }
    deleteCurrent = () =>{
        const {key} = this.state;
        const keyCampeonato = this.props.match.params.keyCampeonato;
        if(keyCampeonato && key) {
            campeonatosRef.child(keyCampeonato+'/jornadas/'+key +'/').remove();
            this.back();
            this.startOver();
        }
    }
    handleChangeJornada = (e) => {
        const currentNumero = e.target.value;
        if (isNumber(currentNumero)) { 
            this.setState({currentNumero}); 
        }
    }
    handleEditCurrent = () =>{
        this.setState({isLoading:true});
        const keyCampeonato = this.props.match.params.keyCampeonato;
        const {jornadas} = this.state;
        const exists = jornadas.filter(obj => this.state.currentNumero.toString() === obj.numero.toString())[0];
        const desde = this.state.currentDesde.getFullYear() + "/" + (this.state.currentDesde.getMonth() + 1) + "/" + this.state.currentDesde.getDate()
        const hasta = this.state.currentHasta.getFullYear() + "/" + (this.state.currentHasta.getMonth() + 1) + "/" + this.state.currentHasta.getDate()
        if(keyCampeonato) {
            if(this.state.addingNewone===true){
                const key = campeonatosRef.child(keyCampeonato+'/jornadas/').push().key;
                let newJornada = {
                    numero: this.state.currentNumero,
                    desde,
                    hasta,
                    key
                }
                if (!exists) {
                    campeonatosRef.child(keyCampeonato+'/jornadas/'+key).set(newJornada);
                } else {
                    alert('Error, el numero de jornada ingresado ya existe.')
                }
            }
            else{
                const {key} = this.state;
                let editingJornada = {
                    numero:this.state.currentNumero,
                    desde,
                    hasta,
                    key
                };
                if(!exists) {
                    campeonatosRef.child(keyCampeonato+'/jornadas/'+key).set(editingJornada);
                } else {
                    if (exists.key === key) {
                        campeonatosRef.child(keyCampeonato+'/jornadas/'+key).set(editingJornada);
                    } else {
                        alert('Error, el numero de jornada ingresado ya existe.')
                    }
                }
            }
            this.back();
            this.startOver();
        }
    }
    addNew = () =>{
        this.setState({isEditing:true,addingNewone:true});
    }

    render() {
        const {jornadas, isLoading, authorized} = this.state;
        let dialogButton = 
            this.state.currentNumero !== '' ? 
                this.state.currentDesde !== '' ? 
                    this.state.currentHasta !== ''? 
                        <Dialogo btnLabel="Guardar" btnType="raised"  dialogText = "Está seguro que desea guardar esta información?" confirm={this.handleEditCurrent} />:'':'':'';
        let dialogButtonDelete = <Dialogo btnLabel="Eliminar" deleteButton={true} dialogText = "Está seguro que desea eliminar esta información?" confirm={this.deleteCurrent} />
        return isLoading === true ? <div className="content"><CircularProgress color={green400} size={100} thickness={7} /></div> :
         authorized === false ?  
            <div>No tiene permiso para acceder a este módulo</div>
        :(
            <div className="App">
                <PageTitle 
                    title="Tu Fútbol"
                    isLoggedIn={true}
                    hasSettingsMenu ={false}
                />
                <br />
                <div style={{width: '100%'}}>
                    <Paper className="paperCanchas" zDepth={2} style={{width: '100%'}}>
                    <Toolbar style={{width: '100%',  maxWidth: '100%'}}>
                        <ToolbarGroup>
                        {
                            this.state.isEditing === true ?
                            <IconButton iconClassName="material-icons" style={{textAlign:'left',color:'#000'}} tooltip="Volver" onClick={this.back}>arrow_back_ios</IconButton>
                            :
                            <IconButton style={{textAlign:'left',color:'#000'}} tooltip="Atras" iconClassName="material-icons" onClick={()=>window.history.back()}>arrow_back_ios</IconButton>
                        }
                            <ToolbarTitle text={this.state.title} style={{color:'#000'}} className="toolbarTitle"/>
                        </ToolbarGroup>
                        {
                            this.state.isEditing === false ?
                                <IconButton iconClassName="material-icons" style={{textAlign:'right',color:'#000'}} tooltip="Agregar" onClick={this.addNew}>add</IconButton>
                            :
                                <div>
                                {
                                    this.state.current!==null && dialogButtonDelete
                                }
                                </div>
                        }
                    </Toolbar>
                    <br />
                    {
                        this.state.isEditing === false ?
                            <ReactTable 
                                data={jornadas}
                                defaultPageSize={10}
                                previousText= 'Anterior'
                                nextText= 'Siguiente'
                                loadingText= 'Cargando...'
                                noDataText= 'No hay datos'
                                pageText= 'Página'
                                ofText= 'de'
                                rowsText= 'Filas'
                                pageJumpText= 'Ir a página'
                                rowsSelectorText= 'filas por página'
                                filterable 
                                defaultFilterMethod={(filter, row) =>String(row[filter.id]).toLowerCase().includes( filter.value.toLowerCase())}
                                columns={[
                                {
                                    columns: [
                                    {
                                        Header: "Jornada",
                                        accessor: "numero",
                                        width: 120,
                                    },
                                    {
                                        Header: "Desde",
                                        accessor: "desde"
                                    },
                                    {
                                        Header: "Hasta",
                                        accessor: "hasta"
                                    },
                                    {
                                        Header:'Detalle',
                                        accessor:'key',
                                        width:85,
                                        Cell: row => (
                                            <IconButton iconClassName="material-icons" onClick={()=>this.handleEdit(row.original,row.original.key)}>edit</IconButton>
                                        )
                                    }
                                ]
                                }
                                ]}
                                className={"-striped -highlight table"}
                            />
                        :
                        <div>
                        <Table bodyStyle={{overflow:'visible'}}  selectable={false}>
                            <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
                                <TableRow>
                                    <TableHeaderColumn className="smallWidth">Info</TableHeaderColumn>
                                    <TableHeaderColumn>Dato</TableHeaderColumn>
                                </TableRow>
                            </TableHeader>
                            <TableBody displayRowCheckbox={false}>
                                <TableRow >
                                    <TableRowColumn className="smallWidth">Número</TableRowColumn>
                                    <TableRowColumn>
                                        <TextField
                                            hintText="Número"
                                            fullWidth={true}
                                            value={this.state.currentNumero}
                                            onChange={this.handleChangeJornada}
                                        />
                                    </TableRowColumn>
                                </TableRow>
                                <TableRow >
                                    <TableRowColumn className="smallWidth">Desde</TableRowColumn>
                                    <TableRowColumn>
                                        <DatePicker 
                                            hintText="Fecha Desde" 
                                            container="inline"
                                            fullWidth={true}
                                            value={this.state.currentDesde}
                                            onChange={(e, currentDesde) => this.setState({currentDesde})}
                                        />
                                    </TableRowColumn>
                                </TableRow>
                                <TableRow >
                                    <TableRowColumn className="smallWidth">Hasta</TableRowColumn>
                                    <TableRowColumn>
                                        <DatePicker 
                                            hintText="Fecha Hasta" 
                                            container="inline"
                                            fullWidth={true}
                                            value={this.state.currentHasta}
                                            onChange={(e, currentHasta) => this.setState({currentHasta})}
                                        />
                                    </TableRowColumn>
                                </TableRow>
                            </TableBody>
                        </Table>
                        {dialogButton}
                        </div>
                    }
                    </Paper>
                </div>
            </div>
        )
    }
}
