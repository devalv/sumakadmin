import React, { Component } from 'react';
import AppBar from 'material-ui/AppBar';
import FlatButton  from 'material-ui/FlatButton';
import FontIcon from 'material-ui/FontIcon';
import Exit from 'material-ui/svg-icons/action/exit-to-app';
import {logout} from '../utils/Firebase';
import {logRef} from '../utils/Controller';
import {Link} from 'react-router-dom';
import moment from 'moment';
import { green400 } from 'material-ui/styles/colors';

export default class PageTitle extends Component {
    constructor(props){
        super(props);
        this.state={

        };
    }
    logUserOut = () =>{
      const email = sessionStorage.getItem('e');
      const uid = sessionStorage.getItem('uid');
      const dateTime = moment().format("YYYY-MM-DD HH:mm:ss");
      const user = {
          email,
          uid,
          description: 'logged out'
      }
      logRef.child(dateTime).set(user);   
      sessionStorage.clear();
      logout();
    }
    componentWillMount(){
        
    }
    render() {
        return (
            <AppBar 
            iconElementLeft={
              <Link to="/"><FontIcon style={{margin: '10px 0 0',color: '#fff', verticalAlign:'middle'}} className="material-icons">home</FontIcon></Link>
            }
            title={this.props.title} 
            style={{marginBottom:5,backgroundColor: green400,color:'#fff',marginTop:0,marginLeft:0,marginRight:0,width:'100%' }} 
            iconElementRight = {
              <div>
              {
                this.props.isLoggedIn===true&&
                <div>
                  {
                    this.props.hasSettingsMenu&&
                    <a href="manage">
                    <FontIcon style={{color: '#fff', verticalAlign:'middle'}} className="material-icons">settings</FontIcon>
                    </a>  
                  }
                  <Link to="/">
                    <FlatButton 
                      style={{display: 'inline-block',margin: '7px 0 0', color: '#fff'}} 
                      onClick={this.logUserOut}
                      icon = {<Exit />}
                    />
                  </Link>
                </div>   
              }
              </div>
            } 
          />  
        );
    }
}


