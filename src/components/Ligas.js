import React, { Component } from 'react';
import '../utils/Style.css';
import 'react-table/react-table.css';
import ReactTable from "react-table";
import ActionUpload from 'material-ui/svg-icons/file/cloud-upload';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import PageTitle from './PageTitle';
import CircularProgress from 'material-ui/CircularProgress';
import Paper from 'material-ui/Paper';
import FileUploader from 'react-firebase-file-uploader';
import TextField from 'material-ui/TextField';
import IconButton from 'material-ui/IconButton';
import Toggle from 'material-ui/Toggle';
import Dialogo from '../utils/Dialogo';
import {Table,TableBody,TableHeader,TableHeaderColumn,TableRow,TableRowColumn} from 'material-ui/Table';
import {editLiga, addLiga,activateDisactivateLigas,getLigas,getProvincias,deleteLiga} from '../utils/Controller';
import {storage} from '../utils/Firebase';
import {green400} from 'material-ui/styles/colors'
import RaisedButton from 'material-ui/RaisedButton';
import {Link} from 'react-router-dom';
import ReactTooltip from 'react-tooltip';
import placeholder from '../assets/placeholder.png';
import player from '../assets/player.png';
import {Toolbar, ToolbarGroup,  ToolbarTitle} from 'material-ui/Toolbar';
const today = new Date();
export default class Ligas extends Component {
    constructor(props){
        super(props);
        this.state={
            ligas:null,
            current:null,
            isEditing:false,
            currentNombre:'',
            currentInfo:'',
            provincias:null,
            currentDireccion:'',
            currentProvincia:'',
            currentTipo:'',
            currentTelefono:'',
            currentFechaInicio:(today),
            currentCoordenadas:'',
            currentActive:false,
            currentDestacado:false,
            currentPrecio:'',
            currentDateAdded:'',
            currentType:'',
            logo:null,
            key:null,
            addingNewone:false,
            title:"Ligas",
            loading:true,
            addedLogo:false,
            isUploading: false,
            progress:0,
            tab:null,
            logoUploaded:false,
        };
    }

    handleUploadStart = () => this.setState({isUploading: true, progress: 0});
    handleProgress = (progress) => this.setState({progress});
    handleUploadError = (error) => {
        this.setState({isUploading: false});
        console.error(error);
        alert(error);
    }
    handleUploadSuccess = (filename) => {
        this.setState({logo: filename,logoUploaded:true, progress: 100, isUploading: false});
        storage.ref('Ligas').child(filename).getDownloadURL().then(url => this.setState({logo: url}));
    };
    deleteCurrent = () =>{
        let valid = deleteLiga(this.state.key);
        if(valid){
            this.back();
            this.startOver();
        }
    }
    back = () => {
        this.setState({
            ligas:null, 
            current:null,
            isEditing:false,
            currentNombre:'',
            currentInfo:'',
            provincias:null,
            currentDireccion:'',
            currentTelefono:'',
            currentProvincia:'',
            currentTipo:'',
            currentFechaInicio:'',
            currentCoordenadas:'',
            currentActive:false,
            currentDestacado:false,
            currentPrecio:'',
            logo:null,
            currentDateAdded:'',
            currentType:'',
            key:null,
            addingNewone:false,
            title:"Ligas",
            loading:true,
            addedLogo:false,
            isUploading: false,
            progress:0,
            tab:null,
            logoUploaded:false,
        });
        this.startOver();
    }
    startOver(){
        getProvincias().then(value=>{
            let provincias = value.toJSON();
            this.setState({provincias});
        })
        getLigas().then(value=>{
            let ligasOrigin = value.toJSON();
            let ligas = {}
            const rol = sessionStorage.getItem('rol');
            if(rol === 'superadmin') {
                this.setState({loading:false, ligas: ligasOrigin})
            }
            else {
                const ligasAsignadas = JSON.parse(sessionStorage.getItem('ligasAsignadas'));
                if(ligasAsignadas) {
                    ligasAsignadas.forEach(element => {
                        if(element in ligasOrigin) { 
                            ligas[element] = ligasOrigin[element]
                        }
                    });
                }
                else {
                    ligas = ligasOrigin;
                }
                this.setState({ligas,loading:false})
            }
        }).catch(error=>{
            console.log('error: ', error);
            this.setState({loading:false})

        })
    }
    handleEdit = (current,key) =>{
        this.setState({
            current,
            isEditing:true,
            key,
            title:"Editar: "+current.nombre,
            currentEquipos:current.equipos?current.equipos:'',
            currentNombre:current.nombre?current.nombre:'',
            currentInfo:current.info?current.info:'',
            currentDireccion:current.dirección?current.dirección:'',
            currentTelefono:current.teléfono?current.teléfono:'',
            currentProvincia:current.provincia?current.provincia:'',
            currentTipo:current.tipo?current.tipo:'',
            currentActive:current.activo?current.activo:'',
            currentDateAdded:current.dateAdded?current.dateAdded:'',
            logo:current.logo?current.logo:'',
        })
    }
    handleChangeProvincia =  (event, index, value) => this.setState({currentProvincia:value});
    handleChangeTipo =  (event, index, value) => this.setState({currentTipo:value});

    handleEditCurrent = () =>{
        let valid = false;
        let dat = new Date();
        this.setState({loading:true});
        if(this.state.addingNewone===true){
            let editing = {
                dirección:this.state.currentDireccion,
                equipos:[],
                teléfono:this.state.currentTelefono,
                info:this.state.currentInfo,
                nombre:this.state.currentNombre,
                activo: this.state.currentActive,
                tipo:this.state.currentTipo,
                provincia:this.state.currentProvincia,
                logo:this.state.logo,
                dateAdded:dat.getTime(),
            }
            valid = addLiga(editing);
            console.log('editing: ', editing);
        }
        else{
            let editing = {
                dirección:this.state.currentDireccion,
                info:this.state.currentInfo,
                equipos:this.state.currentEquipos,
                nombre:this.state.currentNombre,
                activo:this.state.currentActive,
                provincia:this.state.currentProvincia,
                tipo:this.state.currentTipo,
                teléfono:this.state.currentTelefono,
                dateAdded:this.state.currentDateAdded,
                dateModified:dat.getTime(),
                logo:this.state.logo,
            };
            valid = editLiga(editing,this.state.key);
        }
        if(valid){
            this.back();
        }
       this.startOver();
    }
    enableOrDisable = (element,key) =>{
        let newValue = !element.activo;
        activateDisactivateLigas(newValue,key);
        this.startOver();
    }
    addNew = () =>{
        this.setState({isEditing:true,addingNewone:true});
    }
    componentWillMount(){
        this.startOver();
    }
    render() {
        let dialogButton = this.state.currentNombre!==''?this.state.currentProvincia!==''?<Dialogo btnLabel="Guardar" btnType="raised"  dialogText = "Está seguro que desea guardar esta información?" confirm={this.handleEditCurrent} />:'':'';
        let dialogButtonDelete = <Dialogo btnLabel="Eliminar" deleteButton={true} dialogText = "Está seguro que desea eliminar esta información?" confirm={this.deleteCurrent} />
        let provincias = this.state.provincias!==null && Object.values(this.state.provincias).map((item,index)=>{
            return(
                <MenuItem value={item.Provincia} primaryText={item.Provincia} key={index} />
            )
        })
        let detalle= this.state.ligas!==null&&Object.entries(this.state.ligas).map((item,index)=>{
            let element = item[1];
            let key = item[0];
            element.key=key;
            return element;
        });
          return this.state.loading === true ? <div className="content"><CircularProgress color={green400} size={100} thickness={7} /></div> : (
            <div className="App">
                <PageTitle 
                    title="Tu Fútbol"
                    isLoggedIn={true}
                    hasSettingsMenu ={false}
                />
                <br />
                <div style={{width: '100%'}}>
                    <Paper className="paperCanchas" zDepth={2} style={{width: '100%'}}>
                    <Toolbar style={{width: '100%',  maxWidth: '100%'}}>
                        <ToolbarGroup>
                        {
                            this.state.isEditing === true?
                            <IconButton iconClassName="material-icons" style={{textAlign:'left',color:'#000'}} tooltip="Atras" onClick={this.back}>arrow_back_ios</IconButton>
                            :
                            <Link to={"/"}><IconButton style={{textAlign:'left',color:'#000'}} iconClassName="material-icons" tooltip="Atras">arrow_back_ios</IconButton></Link>
                        }
                            <ToolbarTitle text={this.state.title} style={{color:'#000'}} className="toolbarTitle"/>
                        </ToolbarGroup>
                        {
                            sessionStorage.getItem('rol') === 'superadmin' &&
                            <div>
                            {
                                this.state.isEditing === false ?
                                <IconButton iconClassName="material-icons" style={{textAlign:'right',color:'#000'}} tooltip="Agregar" onClick={this.addNew}>add</IconButton>
                                :
                                <div>
                                {
                                    this.state.current!==null && dialogButtonDelete
                                }
                                </div>
                            }
                            </div>
                        }
                    </Toolbar>
                    <br />
                    {
                        this.state.isEditing === false ?
                            <ReactTable 
                                data={detalle}
                                defaultPageSize={10}
                                previousText= 'Anterior'
                                nextText= 'Siguiente'
                                loadingText= 'Cargando...'
                                noDataText= 'No hay datos'
                                pageText= 'Página'
                                ofText= 'de'
                                rowsText= 'Filas'
                                pageJumpText= 'Ir a página'
                                rowsSelectorText= 'filas por página'
                                filterable 
                                defaultFilterMethod={(filter, row) =>String(row[filter.id]).toLowerCase().includes( filter.value.toLowerCase())}
                                columns={[
                                {
                                    columns: [
                                    {
                                        Header: "Logo",
                                        accessor: "logo",
                                        width:85,
                                        filterable: false,
                                        Cell: row=> (
                                            row.original.logo?
                                                <a href={row.original.logo} target="_blank"><img src={row.original.logo} alt="logo" style={{margin:10, maxWidth:75, maxHeight:75}}/></a>
                                            :
                                                <img src={placeholder} alt="logo" style={{margin:10, maxWidth:75, maxHeight:75}}/>
                                        )
                                    },
                                    {
                                        Header: "Nombre",
                                        accessor: "nombre",
                                    },
                                    {
                                        Header: "Activo",
                                        accessor: "activo",
                                        width:60,
                                        filterable: false,
                                        Cell:row=>(
                                            <Toggle
                                                onToggle = {()=>this.enableOrDisable(row.original,row.original.key)}
                                                defaultToggled={row.original.activo}
                                            />
                                        )
                                    },
                                    {
                                        Header:'Editar',
                                        accessor:'key',
                                        width:60,
                                        filterable: false,
                                        Cell: row => (
                                            <IconButton tooltip="Editar liga" iconClassName="material-icons" onClick={()=>this.handleEdit(row.original,row.original.key)}>edit</IconButton>
                                        )
                                    },
                                    {
                                        Header:'Equipos',
                                        accessor:'key',
                                        width:70,
                                        filterable: false,
                                        Cell: row => (
                                            <Link to={"equipos/"+row.original.key}>
                                                <img alt="logo" data-tip="Equipos" src={player} style={{width: 50, height: 50}}/>
                                                <ReactTooltip place="top" type="dark" effect="float"/>
                                            </Link>
                                        )
                                    }
                                ]
                                }
                                ]}
                                className={"-striped -highlight table"}
                            />
                        :
                        <div>
                        <Table bodyStyle={{overflow:'visible'}}  selectable={false}>
                            <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
                                <TableRow>
                                    <TableHeaderColumn className="smallWidth">Info</TableHeaderColumn>
                                    <TableHeaderColumn>Dato</TableHeaderColumn>
                                </TableRow>
                            </TableHeader>
                            <TableBody displayRowCheckbox={false}>
                                <TableRow>
                                    <TableRowColumn className="smallWidth">Activo</TableRowColumn>
                                    <TableRowColumn>
                                        <Toggle
                                            onToggle = {(event,activated)=>this.setState({currentActive:activated})}
                                            defaultToggled={this.state.currentActive}
                                        />
                                    </TableRowColumn>
                                </TableRow>
                                <TableRow >
                                    <TableRowColumn className="smallWidth">Nombre</TableRowColumn>
                                    <TableRowColumn>
                                        <TextField
                                            hintText="Nombre"
                                            fullWidth={true}
                                            value={this.state.currentNombre}
                                            onChange={e => this.setState({currentNombre: e.target.value})}
                                        />
                                    </TableRowColumn>
                                </TableRow>
                                <TableRow>
                                    <TableRowColumn className="smallWidth">Dirección</TableRowColumn>
                                    <TableRowColumn>
                                        <TextField
                                            hintText="Dirección"
                                            value={this.state.currentDireccion}
                                            fullWidth={true}
                                            onChange={e => this.setState({currentDireccion: e.target.value})}
                                        />
                                    </TableRowColumn>
                                </TableRow>
                                <TableRow>
                                    <TableRowColumn className="smallWidth">Teléfono</TableRowColumn>
                                    <TableRowColumn>
                                        <TextField
                                            hintText="Teléfono"
                                            value={this.state.currentTelefono}
                                            fullWidth={true}
                                            onChange={e => this.setState({currentTelefono: e.target.value})}
                                        />
                                    </TableRowColumn>
                                </TableRow>
                                <TableRow>
                                    <TableRowColumn className="smallWidth">Tipo de liga</TableRowColumn>
                                    <TableRowColumn>
                                        <SelectField
                                            floatingLabelText="Tipo de liga"
                                            value={this.state.currentTipo}
                                            onChange={this.handleChangeTipo}
                                            fullWidth
                                        >
                                            <MenuItem value="Liga Federada" primaryText="Liga Federada" />
                                            <MenuItem value="Liga Independiente" primaryText="Liga Independiente" />
                                        </SelectField>
                                    </TableRowColumn>
                                </TableRow>
                                <TableRow>
                                    <TableRowColumn className="smallWidth">Provincia</TableRowColumn>
                                    <TableRowColumn>
                                        <SelectField
                                            floatingLabelText="Provincia"
                                            value={this.state.currentProvincia}
                                            onChange={this.handleChangeProvincia}
                                            fullWidth
                                        >
                                            {provincias}
                                        </SelectField>
                                    </TableRowColumn>
                                </TableRow>
                                <TableRow>
                                    <TableRowColumn className="smallWidth">Información</TableRowColumn>
                                    <TableRowColumn >
                                        <TextField
                                            hintText="Información"
                                            multiLine={true}
                                            rows={2}
                                            rowsMax={5}
                                            fullWidth={true}
                                            value={this.state.currentInfo}
                                            onChange={e => this.setState({currentInfo: e.target.value})}
                                        />
                                    </TableRowColumn>
                                </TableRow>
                                <TableRow>
                                    <TableRowColumn className="smallWidth">Logotipo</TableRowColumn>
                                    <TableRowColumn>
                                    {
                                        this.state.isUploading &&
                                            <p>Progreso: {this.state.progress}</p>
                                    }
                                    {
                                        this.state.logo &&
                                            <div>
                                                <a target="_blank" href={this.state.logo}>
                                                    <img src={this.state.logo} style={{margin:20,width:150,height:150}} alt="img"/>
                                                </a>
                                                <br />
                                            </div>
                                            
                                    }
                                    {
                                        this.state.logoUploaded === false &&
                                    
                                        <RaisedButton
                                            label="Logotipo"
                                            labelPosition="before"
                                            style={{margin:20}}
                                            containerElement="label"
                                            icon={<ActionUpload />}
                                        >   
                                            <FileUploader
                                                hidden
                                                accept="file/*"
                                                name="logo"
                                                randomizeFilename
                                                storageRef={storage.ref('Ligas')}
                                                onUploadStart={this.handleUploadStart}
                                                onUploadError={this.handleUploadError}
                                                onUploadSuccess={this.handleUploadSuccess}
                                                onProgress={this.handleProgress}
                                            />
                                        </RaisedButton>
                                        
                                    }
                                    </TableRowColumn>
                                </TableRow>
                            </TableBody>
                        </Table>
                        {dialogButton}
                        </div>
                    }
                    </Paper>
                </div>
            </div>
        )
    }
}


