import React, { Component } from 'react';
import '../utils/Style.css';
import ActionUpload from 'material-ui/svg-icons/file/cloud-upload';
import PageTitle from './PageTitle';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import Checkbox from 'material-ui/Checkbox';
import CircularProgress from 'material-ui/CircularProgress';
import Paper from 'material-ui/Paper';
import FileUploader from 'react-firebase-file-uploader';
import TextField from 'material-ui/TextField';
import DatePicker from 'material-ui/DatePicker';
import IconButton from 'material-ui/IconButton';
import Toggle from 'material-ui/Toggle';
import 'react-table/react-table.css';
import ReactTable from "react-table";
import {Link} from 'react-router-dom';
import Dialogo from '../utils/Dialogo';
import {Table,TableBody,TableHeader,TableHeaderColumn,TableRow,TableRowColumn} from 'material-ui/Table';
import {getCampeonatos, editCampeonato,getCategorías, addCampeonato,activateDisactivate,getBarriales,formatDate, getEquipos,deleteCampeonato, isNumber} from '../utils/Controller';
import {storage} from '../utils/Firebase';
import {green400} from 'material-ui/styles/colors';
import ReactTooltip from 'react-tooltip';
import placeholder from '../assets/placeholder.png';
import calendar from '../assets/calendar.png';
import referee from '../assets/referee.png';
import RaisedButton from 'material-ui/RaisedButton';
import {Toolbar, ToolbarGroup, ToolbarTitle} from 'material-ui/Toolbar';
const today = new Date();

const styles = {
    block: {
      maxWidth: 250,
    },
    checkbox: {
      marginBottom: 16,
    },
  };
  
export default class Campeonatos extends Component {
    constructor(props){
        super(props);
        this.state={
            campeonatos:null,
            current:null,
            isEditing:false,
            currentNombre:'',
            equipos:[],
            currentInfo:'',
            currentDiferibles:0,
            currentCanchas:0,
            barriales:null,
            currentCategoría:'',
            categorías:null,
            currentBarrial:'',
            currentFechaDeInicio:(today),
            currentCoordenadas:'',
            currentActive:false,
            currentEquipos:[],
            currentPartidos:[],
            currentDateAdded:'',
            logo:null,
            key:null,
            addingNewone:false,
            title:"Campeonatos",
            loading:true,
            addedLogo:false,
            isUploading: false,
            progress:0,
            tab:null,
            logoUploaded:false,
        };
    }
    handleChangeBarrial =  (event, index, value) => {
        this.setState({currentBarrial:value});
        this.handleEquipos(value,this.state.currentCategoría)
    }
    handleChangeCategoría =  (event, index, value) => {
        this.setState({currentCategoría:value})
        this.handleEquipos(this.state.currentBarrial,value);
    };
    handleUploadStart = () => this.setState({isUploading: true, progress: 0});
    handleProgress = (progress) => this.setState({progress});
    handleUploadError = (error) => {
        this.setState({isUploading: false});
        console.error(error);        
    }
    handleUploadSuccess = (filename) => {
        this.setState({logo: filename,logoUploaded:true, progress: 100, isUploading: false});
        storage.ref('Campeonatos').child(filename).getDownloadURL().then(url => this.setState({logo: url}));
    };
   
    back = () => {
        this.setState({
            campeonatos:null, 
            current:null,
            isEditing:false,
            currentNombre:'',
            currentInfo:'',
            currentDiferibles:0,
            currentCanchas:0,
            currentCategoría:'',
            categorías:null,
            currentBarrial:'',
            barriales:null,
            currentFechaDeInicio:'',
            currentEquipos:[],
            currentPartidos:[],
            currentCoordenadas:'',
            currentActive:false,
            logo:null,
            currentDateAdded:'',
            key:null,
            addingNewone:false,
            title:"Campeonatos",
            loading:true,
            addedLogo:false,
            isUploading: false,
            progress:0,
            tab:null,
            logoUploaded:false,
        });
        this.startOver();
    }
    startOver(){
        getCategorías().then(value=>{
            this.setState({categorías:Object.values(value.toJSON())})
        }).catch(error=>{
            console.log('error: ', error);
        })
        getBarriales().then(value=>{
            this.setState({barriales:Object.values(value.toJSON())})
        }).catch(error=>{
            console.log('error: ', error);
        })
        getCampeonatos().then(value=>{
            let campeonatosOrigin = value.toJSON();
            let campeonatos = {}
            const rol = sessionStorage.getItem('rol');
            if(rol === 'superadmin') {
                this.setState({loading:false, campeonatos: campeonatosOrigin})
            }
            else {
                const ligasAsignadas = JSON.parse(sessionStorage.getItem('ligasAsignadas'));
                const campeonatosAsignados = JSON.parse(sessionStorage.getItem('campeonatosAsignados'));
                if(ligasAsignadas) {
                    ligasAsignadas.forEach(element => {
                        Object.entries(campeonatosOrigin).forEach(item=>{
                            if(item[1].liga === element) {
                                campeonatos[item[1].nombre] = campeonatosOrigin[item[0]]
                            }
                        })
                    });
                }
                else if(campeonatosAsignados) {
                    campeonatosAsignados.forEach(element => {
                        Object.entries(campeonatosOrigin).forEach(item=>{
                            if(item[1].nombre === element) {
                                campeonatos[item[1].nombre] = campeonatosOrigin[item[0]]
                            }
                        })
                    });
                }
                else {
                    campeonatos = campeonatosOrigin;
                }
                this.setState({campeonatos,loading:false})
            }

        }).catch(error=>{
            console.log('error: ', error);
        })
    }
    handleEquipos = (currentBarrial,currentCategoría) =>{
        let self  =  this;
        getEquipos(currentBarrial).then(val=>{
            let v = val.toJSON();
            if(v){
                let teams =Object.values(v);
                let equipos = [];
                teams.map(element=>{
                    return Object.values(element.categorías).map(el=>{
                        if(el === currentCategoría){
                            equipos.push(element);
                        }
                        return el;
                    });
                })
                self.setState({equipos})
            }
            else {
                console.log('Teams not found');
                if(self.state.addingNewone===false){
                    alert('No existen equipos');
                }
                self.setState({equipos:[]})
            }
        }).catch(error=>{
            console.log('error: ', error);
        });
    }
    deleteCurrent = () =>{
        let valid = deleteCampeonato(this.state.key);
        if(valid){
            this.back();
            this.startOver();
        }
    }
    handleChangeDiferidos = (e) => {
        const currentDiferibles = e.target.value;
        if (isNumber(currentDiferibles)) { 
            this.setState({currentDiferibles}); 
        }
    }

    handleChangeCanchas = (e) => {
        const currentCanchas = e.target.value;
        if (isNumber(currentCanchas)) { 
            this.setState({currentCanchas}); 
        }
    }
    handleEdit = (current,key) =>{
        let fechaDeInicio = current.fechaDeInicio?new Date(current.fechaDeInicio):formatDate(new Date());
        this.setState({
            current,
            isEditing:true,
            key,
            title:"Editar "+current.nombre,
            currentNombre:current.nombre?current.nombre:'',
            currentInfo:current.info?current.info:'',
            currentDiferibles:current.diferibles?current.diferibles:0,
            currentCanchas:current.canchas?current.canchas:0,
            currentEquipos:current.equipos?Object.values(current.equipos):[],
            currentCategoría:current.categoría?current.categoría:'',
            currentBarrial:current.liga?current.liga:'',
            currentPartidos:current.partidos?Object.values(current.partidos):[],
            currentActive:current.activo?current.activo:true,
            currentFechaDeInicio:fechaDeInicio,
            currentDateAdded:current.dateAdded,
            logo:current.logo,
        })
        this.handleEquipos(current.liga,current.categoría);
    }
    checkEquipo = (item) => {
        let currentEquipos = (this.state.currentEquipos);
        let exists = false;
        let index = -1;
        for(var i = 0; i<currentEquipos.length;i++){
            if(currentEquipos[i].nombre===item.nombre && currentEquipos[i].dateAdded === item.dateAdded && currentEquipos[i].notas === item.notas && currentEquipos[i].escudo===item.escudo){
                exists = true;
                index = i;
            }
        }
        if(exists && index > -1){
            currentEquipos.splice(index, 1);
        }
        else {
            currentEquipos.push(item);
        }
        this.setState({currentEquipos});
    }
    handleEditCurrent = () =>{
        let valid = false;
        let currentFechaDeInicio = this.state.currentFechaDeInicio.getFullYear() + "/" + (this.state.currentFechaDeInicio.getMonth() + 1) + "/" + this.state.currentFechaDeInicio.getDate();
        let dat = new Date();
        this.setState({loading:true});
        if(this.state.addingNewone===true){
            let editing = {
                liga:this.state.currentBarrial,
                info:this.state.currentInfo,
                diferibles:this.state.currentDiferibles,
                canchas:this.state.currentCanchas,
                equipos:this.state.currentEquipos,
                nombre:this.state.currentNombre,
                categoría:this.state.currentCategoría,
                activo: this.state.currentActive,
                fechaDeInicio:currentFechaDeInicio,
                partidos:this.state.currentPartidos,
                logo:this.state.logo ? this.state.logo : '',
                dateAdded:dat.getTime(),
                jornadas: this.state.jornadas || []
            }
            valid = addCampeonato(editing);
        }
        else{
            let editing = {
                liga:this.state.currentBarrial,
                info:this.state.currentInfo,
                diferibles:this.state.currentDiferibles,
                canchas:this.state.currentCanchas,
                equipos:this.state.currentEquipos,
                nombre:this.state.currentNombre,
                categoría:this.state.currentCategoría,
                activo:this.state.currentActive,
                fechaDeInicio:currentFechaDeInicio,
                partidos:this.state.currentPartidos,
                dateAdded:this.state.currentDateAdded,
                dateModified:dat.getTime(),
                logo:this.state.logo ? this.state.logo : '',
                jornadas: this.state.current.jornadas || []
            };
            valid = editCampeonato(editing,this.state.key);
        }
        if(valid){
            this.back();
        }
       this.startOver();
    }
    enableOrDisable = (element,key) =>{
        let newValue = !element.activo;
        activateDisactivate(newValue,key);
        this.startOver();
    }
    addNew = () =>{
        this.setState({isEditing:true,addingNewone:true});
    }
    handleBirthDate =  (event, currentFechaDeInicio) => {
        this.setState({currentFechaDeInicio});
      };
    componentWillMount(){
        this.startOver();
    }
    renderTable = () => {
        if(sessionStorage.getItem('rol') === 'sancionador') {
            return (
                [
                    {
                        columns: [
                            {
                                Header: "Logo",
                                accessor: "logo",
                                width:85,
                                Cell: row=> (
                                    row.original.logo?
                                        <a href={row.original.logo} target="_blank"><img src={row.original.logo} alt="logo" style={{margin:10, maxWidth:75, maxHeight:75}}/></a>
                                    :
                                        <img src={placeholder} alt="logo" style={{margin:10, maxWidth:75, maxHeight:75}}/>
                                )
                            },
                            {
                                Header: "Nombre",
                                accessor: "nombre",
                            },
                            {
                                Header:'Editar',
                                accessor:'key',
                                width:90,
                                Cell: row => (
                                    <IconButton iconClassName="material-icons" tooltip="Editar campeonato" onClick={()=>this.handleEdit(row.original,row.original.key)}>edit</IconButton>
                                ),
                                filterable: false
                            },
                            {
                                Header:'Partidos',
                                accessor:'key',
                                width:75,
                                Cell: row => (
                                    <Link to={"/partidos/"+row.original.key}>
                                        <img data-tip="Partidos" src={calendar} style={{width: 50, height: 50}} alt="logo"/>
                                        <ReactTooltip place="top" type="dark" effect="float"/>
                                    </Link>
                                )
                            },
                            {
                                Header:'Amonestaciones',
                                accessor:'key',
                                width:130,
                                Cell: row => (
                                    <Link to={"/amonestaciones/"+row.original.key}>
                                        <img data-tip="Amonestaciones" src={referee} style={{width: 50, height: 50}} alt="logo"/>
                                        <ReactTooltip place="top" type="dark" effect="float"/>
                                    </Link>
                                )
                            }
                        ]
                    }
                ]
            )
        }
        if(sessionStorage.getItem('rol') === 'vocal') {
            return (
                [
                    {
                        columns: [
                            {
                                Header: "Logo",
                                accessor: "logo",
                                width:85,
                                Cell: row=> (
                                    row.original.logo?
                                        <a href={row.original.logo} target="_blank"><img src={row.original.logo} alt="logo" style={{margin:10, maxWidth:75, maxHeight:75}}/></a>
                                    :
                                        <img src={placeholder} alt="logo" style={{margin:10, maxWidth:75, maxHeight:75}}/>
                                )
                            },
                            {
                                Header: "Nombre",
                                accessor: "nombre",
                            },
                            {
                                Header:'Partidos',
                                accessor:'key',
                                width:75,
                                Cell: row => (
                                    <Link to={"/partidos/"+row.original.key}>
                                        <img data-tip="Partidos" src={calendar} style={{width: 50, height: 50}} alt="logo"/>
                                        <ReactTooltip place="top" type="dark" effect="float"/>
                                    </Link>
                                )
                            }
                        ]
                    }
                ]
            )
        }
        if(sessionStorage.getItem('rol') === 'admin') {
            return (
                [
                    {
                        columns: [
                            {
                                Header: "Logo",
                                accessor: "logo",
                                width:85,
                                Cell: row=> (
                                    row.original.logo?
                                        <a href={row.original.logo} target="_blank"><img src={row.original.logo} alt="logo" style={{margin:10, maxWidth:75, maxHeight:75}}/></a>
                                    :
                                        <img src={placeholder} alt="logo" style={{margin:10, maxWidth:75, maxHeight:75}}/>
                                ),
                                filterable: false
                            },
                            {
                                Header: "Nombre",
                                accessor: "nombre",
                            },
                            {
                                Header: "Activo",
                                accessor: "activo",
                                width:60,
                                Cell:row=>(
                                    <Toggle
                                        onToggle = {()=>this.enableOrDisable(row.original,row.original.key)}
                                        defaultToggled={row.original.activo}
                                    />
                                ),
                                filterable: false
                            },
                            {
                                Header:'Editar',
                                accessor:'key',
                                width:90,
                                Cell: row => (
                                    <IconButton iconClassName="material-icons" tooltip="Editar campeonato" onClick={()=>this.handleEdit(row.original,row.original.key)}>edit</IconButton>
                                ),
                                filterable: false
                            },
                            {
                                Header:'Partidos',
                                accessor:'key',
                                width:75,
                                Cell: row => (
                                    <Link to={"/partidos/"+row.original.key}>
                                        <img data-tip="Partidos" src={calendar} style={{width: 50, height: 50}} alt="logo"/>
                                        <ReactTooltip place="top" type="dark" effect="float"/>
                                    </Link>
                                ),
                                filterable: false
                            }
                        ]
                    }
                ]
            )
        }
        return (
            [
                {
                    columns: [
                        {
                            Header: "Logo",
                            accessor: "logo",
                            width:85,
                            Cell: row=> (
                                row.original.logo?
                                    <a href={row.original.logo} target="_blank"><img src={row.original.logo} alt="logo" style={{margin:10, maxWidth:75, maxHeight:75}}/></a>
                                    :
                                        <img src={placeholder} alt="logo" style={{margin:10, maxWidth:75, maxHeight:75}}/>
                            ),
                            filterable: false
                        },
                        {
                            Header: "Nombre",
                            accessor: "nombre",
                        },
                        {
                            Header: "Activo",
                            accessor: "activo",
                            width:60,
                            Cell:row=>(
                                <Toggle
                                    onToggle = {()=>this.enableOrDisable(row.original,row.original.key)}
                                    defaultToggled={row.original.activo}
                                />
                            ),
                            filterable: false
                        },
                        {
                            Header:'Editar',
                            accessor:'key',
                            width:90,
                            Cell: row => (
                                <IconButton iconClassName="material-icons" tooltip="Editar campeonato" onClick={()=>this.handleEdit(row.original,row.original.key)}>edit</IconButton>
                            ),
                            filterable: false
                        },
                        {
                            Header:'Partidos',
                            accessor:'key',
                            width:75,
                            Cell: row => (
                                <Link to={"/partidos/"+row.original.key}>
                                    <img data-tip="Partidos" src={calendar} style={{width: 50, height: 50}} alt="logo"/>
                                    <ReactTooltip place="top" type="dark" effect="float"/>
                                </Link>
                            ),
                            filterable: false
                        },
                        {
                            Header:'Amonestaciones',
                            accessor:'key',
                            width:130,
                            Cell: row => (
                                <Link to={"/amonestaciones/"+row.original.key}>
                                    <img data-tip="Amonestaciones" src={referee} style={{width: 50, height: 50}} alt="logo"/>
                                    <ReactTooltip place="top" type="dark" effect="float"/>
                                </Link>
                            ),
                            filterable: false

                        }
                    ]
                }
            ]
        )
    }
    render() {
        let dialogButton = this.state.currentEquipos.length>0?this.state.currentNombre!==''?this.state.currentCategoría!==''?this.state.currentBarrial!==''?this.state.currentFechaDeInicio!==''?<Dialogo btnLabel="Guardar" btnType="raised"  dialogText = "Está seguro que desea guardar esta información?" confirm={this.handleEditCurrent} />:'':'':'':'':'';
        let dialogButtonDelete = <Dialogo btnLabel="Eliminar" deleteButton={true} dialogText = "Está seguro que desea eliminar esta información?" confirm={this.deleteCurrent} />
        let barriales = this.state.barriales!==null && Object.values(this.state.barriales).map((item,index)=>{
            return(
                <MenuItem value={item.nombre} primaryText={item.nombre} key={index} />
            )
        });
        let categorías = this.state.categorías!==null && Object.values(this.state.categorías).map((item,index)=>{
            return(
                <MenuItem value={item.nombre} primaryText={item.nombre} key={index} />
            )
        });
        let equipos = this.state.equipos.length>0&&Object.values(this.state.equipos).map((item,index)=>{
            let exists = false;
            let equips=this.state.currentEquipos;
            for(var i = 0; i<equips.length;i++){
                if(equips[i].nombre===item.nombre && equips[i].dateAdded === item.dateAdded && equips[i].notas === item.notas && equips[i].escudo===item.escudo){
                    exists = true;
                }
            }
            return(
                <Checkbox style={styles.checkbox} key={index} label={item.nombre} onCheck={()=>this.checkEquipo(item)} checked = {exists}/>
            )
        });

        let detalle= this.state.campeonatos!==null&&Object.entries(this.state.campeonatos).map((item,index)=>{
            let element = item[1];
            let key = item[0];
            element.key=key;
            return element;
          });
          return this.state.loading === true ? <div className="content"><CircularProgress color={green400} size={100} thickness={7} /></div> : (
            <div className="App">
                <PageTitle 
                    title="Tu Fútbol"
                    isLoggedIn={true}
                    hasSettingsMenu ={false}
                />
                <br />
                <div style={{width: '100%'}}>
                    <Paper className="paperCanchas" zDepth={2} style={{width: '100%'}}>
                    <Toolbar style={{width: '100%',  maxWidth: '100%'}}>
                        <ToolbarGroup>
                        {
                            this.state.isEditing === true ?
                            <IconButton iconClassName="material-icons" style={{textAlign:'left',color:'#000'}} tooltip="Atras"  onClick={this.back}>arrow_back_ios</IconButton>
                            :
                            <Link to={"/"}><IconButton style={{textAlign:'left',color:'#000'}} iconClassName="material-icons" tooltip="Atras">arrow_back_ios</IconButton></Link>
                        }
                            <ToolbarTitle text={this.state.title} style={{color:'#000'}} className="toolbarTitle"/>
                        </ToolbarGroup>
                        {
                            sessionStorage.getItem('rol') !== 'vocal' &&
                            <div>
                                {
                                    this.state.isEditing === false ?
                                    <IconButton iconClassName="material-icons" style={{textAlign:'right',color:'#000'}} tooltip="Agregar" onClick={this.addNew}>add</IconButton>
                                    :
                                    <div>{this.state.current!==null&&dialogButtonDelete}</div>
                                }
                            </div>
                        }
                    </Toolbar>
                    <br />
                    {
                        this.state.isEditing === false ?
                        <div style={{width: '100%'}}>
                            {
                                detalle.length>0&&   
                                <ReactTable 
                                    data={detalle}
                                    defaultPageSize={10}
                                    previousText= 'Anterior'
                                    nextText= 'Siguiente'
                                    loadingText= 'Cargando...'
                                    noDataText= 'No hay datos'
                                    pageText= 'Página'
                                    ofText= 'de'
                                    rowsText= 'Filas'
                                    pageJumpText= 'Ir a página'
                                    rowsSelectorText= 'filas por página'
                                    filterable 
                                    defaultFilterMethod={(filter, row) =>String(row[filter.id]).toLowerCase().includes( filter.value.toLowerCase())}
                                    columns={this.renderTable()}
                                    className={"-striped -highlight table"}
                                    style={{width: '100%'}}
                                />
                            }
                            </div>
                        :
                        <div style={{width:'100%'}}>
                        <Table bodyStyle={{overflow:'visible'}}  selectable={false} style={{width:'100%'}}>
                            <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
                                <TableRow>
                                    <TableHeaderColumn className="smallWidth">Info</TableHeaderColumn>
                                    <TableHeaderColumn>Dato</TableHeaderColumn>
                                </TableRow>
                            </TableHeader>
                            <TableBody displayRowCheckbox={false}>
                                <TableRow>
                                    <TableRowColumn className="smallWidth">Activo</TableRowColumn>
                                    <TableRowColumn>
                                        <Toggle
                                            onToggle = {(event,activated)=>this.setState({currentActive:activated})}
                                            defaultToggled={this.state.currentActive}
                                        />
                                    </TableRowColumn>
                                </TableRow>
                                <TableRow >
                                    <TableRowColumn className="smallWidth">Nombre</TableRowColumn>
                                    <TableRowColumn>
                                        <TextField
                                            hintText="Nombre"
                                            fullWidth={true}
                                            value={this.state.currentNombre}
                                            onChange={e => this.setState({currentNombre: e.target.value})}
                                        />
                                    </TableRowColumn>
                                </TableRow>
                                <TableRow>
                                    <TableRowColumn className="smallWidth">Liga</TableRowColumn>
                                    <TableRowColumn>
                                        <SelectField
                                            fullWidth={true}
                                            floatingLabelText="Barrial"
                                            value={this.state.currentBarrial}
                                            onChange={this.handleChangeBarrial}
                                        >
                                            {barriales}
                                        </SelectField>
                                    </TableRowColumn>
                                </TableRow>
                                <TableRow>
                                    <TableRowColumn className="smallWidth">Categoría</TableRowColumn>
                                    <TableRowColumn>
                                        <SelectField
                                            fullWidth={true}
                                            floatingLabelText="Categoría"
                                            value={this.state.currentCategoría}
                                            onChange={this.handleChangeCategoría}
                                        >
                                            {categorías}
                                        </SelectField>
                                    </TableRowColumn>
                                </TableRow>
                                {
                                    this.state.currentCategoría!==''&&
                                    <TableRow>
                                        <TableRowColumn className="smallWidth">Equipos</TableRowColumn>
                                        <TableRowColumn>
                                                {equipos}
                                        </TableRowColumn>
                                    </TableRow>
                                }
                                <TableRow>
                                    <TableRowColumn className="smallWidth">Fecha de Inicio</TableRowColumn>
                                    <TableRowColumn>
                                        <DatePicker 
                                            hintText="Fecha de Inicio" 
                                            container="inline"
                                            fullWidth={true}
                                            value={this.state.currentFechaDeInicio}
                                            onChange={this.handleBirthDate}
                                        />
                                    </TableRowColumn>
                                </TableRow>
                                <TableRow>
                                    <TableRowColumn className="smallWidth">Información</TableRowColumn>
                                    <TableRowColumn >
                                        <TextField
                                            hintText="Información"
                                            multiLine={true}
                                            rows={2}
                                            rowsMax={5}
                                            fullWidth={true}
                                            value={this.state.currentInfo}
                                            onChange={e => this.setState({currentInfo: e.target.value})}
                                        />
                                    </TableRowColumn>
                                </TableRow>
                                <TableRow>
                                    <TableRowColumn className="smallWidth">Partidos diferibles por equipo</TableRowColumn>
                                    <TableRowColumn >
                                        <TextField
                                            hintText="Partidos diferibles por equipo"
                                            fullWidth={true}
                                            value={this.state.currentDiferibles}
                                            onChange={this.handleChangeDiferidos}
                                        />
                                    </TableRowColumn>
                                </TableRow>
                                <TableRow>
                                    <TableRowColumn className="smallWidth">Número de canchas en las que se jugará</TableRowColumn>
                                    <TableRowColumn >
                                        <TextField
                                            hintText="Número de canchas en las que se jugará"
                                            fullWidth={true}
                                            value={this.state.currentCanchas}
                                            onChange={this.handleChangeCanchas}
                                        />
                                    </TableRowColumn>
                                </TableRow>
                                {
                                    this.state.addingNewone === false &&
                                        <TableRow>
                                            <TableRowColumn className="smallWidth">Jornadas</TableRowColumn>
                                            <TableRowColumn>
                                                <Link to={"/jornadas/"+this.state.key}>
                                                    <RaisedButton label="Jornadas"></RaisedButton>
                                                </Link>
                                            </TableRowColumn>
                                        </TableRow>
                                }
                                <TableRow>
                                    <TableRowColumn className="smallWidth">Logotipo</TableRowColumn>
                                    <TableRowColumn>
                                    {
                                        this.state.isUploading &&
                                            <p>Progreso: {this.state.progress}</p>
                                    }
                                    {
                                        this.state.logo &&
                                            <div>
                                                <a target="_blank" href={this.state.logo}>
                                                    <img src={this.state.logo} style={{margin:20,width:150,height:150}} alt="img"/>
                                                </a>
                                                <br />
                                            </div>
                                    }
                                    {
                                        this.state.logoUploaded === false &&
                                    
                                        <RaisedButton
                                            label="Logotipo"
                                            labelPosition="before"
                                            style={{margin:20}}
                                            containerElement="label"
                                            icon={<ActionUpload />}
                                        >   
                                            <FileUploader
                                                hidden
                                                accept="file/*"
                                                name="logo"
                                                randomizeFilename
                                                storageRef={storage.ref('Campeonatos')}
                                                onUploadStart={this.handleUploadStart}
                                                onUploadError={this.handleUploadError}
                                                onUploadSuccess={this.handleUploadSuccess}
                                                onProgress={this.handleProgress}
                                            />
                                        </RaisedButton>
                                        
                                    }
                                    </TableRowColumn>
                                </TableRow>
                            </TableBody>
                        </Table>
                        {dialogButton}
                        </div>
                    }
                    </Paper>
                </div>
            </div>
        )
    }
}


