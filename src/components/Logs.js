import React, { Component } from 'react';
import '../utils/Style.css';
import PageTitle from './PageTitle';
import CircularProgress from 'material-ui/CircularProgress';
import Paper from 'material-ui/Paper';
import IconButton from 'material-ui/IconButton';
import {Link} from 'react-router-dom';
import {getLogs} from '../utils/Controller';
import {green400} from 'material-ui/styles/colors'
import {Toolbar, ToolbarGroup,  ToolbarTitle} from 'material-ui/Toolbar';
import 'react-table/react-table.css';
import ReactTable from "react-table";
export default class Logs extends Component {
    constructor(props){
        super(props);
        this.state={
            logs:null,
            title:"Logs",
            loading: true
        };
    }
    componentWillMount(){
        this.startOver();
    }
    back = () => {
        this.setState({
            logs:null,
            title:"Logs",
        });
        this.startOver();
    }
    startOver = () => {
        this.setState({loading: true});
        getLogs().then(value=>{
            let logs = value.toJSON();
            this.setState({logs,loading:false});
        }).catch((err) => {
            console.log('err: ', err);
            this.setState({loading: true});
        })
    }
    render() {
        const data = this.state.logs!==null?Object.entries(this.state.logs).map((item,index)=>{
            return {
                date: item[0],
                data: item[1].description ? JSON.stringify(item[1]) : item[1]
            };
          }):[];
          return this.state.loading === true ? <div className="content"><CircularProgress color={green400} size={100} thickness={7} /></div> : (
            <div className="App">
                <PageTitle 
                    title="Tu Fútbol"
                    isLoggedIn={true}
                    hasSettingsMenu ={false}
                />
                <br />
                <div style={{width: '100%'}}>
                    <Paper className="paperCanchas" zDepth={2} style={{width: '100%'}}>
                    <Toolbar style={{width: '100%',  maxWidth: '100%'}}>
                        <ToolbarGroup>
                            <Link to={"/"}><IconButton style={{textAlign:'left',color:'#000'}} tooltip="Atras" iconClassName="material-icons">arrow_back_ios</IconButton></Link>
                            <ToolbarTitle text={this.state.title} style={{color:'#000'}} className="toolbarTitle"/>
                        </ToolbarGroup>
                    </Toolbar>
                    <ReactTable 
                        data={data}
                        defaultPageSize={10}
                        previousText= 'Anterior'
                        nextText= 'Siguiente'
                        loadingText= 'Cargando...'
                        noDataText= 'No hay datos'
                        pageText= 'Página'
                        ofText= 'de'
                        rowsText= 'Filas'
                        pageJumpText= 'Ir a página'
                        rowsSelectorText= 'filas por página'
                        filterable 
                        defaultFilterMethod={(filter, row) =>String(row[filter.id]).toLowerCase().includes( filter.value.toLowerCase())}
                        columns={[
                        {
                            columns: [
                            {
                                Header: "Fecha",
                                accessor: "date",
                                width: 200
                            },
                            {
                                Header: "Data",
                                accessor: "data",
                                style: { 'whiteSpace': 'unset' },
                                Cell:  row => (<code>{row.original.data}</code>)
                            }
                        ]
                        }
                        ]}
                        className={"-striped -highlight table"}
                    />
                    </Paper>
                </div>
            </div>
        )
    }
}


