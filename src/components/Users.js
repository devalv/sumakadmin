import React, { Component } from 'react';
import '../utils/Style.css';
import PageTitle from './PageTitle';
import CircularProgress from 'material-ui/CircularProgress';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import IconButton from 'material-ui/IconButton';
import Dialogo from '../utils/Dialogo';
import {Link} from 'react-router-dom';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import {Table,TableBody,TableHeader,TableHeaderColumn,TableRow,TableRowColumn} from 'material-ui/Table';
import {getUsers,addUser,editUser,deleteUser, getLigas, getCampeonatos, resetPasswordEmail, campeonatosRef, encode} from '../utils/Controller';
import {green400} from 'material-ui/styles/colors'
import {Toolbar, ToolbarGroup,  ToolbarTitle} from 'material-ui/Toolbar';
import 'react-table/react-table.css';
import ReactTable from "react-table";
import RaisedButton from 'material-ui/RaisedButton/RaisedButton';
export default class Usuarios extends Component {
    constructor(props){
        super(props);
        this.state={
            current:null,
            isEditing:false,
            currentName:'',
            currentEmail: '',
            currentRol: '',
            users:[],
            ligas:[],
            campeonatos:[],
            ligasAsignadas:[],
            campeonatosAsignados:[],
            key:null,
            addingNewone:false,
            title:"Usuarios",
            loading:true,
            uid: '',
            config: null,
            password: '',
            token: '',
            createdBy: '',
            createdOn: '',
        };
    }
    componentWillMount(){
        const e = sessionStorage.getItem('e');
        const p = sessionStorage.getItem('p');
        if(e && p) {
            this.startOver();
        }
    }
    back = () => {
        this.setState({
            current:null,
            isEditing:false,
            currentName:'',
            currentEmail: '',
            currentRol: '',
            users:[],
            ligas:[],
            campeonatos:[],
            ligasAsignadas:[],
            campeonatosAsignados:[],
            key:null,
            addingNewone:false,
            title:"Usuarios",
            loading:true,
            uid: '',
            config: null,
            password: '',
            token: '',
            createdBy: '',
            createdOn: '',
        });
        this.startOver();
    }
    startOver(){
        getLigas().then(value=>{
            const ligas = Object.entries(value.toJSON());
            this.setState({ligas});
        })
        getUsers().then(value=>{
            const users = value.toJSON();
            console.log('users: ', users);
            const rol = sessionStorage.getItem('rol');
            const ligasAsignadas = JSON.parse(sessionStorage.getItem('ligasAsignadas'));
            const defUsers = [];
            
            if(rol==='admin'){
                let usersArr = Object.values(users);
                console.log('usersArr: ', usersArr);
                usersArr.forEach(item=>{
                    if(item.rol === 'vocal' || item.rol === 'sancionador') {
                        if(item.campeonatosAsignados && ligasAsignadas){
                            for(let i = 0; i<ligasAsignadas.length; i++) {
                                const element = ligasAsignadas[i];
                                for(let j = 0; j<Object.values(item.campeonatosAsignados).length; j++){
                                    const el = Object.values(item.campeonatosAsignados)[j];
                                    console.log('el: ', el);
                                    campeonatosRef.child(el+'/liga/').on('value', snapshot=>{
                                        const val = snapshot.toJSON();
                                        if(element === val){
                                            defUsers.push(item);
                                        }
                                    })                                        
                                }
                            }
                            
                        }
                    } else {
                        delete users[item.uid];
                    }
                })
            }
            getCampeonatos().then(val=>{
                let campeonatos = Object.entries(val.toJSON());
                if(rol === 'admin') {
                    let campeonatosAsignados = {}
                    ligasAsignadas.forEach(item=>{
                        campeonatos.forEach(it=>{
                            if(it[1].liga === item) {
                                campeonatosAsignados[item] = it[1];
                            }
                        })
                    })
                    campeonatos = Object.entries(campeonatosAsignados);
                    console.log('campeonatos: ', campeonatos);
                    this.setState({users: defUsers,campeonatos,loading:false});
                }
                else{
                    this.setState({users,campeonatos,loading:false});
                }                
            })
        })
    }
    sendEmailResetPassword = () => {
        this.setState({loading:true});
        resetPasswordEmail(this.state.currentEmail).then((value)=>{
            console.log('value: ', value);
            this.setState({loading:false});
            alert('Se ha enviado un correo de recuperación');
        }).catch(err=>{
            console.log('err: ', err);
            alert('Ha ocurrido un error');
        });
    }
    handleEdit = (current,key) =>{
        this.setState({
            current,
            isEditing:true,
            key,
            title:"Editar usuario",
            currentName:current.name?current.name:'',
            currentEmail:current.email?current.email:'',
            currentRol:current.rol?current.rol:'',
            ligasAsignadas:current.ligasAsignadas?Object.values(current.ligasAsignadas):[],
            campeonatosAsignados:current.campeonatosAsignados?Object.values(current.campeonatosAsignados):[],
            uid: current.uid?current.uid:'',
            config: current.config?current.config:null,
            password: current.password?current.password:'',
            token: current.token?current.token:'',
            createdBy: current.createdBy?current.createdBy:sessionStorage.getItem('uid')?sessionStorage.getItem('uid'):'',
            createdOn: current.createdOn?current.createdOn:Date.now()
        })
    }
    deleteCurrent = () =>{
        let valid = deleteUser(this.state.key);
        if(valid){
            this.back();
            this.startOver();
        }
    }
    handleEditCurrent = () =>{
        this.setState({loading:true});
        try{
            if(this.state.addingNewone===true){
                let editing = {
                    name:this.state.currentName,
                    rol: this.state.currentRol,
                    email: this.state.currentEmail,
                    ligasAsignadas: this.state.ligasAsignadas,
                    campeonatosAsignados: this.state.campeonatosAsignados,
                    password: this.state.password,
                    config: this.state.config,
                    createdOn: Date.now(),
                    createdBy: sessionStorage.getItem('uid')?sessionStorage.getItem('uid'):''
                }
                addUser(editing).then(()=>{
                    this.startOver();
                    this.back();
                });
            }
            else{
                let editing = {
                    name:this.state.currentName,
                    rol: this.state.currentRol,
                    email: this.state.currentEmail,
                    uid: this.state.uid,
                    ligasAsignadas: this.state.ligasAsignadas,
                    campeonatosAsignados: this.state.campeonatosAsignados,
                    password: this.state.password,
                    token: this.state.token,
                    config: this.state.config,
                    createdBy: this.state.createdBy,
                    createdOn: this.state.createdOn
                };
                console.log('editing: ', editing);
                editUser(editing,this.state.uid).then(()=>{
                    this.startOver();
                    this.back();
                });
            }
            
        }
        catch (e){
            console.log('e: ', e);
            alert('error')
        }
    }
    addNew = () =>{
        this.setState({isEditing:true,addingNewone:true});
    }
    renderCampeonatos = () => {
        if(this.state.campeonatos.length>0 ){
            const rol = sessionStorage.getItem('rol');
            if(rol === 'admin') {
                return this.state.campeonatos.map((item,index)=>(
                    <MenuItem key={index} value={encode(item[1].nombre)} primaryText={item[1].nombre}/>
                ))
            } else {
                return this.state.campeonatos.map((item,index)=>(
                    <MenuItem key={index} value={item[0]} primaryText={item[1].nombre}/>
                ))
            }
        }
        return null;
    }
    render() {
        let dialogButton = this.state.currentName!==''?<Dialogo btnLabel="Guardar" btnType="raised"  dialogText = "Está seguro que desea guardar esta información?" confirm={this.handleEditCurrent} />:'';
        let dialogButtonDelete = <Dialogo btnLabel="Eliminar" deleteButton={true} dialogText = "Está seguro que desea eliminar esta información?" confirm={this.deleteCurrent} />
        let detalle= this.state.users!==null&&Object.entries(this.state.users).map((item,index)=>{
            let element = item[1];
            let key = item[0];
            element.key=key;
            return element;
          });
          return this.state.loading === true ? <div className="content"><CircularProgress color={green400} size={100} thickness={7} /></div> : (
            <div className="App">
                <PageTitle 
                    title="Tu Fútbol"
                    isLoggedIn={true}
                    hasSettingsMenu ={false}
                />
                <br />
                <div style={{width: '100%'}}>
                    <Paper className="paperCanchas" zDepth={2} style={{width: '100%'}}>
                    <Toolbar style={{width: '100%',  maxWidth: '100%'}}>
                        <ToolbarGroup>
                        {
                            this.state.isEditing === true ?
                            <IconButton iconClassName="material-icons" style={{textAlign:'left',color:'#000'}} tooltip="Volver" onClick={this.back}>arrow_back_ios</IconButton>
                            :
                            <Link to={"/"}><IconButton style={{textAlign:'left',color:'#000'}} tooltip="Atras" iconClassName="material-icons">arrow_back_ios</IconButton></Link>
                        }
                            <ToolbarTitle text={this.state.title} style={{color:'#000'}} className="toolbarTitle"/>
                        </ToolbarGroup>
                        {
                            this.state.isEditing === false ?
                                <IconButton iconClassName="material-icons" style={{textAlign:'right',color:'#000'}} tooltip="Agregar" onClick={this.addNew}>add</IconButton>
                            :
                                <div>
                                {
                                    this.state.current!==null && dialogButtonDelete
                                }
                                </div>
                        }
                    </Toolbar>
                    <br />
                    {
                        this.state.isEditing === false ?
                            <ReactTable 
                                data={detalle}
                                defaultPageSize={10}
                                previousText= 'Anterior'
                                nextText= 'Siguiente'
                                loadingText= 'Cargando...'
                                noDataText= 'No hay datos'
                                pageText= 'Página'
                                ofText= 'de'
                                rowsText= 'Filas'
                                pageJumpText= 'Ir a página'
                                rowsSelectorText= 'filas por página'
                                filterable 
                                defaultFilterMethod={(filter, row) =>String(row[filter.id]).toLowerCase().includes( filter.value.toLowerCase())}
                                columns={[
                                {
                                    columns: [
                                    {
                                        Header: "Nombre",
                                        accessor: "name",
                                    },
                                    {
                                        Header: "Email",
                                        accessor: "email",
                                    },
                                    {
                                        Header: "Rol",
                                        accessor: "rol",
                                        Cell:row=>(row.value.charAt(0).toUpperCase() + row.value.slice(1))
                                    },
                                    {
                                        Header:'Editar',
                                        accessor:'key',
                                        width:60,
                                        filterable: false,
                                        Cell: row => (
                                            <IconButton tooltip="Editar usuario" iconClassName="material-icons" onClick={()=>this.handleEdit(row.original,row.original.key)}>edit</IconButton>
                                        )
                                    }
                                ]
                                }
                                ]}
                                className={"-striped -highlight table"}
                            />
                        :
                        <div>
                        <Table bodyStyle={{overflow:'visible'}}  selectable={false}>
                            <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
                                <TableRow>
                                    <TableHeaderColumn className="smallWidth">Info</TableHeaderColumn>
                                    <TableHeaderColumn>Dato</TableHeaderColumn>
                                </TableRow>
                            </TableHeader>
                            <TableBody displayRowCheckbox={false}>
                                <TableRow >
                                    <TableRowColumn className="smallWidth">Rol</TableRowColumn>
                                    <TableRowColumn>
                                      
                                      {
                                        sessionStorage.getItem('rol') === 'superadmin' ?
                                        <SelectField
                                            floatingLabelText="Rol"
                                            value={this.state.currentRol}
                                            onChange={(e, index, value) => this.setState({currentRol: value})}
                                        >
                                            <MenuItem value={'superadmin'} primaryText="Super Administrador" />
                                            <MenuItem value={'admin'} primaryText="Administrador" />
                                            <MenuItem value={'vocal'} primaryText="Vocal" />
                                            <MenuItem value={'sancionador'} primaryText="Sancionador" />
                                        </SelectField>
                                        :
                                        <SelectField
                                            floatingLabelText="Rol"
                                            value={this.state.currentRol}
                                            onChange={(e, index, value) => this.setState({currentRol: value})}
                                        >
                                            <MenuItem value={'vocal'} primaryText="Vocal" />
                                            <MenuItem value={'sancionador'} primaryText="Sancionador" />
                                        </SelectField>
                                      }
                                        
                                    </TableRowColumn>
                                </TableRow>
                                {
                                    this.state.currentRol === 'admin' &&
                                    <TableRow >
                                        <TableRowColumn className="smallWidth">Ligas Asignadas</TableRowColumn>
                                        <TableRowColumn>
                                        <SelectField
                                            floatingLabelText="Ligas Asignadas"
                                            multiple
                                            fullWidth
                                            value={this.state.ligasAsignadas}
                                            onChange={(e, index, value) => this.setState({ligasAsignadas: value})}
                                        >
                                            {
                                                this.state.ligas.length>0 && this.state.ligas.map((item,index)=>(
                                                    <MenuItem key={index} value={item[0]} primaryText={item[1].nombre}/>
                                                ))
                                            }
                                        </SelectField>
                                        </TableRowColumn>
                                    </TableRow>
                                }
                                {
                                    (this.state.currentRol === 'vocal' || this.state.currentRol === 'sancionador') &&
                                    <TableRow >
                                        <TableRowColumn className="smallWidth">Campeonatos asignados</TableRowColumn>
                                        <TableRowColumn>
                                            <SelectField
                                                floatingLabelText="Campeonatos Asignadas"
                                                multiple
                                                fullWidth
                                                value={this.state.campeonatosAsignados}
                                                onChange={(e, index, value) => this.setState({campeonatosAsignados: value})}
                                            >
                                                {this.renderCampeonatos()}
                                            </SelectField>
                                        </TableRowColumn>
                                    </TableRow>
                                }
                                <TableRow >
                                    <TableRowColumn className="smallWidth">Nombre y Apellido</TableRowColumn>
                                    <TableRowColumn>
                                        <TextField
                                            hintText="Nombre"
                                            fullWidth={true}
                                            value={this.state.currentName}
                                            onChange={e => this.setState({currentName: e.target.value})}
                                        />
                                    </TableRowColumn>
                                </TableRow>
                                <TableRow >
                                    <TableRowColumn className="smallWidth">Email</TableRowColumn>
                                    <TableRowColumn>
                                        <TextField
                                            hintText="Email"
                                            fullWidth={true}
                                            value={this.state.currentEmail}
                                            disabled={!this.state.addingNewone}
                                            onChange={e => this.setState({currentEmail: e.target.value})}
                                        />
                                    </TableRowColumn>
                                </TableRow>
                                
                                  <TableRow >
                                      <TableRowColumn className="smallWidth">Password</TableRowColumn>
                                      <TableRowColumn>
                                      {
                                        this.state.addingNewone ?
                                                <TextField
                                                    hintText="Password"
                                                    fullWidth={true}
                                                    type="password"
                                                    value={this.state.password}
                                                    onChange={e => this.setState({password: e.target.value})}
                                                />
                                            :
                                                <RaisedButton label="Recuperar contraseña" onClick={this.sendEmailResetPassword}/>
                                        }   
                                        </TableRowColumn>
                                  </TableRow>
                            </TableBody>
                        </Table>
                        {dialogButton}
                        </div>
                    }
                    </Paper>
                </div>
            </div>
        )
    }
}


