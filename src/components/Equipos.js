import React, { Component } from 'react';
import '../utils/Style.css';
import ActionUpload from 'material-ui/svg-icons/file/cloud-upload';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import PageTitle from './PageTitle';
import 'react-table/react-table.css';
import ReactTable from "react-table";
import CircularProgress from 'material-ui/CircularProgress';
import Paper from 'material-ui/Paper';
import FileUploader from 'react-firebase-file-uploader';
import TextField from 'material-ui/TextField';
import IconButton from 'material-ui/IconButton';
import Dialogo from '../utils/Dialogo';
import {Table,TableBody,TableHeader,TableHeaderColumn,TableRow,TableRowColumn} from 'material-ui/Table';
import {editEquipo, addEquipo,getEquipos,getBarriales, getCategorías,deleteEquipo} from '../utils/Controller';
import {storage} from '../utils/Firebase';
import {green400} from 'material-ui/styles/colors'
import RaisedButton from 'material-ui/RaisedButton';
import ReactTooltip from 'react-tooltip';
import placeholder from '../assets/placeholder.png';
import soccer from '../assets/soccer.png';
import {Link} from 'react-router-dom';
import {Toolbar, ToolbarGroup,  ToolbarTitle} from 'material-ui/Toolbar';
const today = new Date();
export default class Equipos extends Component {
    constructor(props){
        super(props);
        this.state={
            keyEquipo:null,
            equipos:null,
            current:null,
            isEditing:false,
            currentNombre:'',
            currentNotas:'',
            barriales:null,
            categorías:null,
            currentBarrial:'',
            currentCategorías:[],
            currentFechaInicio:(today),
            currentCoordenadas:'',
            currentActive:false,
            currentDestacado:false,
            currentPrecio:'',
            currentDateAdded:'',
            currentType:'',
            escudo:null,
            key:null,
            addingNewone:false,
            title:"Equipos",
            loading:true,
            addedEscudo:false,
            isUploading: false,
            progress:0,
            tab:null,
            escudoUploaded:false,
        };
    }

    handleUploadStart = () => this.setState({isUploading: true, progress: 0});
    handleProgress = (progress) => this.setState({progress});
    handleUploadError = (error) => {
        this.setState({isUploading: false});
        console.error(error);
        alert(error);
    }
    handleUploadSuccess = (filename) => {
        this.setState({escudo: filename,escudoUploaded:true, progress: 100, isUploading: false});
        storage.ref('Equipos').child(filename).getDownloadURL().then(url => this.setState({escudo: url}));
    };
   
    back = () => {
        this.setState({
            equipos:null, 
            keyEquipo:'',
            current:null,
            isEditing:false,
            currentNombre:'',
            currentNotas:'',
            barriales:null,
            categorías:null,
            currentCategorías:[],
            currentFechaInicio:'',
            currentCoordenadas:'',
            currentActive:false,
            currentDestacado:false,
            currentPrecio:'',
            escudo:null,
            currentDateAdded:'',
            currentType:'',
            key:null,
            addingNewone:false,
            title:"Equipos",
            loading:true,
            addedEscudo:false,
            isUploading: false,
            progress:0,
            tab:null,
            escudoUploaded:false,
        });
        this.startOver();
    }
    startOver(){
        let keyLiga = this.props.match.params.keyLiga;
        this.setState({keyLiga});
        getBarriales().then(value=>{
            this.setState({barriales:Object.values(value.toJSON())})
        }).catch(error=>{
            console.log('error: ', error);
        })
        getCategorías().then(value=>{
            this.setState({categorías:Object.values(value.toJSON())})
        }).catch(error=>{
            console.log('error: ', error);
        })
        getEquipos(keyLiga).then(value=>{
            let equipos = value.toJSON();
            this.setState({equipos,loading:false})
        }).catch(error=>{
            console.log('error: ', error);
        })
    }
    deleteCurrent = () =>{
        let valid = deleteEquipo(this.state.keyLiga,this.state.key);
        if(valid){
            this.back();
            this.startOver();
        }
    }
    handleEdit = (current,key) => {
        this.setState({
            current,
            isEditing:true,
            key,
            title:"Editar: " + current.nombre,
            currentJugadores:current.jugadores?current.jugadores:'',
            currentNombre:current.nombre?current.nombre:'',
            currentNotas:current.notas?current.notas:'',
            currentBarrial:current.barrial?current.barrial:'',
            currentCategorías:current.categorías?Object.values(current.categorías):'',
            currentDateAdded:current.dateAdded?current.dateAdded:'',
            escudo:current.escudo?current.escudo:'',
        })
    }
    componentDidMount() {
        this.setState({currentBarrial:this.props.match.params.keyLiga});
    }
    handleChangeCategorías =  (event, index, values) => this.setState({currentCategorías:values});
    handleEditCurrent = () =>{
        let valid = false;
        let dat = new Date();
        this.setState({loading:true});
        if(this.state.addingNewone===true){
            let editing = {
                jugadores:this.state.currentJugadores?this.state.currentJugadores:[],
                notas:this.state.currentNotas,
                nombre:this.state.currentNombre,
                barrial:this.state.currentBarrial,
                categorías:this.state.currentCategorías,
                escudo:this.state.escudo,
                dateAdded:dat.getTime(),
            }
            valid = addEquipo(this.state.keyLiga,editing);
        }
        else{
            let editing = {
                notas:this.state.currentNotas,
                jugadores:this.state.currentJugadores,
                nombre:this.state.currentNombre,
                barrial:this.state.currentBarrial,
                categorías:this.state.currentCategorías,
                dateAdded:this.state.currentDateAdded,
                dateModified:dat.getTime(),
                escudo:this.state.escudo,
            };
            valid = editEquipo(this.state.keyLiga,editing,this.state.key);
        }
        if(valid){
            this.back();
        }
       this.startOver();
    }
    addNew = () =>{
        this.setState({isEditing:true,addingNewone:true});
    }
    componentWillMount(){
        this.startOver();
    }
    render() {
        let dialogButton = this.state.currentNombre!==''?this.state.currentCategorías.length>0?<Dialogo btnLabel="Guardar" btnType="raised"  dialogText = "Está seguro que desea guardar esta información?" confirm={this.handleEditCurrent} />:'':'';
        let dialogButtonDelete = <Dialogo btnLabel="Eliminar" deleteButton={true} dialogText = "Está seguro que desea eliminar esta información?" confirm={this.deleteCurrent} />
        let categorías = this.state.categorías!==null && Object.values(this.state.categorías).map((item,index)=>{
            return(
                <MenuItem value={item.nombre} primaryText={item.nombre} key={index} />
            )
        });
        let detalle= this.state.equipos!==null&&Object.entries(this.state.equipos).map((item,index)=>{
            let element = item[1];
            let key = item[0];
            element.key=key;
            return element;
        });
          return this.state.loading === true ? <div className="content"><CircularProgress color={green400} size={100} thickness={7} /></div> : (
            <div className="App">
                <PageTitle 
                    title="Tu Fútbol"
                    isLoggedIn={true}
                    hasSettingsMenu ={false}
                />
                <br />
                <div style={{width: '100%'}}>
                    <Paper className="paperCanchas" zDepth={2} style={{width: '100%'}}>
                    <Toolbar style={{width: '100%',  maxWidth: '100%'}}>
                        <ToolbarGroup>
                        {
                            this.state.isEditing === true ?
                            <IconButton iconClassName="material-icons" style={{textAlign:'left',color:'#000'}} tooltip="Atras" onClick={this.back}>arrow_back_ios</IconButton>
                            :
                            <Link to={"/ligas"}><IconButton style={{textAlign:'left',color:'#000'}} tooltip="Atras" iconClassName="material-icons">arrow_back_ios</IconButton></Link>
                        }
                            <ToolbarTitle text={this.state.title} style={{color:'#000'}} className="toolbarTitle"/>
                        </ToolbarGroup>
                        {
                            this.state.isEditing === false ?
                            <IconButton iconClassName="material-icons" style={{textAlign:'right',color:'#000'}} tooltip="Agregar" onClick={this.addNew}>add</IconButton>
                            :
                            <div>
                                {this.state.current!==null && dialogButtonDelete}
                            </div>
                            
                        }
                    </Toolbar>
                    <br />
                    {
                        this.state.isEditing === false ?
                            <div>
                            {
                                detalle.length>0&&   
                                <ReactTable 
                                    data={detalle}
                                    defaultPageSize={10}
                                    previousText= 'Anterior'
                                    nextText= 'Siguiente'
                                    loadingText= 'Cargando...'
                                    noDataText= 'No hay datos'
                                    pageText= 'Página'
                                    ofText= 'de'
                                    rowsText= 'Filas'
                                    pageJumpText= 'Ir a página'
                                    rowsSelectorText= 'filas por página'
                                    filterable 
                                    defaultFilterMethod={(filter, row) =>String(row[filter.id]).toLowerCase().includes( filter.value.toLowerCase())}
                                    columns={[
                                    {
                                        columns: [
                                        {
                                            Header: "Escudo",
                                            accessor: "escudo",
                                            width:85,
                                            Cell: row=> (
                                                row.original.escudo?
                                                    <a href={row.original.escudo} target="_blank"><img src={row.original.escudo} alt="logo" style={{margin:10, maxWidth:75, maxHeight:75}}/></a>
                                                :
                                                    <img src={placeholder} alt="logo" style={{margin:10, maxWidth:75, maxHeight:75}}/>
                                            ),
                                            filterable: false
                                        },
                                        {
                                            Header: "Nombre",
                                            accessor: "nombre",
                                        },
                                        {
                                            Header:'Editar',
                                            accessor:'key',
                                            width:60,
                                            filterable: false,
                                            Cell: row => (
                                                <IconButton tooltip="Editar equipo" iconClassName="material-icons" onClick={()=>this.handleEdit(row.original,row.original.key)}>edit</IconButton>
                                            )
                                        },
                                        {
                                            Header:'Jugadores',
                                            accessor:'key',
                                            width:90,
                                            filterable: false,
                                            Cell: row => (
                                                <Link to={"/jugadores/"+this.state.keyLiga+"/"+row.original.key}>
                                                    <img alt="logo" data-tip="Jugadores" src={soccer} style={{width: 50, height: 50}}/>
                                                    <ReactTooltip place="top" type="dark" effect="float"/>
                                                </Link>
                                            )
                                        }
                                    ]
                                    }
                                    ]}
                                    className={"-striped -highlight table"}
                                />
                            }
                            </div>
                        :
                        <div>
                        <Table bodyStyle={{overflow:'visible'}}  selectable={false}>
                            <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
                                <TableRow>
                                    <TableHeaderColumn className="smallWidth">Info</TableHeaderColumn>
                                    <TableHeaderColumn>Dato</TableHeaderColumn>
                                </TableRow>
                            </TableHeader>
                            <TableBody displayRowCheckbox={false}>
                                <TableRow>
                                    <TableRowColumn className="smallWidth">Barrial</TableRowColumn>
                                    <TableRowColumn>
                                        {this.state.currentBarrial}
                                    </TableRowColumn>
                                </TableRow>
                                <TableRow >
                                    <TableRowColumn className="smallWidth">Nombre</TableRowColumn>
                                    <TableRowColumn>
                                        <TextField
                                            hintText="Nombre"
                                            fullWidth={true}
                                            value={this.state.currentNombre}
                                            onChange={e => this.setState({currentNombre: e.target.value})}
                                        />
                                    </TableRowColumn>
                                </TableRow>
                                <TableRow>
                                    <TableRowColumn className="smallWidth">Categorías</TableRowColumn>
                                    <TableRowColumn>
                                        <SelectField
                                            multiple={true}
                                            floatingLabelText="Categorías"
                                            value={this.state.currentCategorías}
                                            onChange={this.handleChangeCategorías}
                                            fullWidth
                                        >
                                            {categorías}
                                        </SelectField>
                                    </TableRowColumn>
                                </TableRow>
                                    <TableRow>
                                    <TableRowColumn className="smallWidth">Notas</TableRowColumn>
                                    <TableRowColumn >
                                        <TextField
                                            hintText="Notas"
                                            multiLine={true}
                                            rows={2}
                                            rowsMax={5}
                                            fullWidth={true}
                                            value={this.state.currentNotas}
                                            onChange={e => this.setState({currentNotas: e.target.value})}
                                        />
                                    </TableRowColumn>
                                </TableRow>
                                <TableRow>
                                    <TableRowColumn className="smallWidth">Escudo</TableRowColumn>
                                    <TableRowColumn>
                                    {
                                        this.state.isUploading &&
                                            <p>Progreso: {this.state.progress}</p>
                                    }
                                    {
                                        this.state.escudo &&
                                            <div>
                                                <a target="_blank" href={this.state.escudo}>
                                                    <img src={this.state.escudo} style={{margin:20,width:150,height:150}} alt="img"/>
                                                </a>
                                                <br />
                                            </div>
                                    }
                                    {
                                        this.state.escudoUploaded === false &&
                                    
                                        <RaisedButton
                                            label="Escudo"
                                            labelPosition="before"
                                            style={{margin:20}}
                                            containerElement="label"
                                            icon={<ActionUpload />}
                                        >   
                                            <FileUploader
                                                hidden
                                                accept="file/*"
                                                name="escudo"
                                                randomizeFilename
                                                storageRef={storage.ref('Equipos')}
                                                onUploadStart={this.handleUploadStart}
                                                onUploadError={this.handleUploadError}
                                                onUploadSuccess={this.handleUploadSuccess}
                                                onProgress={this.handleProgress}
                                            />
                                        </RaisedButton>
                                        
                                    }
                                    </TableRowColumn>
                                </TableRow>
                            </TableBody>
                        </Table>
                        {dialogButton}
                        </div>
                    }
                    </Paper>
                </div>
            </div>
        )
    }
}


