import React, { Component } from 'react';
import '../utils/Style.css';
import PageTitle from './PageTitle';
import RaisedButton from 'material-ui/RaisedButton';
import CircularProgress from 'material-ui/CircularProgress';
import Paper from 'material-ui/Paper';
import {Link} from 'react-router-dom';
import { green400 } from 'material-ui/styles/colors';
import {usersRef} from '../utils/Controller.js';

export default class Index extends Component {
    constructor(props){
        super(props);
        this.state={
            loading:false,
            user: null
        };
    }
    componentWillMount(){
        usersRef.on('value', snapshot => {
            let uid = sessionStorage.getItem('uid');
            if (uid !== null) {
                let users = snapshot.toJSON();
                let user = users[uid];
                if(user) {
                    if(user.rol === 'admin') {
                        const ligasAsignadas = Object.values(user.ligasAsignadas);
                        sessionStorage.setItem('ligasAsignadas',JSON.stringify(ligasAsignadas));
                    }
                    else if(user.rol === 'vocal' || user.rol === 'sancionador') {
                        const campeonatosAsignados = Object.values(user.campeonatosAsignados);
                        sessionStorage.setItem('campeonatosAsignados',JSON.stringify(campeonatosAsignados));
                    }
                    sessionStorage.setItem('rol',user.rol);
                    sessionStorage.setItem('user',JSON.stringify(user));
                    this.setState({user});
                }
            }
        })
    }

    renderCategoryButton = () => {
        const {user} = this.state;
        if(user) {
            if (user.rol === 'superadmin') {
                return (
                    <div style={{marginTop:20}}>
                        <Link to="categorias"><RaisedButton label="Categorías" ></RaisedButton></Link>
                    </div>
                )
            }
        }
        return null;
    }

    renderPublicidadButton = () => {
        const {user} = this.state;
        if(user) {
            if (user.rol === 'superadmin') {
                return (
                    <div style={{marginTop:20}}>
                        <Link to="publicidad"><RaisedButton label="Publicidad" ></RaisedButton></Link>
                    </div>
                )
            }
        }
        return null;
    }
    renderLogsButton = () => {
        const {user} = this.state;
        if(user) {
            if (user.rol === 'superadmin') {
                return (
                    <div style={{marginTop:20}}>
                        <Link to="logs"><RaisedButton label="Logs" ></RaisedButton></Link>
                    </div>
                )
            }
        }
        return null;
    }
    renderLeaguesButton = () => {
        const {user} = this.state;
        if(user) {
            if (user.rol === 'admin' || user.rol === 'superadmin') {
                return (
                    <div style={{marginTop:20}}>
                        <Link to="ligas"><RaisedButton label="Ligas" ></RaisedButton></Link>
                    </div>
                )
            }
        }
        return null;
    }
    renderUsersButton = () => {
        const {user} = this.state;
        if(user) {
            if (user.rol === 'admin' || user.rol === 'superadmin') {
                return (
                    <div style={{marginTop:20}}>
                        <Link to="usuarios"><RaisedButton label="Usuarios" ></RaisedButton></Link>
                    </div>
                )
            }
        }
        return null;
    }

    render() {
        return this.state.loading === true ? <div className="content"><CircularProgress color={green400} size={100} thickness={7} /></div> : (
            <div className="App">
                <PageTitle 
                    title="Tu Fútbol"
                    isLoggedIn={true}
                    hasSettingsMenu ={false}
                />
                <br /><div>&nbsp;<br />&nbsp;</div>
                {
                    this.state.user !== null &&
                        <div>
                            <div style={{fontFamily: 'Poppins', fontWeight: 'bold', fontSize: 20}}>Bienvenido {' '+ this.state.user.name}</div>
                            <div style={{fontFamily: 'Poppins'}}><b>Rol: </b>{this.state.user.rol.charAt(0).toUpperCase() + this.state.user.rol.slice(1)}</div>
                            <br /><div>&nbsp;<br />&nbsp;</div>
                            <Paper className="loginContainer" zDepth={1}>
                                <div style={{marginTop:20}}>
                                    <Link to="campeonatos"><RaisedButton label="Campeonatos" ></RaisedButton></Link>
                                </div>
                                {this.renderLeaguesButton()}
                                {this.renderCategoryButton()}
                                {this.renderPublicidadButton()}
                                {this.renderUsersButton()}
                                {this.renderLogsButton()}
                            </Paper>
                        </div>
                }
            </div>
        )
    }
}

