import React, { Component } from 'react';
import '../utils/Style.css';
import ActionUpload from 'material-ui/svg-icons/file/cloud-upload';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import PageTitle from './PageTitle';
import DatePicker from 'material-ui/DatePicker';
import 'react-table/react-table.css';
import ReactTable from "react-table";
import CircularProgress from 'material-ui/CircularProgress';
import Paper from 'material-ui/Paper';
import FileUploader from 'react-firebase-file-uploader';
import TextField from 'material-ui/TextField';
import IconButton from 'material-ui/IconButton';
import Dialogo from '../utils/Dialogo';
import {Table,TableBody,TableHeader,TableHeaderColumn,TableRow,TableRowColumn} from 'material-ui/Table';
import {editJugador, addJugador,getEquipoDetail, getCategorías,formatDate,deleteJugador, ligasRef, encode} from '../utils/Controller';
import {storage} from '../utils/Firebase';
import {green400} from 'material-ui/styles/colors'
import RaisedButton from 'material-ui/RaisedButton';
import moment from 'moment';
import {Link} from 'react-router-dom';
import {Toolbar, ToolbarGroup,  ToolbarTitle} from 'material-ui/Toolbar';
const today = new Date();
export default class Jugadores extends Component {
    constructor(props){
        super(props);
        this.state={
            allPlayers:[],
            keyEquipo:null,
            equipo:null,
            jugadores:null,
            current:null,
            isEditing:false,
            currentNombre:'',
            currentNúmero:'',
            currentCédula:'',
            currentFechaDeNacimiento:moment(),
            currentNotas:'',
            categorías:null,
            currentCategorías:[],
            currentFechaInicio:(today),
            currentCoordenadas:'',
            currentActive:false,
            currentDestacado:false,
            currentPrecio:'',
            currentDateAdded:'',
            currentType:'',
            foto:null,
            key:null,
            addingNewone:false,
            title:"Jugadores",
            loading:true,
            addedFoto:false,
            isUploading: false,
            progress:0,
            tab:null,
            fotoUploaded:false,
        };
    }

    handleUploadStart = () => this.setState({isUploading: true, progress: 0});
    handleProgress = (progress) => this.setState({progress});
    handleUploadError = (error) => {
        this.setState({isUploading: false});
        console.error(error);
        alert(error);
    }
    handleUploadSuccess = (filename) => {
        this.setState({foto: filename,fotoUploaded:true, progress: 100, isUploading: false});
        storage.ref('Jugadores').child(filename).getDownloadURL().then(url => this.setState({foto: url}));
    };
    back = () => {
        this.setState({
            equipo:null, 
            jugadores:null,
            keyEquipo:'',
            current:null,
            isEditing:false,
            currentNombre:'',
            currentCédula:'',
            currentNúmero: '',
            currentFechaDeNacimiento:moment(),
            currentNotas:'',
            categorías:null,
            currentCategorías:[],
            currentFechaInicio:'',
            currentCoordenadas:'',
            currentActive:false,
            currentDestacado:false,
            currentPrecio:'',
            foto:null,
            currentDateAdded:'',
            currentType:'',
            key:null,
            addingNewone:false,
            title:"Jugadores",
            loading:true,
            addedFoto:false,
            isUploading: false,
            progress:0,
            tab:null,
            fotoUploaded:false,

        });
        this.startOver();
    }
    startOver(){
        
        let keyLiga = this.props.match.params.keyLiga;
        let keyEquipo = this.props.match.params.keyEquipo;
        this.setState({keyLiga,keyEquipo});
        let ref = encode(keyLiga) + '/equipos/'+ encode(keyEquipo) +'/jugadores/';
        ligasRef.child(ref).on('value', snapshot=>{
            let allPlayers = snapshot.toJSON();
            if(allPlayers && allPlayers !== "") {
                allPlayers = Object.values(allPlayers)
                this.setState({allPlayers})
            }            
        })
        getCategorías().then(value=>{
            this.setState({categorías:Object.values(value.toJSON())})
        }).catch(error=>{
            console.log('error: ', error);
        })
        getEquipoDetail(keyLiga,keyEquipo).then(value=>{
            let equipo = value.toJSON();
            if(equipo.jugadores){
                this.setState({jugadores:equipo.jugadores})
            }
            this.setState({equipo,loading:false})
        }).catch(error=>{
            console.log('error: ', error);
        })
    }
    deleteCurrent = () =>{
        let valid = deleteJugador(this.state.keyLiga,this.state.keyEquipo,this.state.key);
        if(valid){
            this.back();
            this.startOver();
        }
    }
    isFree =(númber) => {
        const {allPlayers, current} = this.state;
        if(allPlayers.length>0){
            const players = current === null ? allPlayers : allPlayers.filter(obj => obj.nombre !== current.nombre);
            let numbers = players.map(obj=>obj.número);
            if(numbers.includes(númber)){
                return false;
            }
        }
        return true;
    }
    isFreeCI =(ci) => {
        const {allPlayers, current} = this.state;
        if(allPlayers.length>0){
            const players = current === null ? allPlayers : allPlayers.filter(obj => obj.nombre !== current.nombre);
            let cis = players.map(obj=>obj.cédula);
            if(cis.includes(ci)){
                return false;
            }
        }
        return true;
    }
    handleEdit = (current,key) => {
        let fechaDeNacimiento = current.fechaDeNacimiento?new Date(current.fechaDeNacimiento):formatDate(new Date());
        this.setState({
            current,
            isEditing:true,
            key,
            title:"Editar: " + current.nombre,
            currentNombre:current.nombre?current.nombre:'',
            currentNúmero:current.número?current.número:'',
            currentCédula:current.cédula?current.cédula:'',
            currentFechaDeNacimiento:fechaDeNacimiento,
            currentNotas:current.notas?current.notas:'',
            currentCategorías:current.categorías?Object.values(current.categorías):'',
            currentDateAdded:current.dateAdded?current.dateAdded:'',
            foto:current.foto?current.foto:'',
        })
    }
    handleChangeCategorías =  (event, index, values) => this.setState({currentCategorías:values});
    handleChangeBirthdate =  (event, currentFechaDeNacimiento) => {
        this.setState({currentFechaDeNacimiento});
      };
    handleEditCurrent = () =>{
        let valid = false;
        let dat = new Date();
        let fechaDeNacimiento = moment(this.state.currentFechaDeNacimiento).format('YYYY-MM-DD');
        const número = this.state.currentNúmero;
        const cédula = this.state.currentCédula;
        if(this.isFree(número) && this.isFreeCI(cédula)) {
            this.setState({loading:true});
            if(this.state.addingNewone===true){
                let editing = {
                    notas:this.state.currentNotas,
                    nombre:this.state.currentNombre,
                    fechaDeNacimiento,
                    número,
                    cédula,
                    categorías:this.state.currentCategorías,
                    foto:this.state.foto,
                    dateAdded:dat.getTime(),
                }
                valid = addJugador(this.state.keyLiga,this.state.keyEquipo,editing);
            }
            else{
                let editing = {
                    notas:this.state.currentNotas,
                    nombre:this.state.currentNombre,
                    fechaDeNacimiento,
                    número:this.state.currentNúmero,
                    cédula:this.state.currentCédula,
                    categorías:this.state.currentCategorías,
                    dateAdded:this.state.currentDateAdded,
                    dateModified:dat.getTime(),
                    foto:this.state.foto,
                };
                valid = editJugador(this.state.keyLiga,this.state.keyEquipo,editing,this.state.key);
            }
            if(valid){
                this.back();
            }
           this.startOver();
        }
        else {
            if(!this.isFree(número)){
                alert('El número: '+número+' no está disponible');
            }
            if(!this.isFreeCI(cédula)){
                alert('El número de cédula: '+cédula+' no está disponible');
            }
            
        }
       
    }
    addNew = () =>{
        this.setState({isEditing:true,addingNewone:true});
    }
    componentWillMount(){
        this.startOver();
    }
    render() {
        let dialogButton = this.state.currentNombre!==''?this.state.currentCategorías.length>0?this.state.currentNúmero!==''?this.state.currentCédula!==''?<Dialogo btnLabel="Guardar" btnType="raised"  dialogText = "Está seguro que desea guardar esta información?" confirm={this.handleEditCurrent} />:'':'':'':'';
        let dialogButtonDelete = <Dialogo btnLabel="Eliminar" deleteButton={true} dialogText = "Está seguro que desea eliminar esta información?" confirm={this.deleteCurrent} />
        let categorías = this.state.categorías!==null && Object.values(this.state.categorías).map((item,index)=>{
            return(
                <MenuItem value={item.nombre} primaryText={item.nombre} key={index} />
            )
        });
        let detalle= this.state.jugadores!==null&&Object.entries(this.state.jugadores).map((item,index)=>{
            let element = item[1];
            let key = item[0];
            element.key=key;
            return element;
          });
          return this.state.loading === true ? <div className="content"><CircularProgress color={green400} size={100} thickness={7} /></div> : (
            <div className="App">
                <PageTitle 
                    title="Tu Fútbol"
                    isLoggedIn={true}
                    hasSettingsMenu ={false}
                />
                <br />
                <div style={{width: '100%'}}>
                    <Paper className="paperCanchas" zDepth={2} style={{width: '100%'}}>
                    <Toolbar style={{width: '100%',  maxWidth: '100%'}}>
                        <ToolbarGroup>
                        {
                            this.state.isEditing === true ?
                            <IconButton iconClassName="material-icons" style={{textAlign:'left',color:'#000'}} tooltip="Atras" onClick={this.back}>arrow_back_ios</IconButton>
                            :
                            <Link to={"/equipos/"+this.state.keyLiga}><IconButton style={{textAlign:'left',color:'#000'}} iconClassName="material-icons" tooltip="Atras">arrow_back_ios</IconButton></Link>
                        }
                            <ToolbarTitle text={this.state.title} style={{color:'#000'}} className="toolbarTitle"/>
                        </ToolbarGroup>
                        {
                            this.state.isEditing === false ?
                            <IconButton iconClassName="material-icons" style={{textAlign:'right',color:'#000'}} tooltip="Agregar" onClick={this.addNew}>add</IconButton>
                            :
                            <div>
                            {this.state.current!==null && dialogButtonDelete}
                            </div>
                            
                        }
                    </Toolbar>
                    <br />
                    {
                        this.state.isEditing === false ?
                        <div>
                        {
                            detalle.length>0&&   
                            <ReactTable 
                                data={detalle}
                                defaultPageSize={10}
                                previousText= 'Anterior'
                                nextText= 'Siguiente'
                                loadingText= 'Cargando...'
                                noDataText= 'No hay datos'
                                pageText= 'Página'
                                ofText= 'de'
                                rowsText= 'Filas'
                                pageJumpText= 'Ir a página'
                                rowsSelectorText= 'filas por página'
                                filterable 
                                defaultFilterMethod={(filter, row) =>String(row[filter.id]).toLowerCase().includes( filter.value.toLowerCase())}
                                columns={[
                                {
                                    columns: [
                                    {
                                        Header: "Foto",
                                        accessor: "foto",
                                        width:85,
                                        filterable: false,
                                        Cell: row=> (
                                            row.original.foto?<a href={row.original.foto} target="_blank"><img src={row.original.foto} alt="logo" style={{margin:10, maxWidth:75, maxHeight:75}}/></a>:''
                                        )
                                    },
                                    {
                                        Header: "Nombre",
                                        accessor: "nombre",
                                    },
                                    {
                                        Header: "Cédula",
                                        accessor: "cédula",
                                        width:105
                                    },
                                    {
                                        Header: "Número",
                                        accessor: "número",
                                        width:85
                                    },
                                    {
                                        Header:'Editar',
                                        accessor:'key',
                                        width:70,
                                        filterable: false,
                                        Cell: row => (
                                            <IconButton tooltip="Editar jugador" iconClassName="material-icons" onClick={()=>this.handleEdit(row.original,row.original.key)}>edit</IconButton>
                                        )
                                    },
                                ]
                                }
                                ]}
                                className={"-striped -highlight table"}
                            />
                        }
                        </div>
                        :
                        <div>
                        <Table bodyStyle={{overflow:'visible'}}  selectable={false}>
                            <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
                                <TableRow>
                                    <TableHeaderColumn className="smallWidth">Info</TableHeaderColumn>
                                    <TableHeaderColumn>Dato</TableHeaderColumn>
                                </TableRow>
                            </TableHeader>
                            <TableBody displayRowCheckbox={false}>
                                <TableRow >
                                    <TableRowColumn className="smallWidth">Liga</TableRowColumn>
                                    <TableRowColumn>
                                       {this.props.match.params.keyLiga}
                                    </TableRowColumn>
                                </TableRow>
                                <TableRow >
                                    <TableRowColumn className="smallWidth">Equipo</TableRowColumn>
                                    <TableRowColumn>
                                       {this.props.match.params.keyEquipo}
                                    </TableRowColumn>
                                </TableRow>
                                <TableRow >
                                    <TableRowColumn className="smallWidth">Nombre</TableRowColumn>
                                    <TableRowColumn>
                                        <TextField
                                            hintText="Nombre"
                                            fullWidth={true}
                                            value={this.state.currentNombre}
                                            onChange={e => this.setState({currentNombre: e.target.value})}
                                        />
                                    </TableRowColumn>
                                </TableRow>
                                <TableRow >
                                    <TableRowColumn className="smallWidth">Fecha de Nacimiento</TableRowColumn>
                                    <TableRowColumn>
                                        <DatePicker 
                                            hintText="Fecha de Nacimiento" 
                                            container="inline"
                                            fullWidth={true}
                                            value={this.state.currentFechaDeNacimiento}
                                            onChange={this.handleChangeBirthdate}
                                        />
                                    </TableRowColumn>
                                </TableRow>
                                <TableRow >
                                    <TableRowColumn className="smallWidth">Cédula</TableRowColumn>
                                    <TableRowColumn>
                                        <TextField
                                            hintText="Número de cédula"
                                            fullWidth={true}
                                            value={this.state.currentCédula}
                                            onChange={e => this.setState({currentCédula: e.target.value})}
                                        />
                                    </TableRowColumn>
                                </TableRow>
                                <TableRow >
                                    <TableRowColumn className="smallWidth">Número</TableRowColumn>
                                    <TableRowColumn>
                                        <TextField
                                            hintText="Número"
                                            fullWidth={true}
                                            value={this.state.currentNúmero}
                                            onChange={e => this.setState({currentNúmero: e.target.value})}
                                        />
                                    </TableRowColumn>
                                </TableRow>
                                <TableRow>
                                    <TableRowColumn className="smallWidth">Categorías</TableRowColumn>
                                    <TableRowColumn>
                                        <SelectField
                                            multiple={true}
                                            floatingLabelText="Categorías"
                                            value={this.state.currentCategorías}
                                            onChange={this.handleChangeCategorías}
                                        >
                                            {categorías}
                                        </SelectField>
                                    </TableRowColumn>
                                </TableRow>
                                    <TableRow>
                                    <TableRowColumn className="smallWidth">Notas</TableRowColumn>
                                    <TableRowColumn >
                                        <TextField
                                            hintText="Notas"
                                            multiLine={true}
                                            rows={2}
                                            rowsMax={5}
                                            fullWidth={true}
                                            value={this.state.currentNotas}
                                            onChange={e => this.setState({currentNotas: e.target.value})}
                                        />
                                    </TableRowColumn>
                                </TableRow>
                                <TableRow>
                                    <TableRowColumn className="smallWidth">Foto</TableRowColumn>
                                    <TableRowColumn>
                                    {
                                        this.state.isUploading &&
                                            <p>Progreso: {this.state.progress}</p>
                                    }
                                    {
                                        this.state.foto &&
                                            <div>
                                                <a target="_blank" href={this.state.foto}>
                                                    <img src={this.state.foto} style={{margin:20,width:150,height:150}} alt="img"/>
                                                </a>
                                                <br />
                                            </div>
                                    }
                                    {
                                        this.state.fotoUploaded === false &&
                                    
                                        <RaisedButton
                                            label="Foto"
                                            labelPosition="before"
                                            style={{margin:20}}
                                            containerElement="label"
                                            icon={<ActionUpload />}
                                        >   
                                            <FileUploader
                                                hidden
                                                accept="file/*"
                                                name="foto"
                                                randomizeFilename
                                                storageRef={storage.ref('Jugadores')}
                                                onUploadStart={this.handleUploadStart}
                                                onUploadError={this.handleUploadError}
                                                onUploadSuccess={this.handleUploadSuccess}
                                                onProgress={this.handleProgress}
                                            />
                                        </RaisedButton>
                                        
                                    }
                                    </TableRowColumn>
                                </TableRow>
                            </TableBody>
                        </Table>
                        {dialogButton}
                        </div>
                    }
                    </Paper>
                </div>
            </div>
        )
    }
}


