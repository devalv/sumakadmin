import React, { Component } from 'react';
import '../utils/Style.css';
import PageTitle from './PageTitle';
import CircularProgress from 'material-ui/CircularProgress';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import IconButton from 'material-ui/IconButton';
import Dialogo from '../utils/Dialogo';
import {Link} from 'react-router-dom';
import {Table,TableBody,TableHeader,TableHeaderColumn,TableRow,TableRowColumn} from 'material-ui/Table';
import {getCategorías,addCategoría,editCategoría,deleteCategory} from '../utils/Controller';
import {green400} from 'material-ui/styles/colors'
import {Toolbar, ToolbarGroup,  ToolbarTitle} from 'material-ui/Toolbar';
import 'react-table/react-table.css';
import ReactTable from "react-table";
export default class Categorías extends Component {
    constructor(props){
        super(props);
        this.state={
            current:null,
            isEditing:false,
            currentNombre:'',
            categorias:null,
            key:null,
            addingNewone:false,
            title:"Categorías",
            loading:true,
        };
    }
   
    back = () => {
        this.setState({
            lcurrent:null,
            isEditing:false,
            currentNombre:'',
            categorias:null,
            key:null,
            addingNewone:false,
            title:"Categorías",
            loading:true,
        });
        this.startOver();
    }
    startOver(){
        getCategorías().then(value=>{
            let categorias = value.toJSON();
            this.setState({categorias,loading:false});
        })
    }
    handleEdit = (current,key) =>{
        this.setState({
            current,
            isEditing:true,
            key,
            title:"Editar",
            currentNombre:current.nombre?current.nombre:'',
        })
    }
    deleteCurrent = () =>{
        let valid = deleteCategory(this.state.key);
        if(valid){
            this.back();
            this.startOver();
        }
    }
    handleEditCurrent = () =>{
        let valid = false;
        this.setState({loading:true});
        if(this.state.addingNewone===true){
            let editing = {
                nombre:this.state.currentNombre,
            }
            valid = addCategoría(editing);
        }
        else{
            let editing = {
                nombre:this.state.currentNombre,
            };
            valid = editCategoría(editing,this.state.key);
        }
        if(valid){
            this.back();
        }
       this.startOver();
    }
    addNew = () =>{
        this.setState({isEditing:true,addingNewone:true});
    }
    componentWillMount(){
        this.startOver();
    }
    render() {
        let dialogButton = this.state.currentNombre!==''?<Dialogo btnLabel="Guardar" btnType="raised"  dialogText = "Está seguro que desea guardar esta información?" confirm={this.handleEditCurrent} />:'';
        let dialogButtonDelete = <Dialogo btnLabel="Eliminar" deleteButton={true} dialogText = "Está seguro que desea eliminar esta información?" confirm={this.deleteCurrent} />
        let detalle= this.state.categorias!==null&&Object.entries(this.state.categorias).map((item,index)=>{
            let element = item[1];
            let key = item[0];
            element.key=key;
            return element;
          });
          return this.state.loading === true ? <div className="content"><CircularProgress color={green400} size={100} thickness={7} /></div> : (
            <div className="App">
                <PageTitle 
                    title="Tu Fútbol"
                    isLoggedIn={true}
                    hasSettingsMenu ={false}
                />
                <br />
                <div style={{width: '100%'}}>
                    <Paper className="paperCanchas" zDepth={2} style={{width: '100%'}}>
                    <Toolbar style={{width: '100%',  maxWidth: '100%'}}>
                        <ToolbarGroup>
                        {
                            this.state.isEditing === true ?
                            <IconButton iconClassName="material-icons" style={{textAlign:'left',color:'#000'}} tooltip="Volver" onClick={this.back}>arrow_back_ios</IconButton>
                            :
                            <Link to={"/"}><IconButton style={{textAlign:'left',color:'#000'}} tooltip="Atras" iconClassName="material-icons">arrow_back_ios</IconButton></Link>
                        }
                            <ToolbarTitle text={this.state.title} style={{color:'#000'}} className="toolbarTitle"/>
                        </ToolbarGroup>
                        {
                            this.state.isEditing === false ?
                                <IconButton iconClassName="material-icons" style={{textAlign:'right',color:'#000'}} tooltip="Agregar" onClick={this.addNew}>add</IconButton>
                            :
                                <div>
                                {
                                    this.state.current!==null && dialogButtonDelete
                                }
                                </div>
                        }
                    </Toolbar>
                    <br />
                    {
                        this.state.isEditing === false ?
                            <ReactTable 
                                data={detalle}
                                defaultPageSize={10}
                                previousText= 'Anterior'
                                nextText= 'Siguiente'
                                loadingText= 'Cargando...'
                                noDataText= 'No hay datos'
                                pageText= 'Página'
                                ofText= 'de'
                                rowsText= 'Filas'
                                pageJumpText= 'Ir a página'
                                rowsSelectorText= 'filas por página'
                                filterable 
                                defaultFilterMethod={(filter, row) =>String(row[filter.id]).toLowerCase().includes( filter.value.toLowerCase())}
                                columns={[
                                {
                                    columns: [
                                    {
                                        Header: "Nombre",
                                        accessor: "nombre",
                                    },
                                    {
                                        Header:'Editar',
                                        accessor:'key',
                                        width:85,
                                        Cell: row => (
                                            <IconButton iconClassName="material-icons" onClick={()=>this.handleEdit(row.original,row.original.key)}>edit</IconButton>
                                        )
                                    }
                                ]
                                }
                                ]}
                                className={"-striped -highlight table"}
                            />
                        :
                        <div>
                        <Table bodyStyle={{overflow:'visible'}}  selectable={false}>
                            <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
                                <TableRow>
                                    <TableHeaderColumn className="smallWidth">Info</TableHeaderColumn>
                                    <TableHeaderColumn>Dato</TableHeaderColumn>
                                </TableRow>
                            </TableHeader>
                            <TableBody displayRowCheckbox={false}>
                                <TableRow >
                                    <TableRowColumn className="smallWidth">Nombre</TableRowColumn>
                                    <TableRowColumn>
                                        <TextField
                                            hintText="Nombre"
                                            fullWidth={true}
                                            value={this.state.currentNombre}
                                            onChange={e => this.setState({currentNombre: e.target.value})}
                                        />
                                    </TableRowColumn>
                                </TableRow>
                            </TableBody>
                        </Table>
                        {dialogButton}
                        </div>
                    }
                    </Paper>
                </div>
            </div>
        )
    }
}


