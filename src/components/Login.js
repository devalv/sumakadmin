import React, { Component } from 'react';
import {auth} from '../utils/Firebase';
import '../utils/Style.css';
import { green400 } from 'material-ui/styles/colors';
import moment from 'moment';
import RaisedButton from 'material-ui/RaisedButton';
import CircularProgress from 'material-ui/CircularProgress';
import TextField from'material-ui/TextField';
import Paper from 'material-ui/Paper';
import logo from '../assets/logo2.png';
import PageTitle from './PageTitle';
import {logRef} from '../utils/Controller';


export default class Login extends Component {
    constructor(props){
        super(props);
        this.state={
            email:'',
            password:'',
            isLoading:false,
            error:false,
        };
    }
    handleSubmit = (e) =>{
        this.setState({isLoading:true})
        let email = this.state.email;
        let password = this.state.password;
        e.preventDefault();
        let self = this;
        auth.signInWithEmailAndPassword(email,password).then((data)=>{
            const dateTime = moment().format("YYYY-MM-DD HH:mm:ss");
            let uid = data.user.uid;
            const user = {
                email,
                uid,
                description: 'logged in'
            }
            logRef.child(dateTime).set(user);    
            sessionStorage.setItem('uid', uid);
            sessionStorage.setItem('e', email);
            sessionStorage.setItem('p', password);
            self.setState({isLoading:false})
        })
        .catch((error) => {
            self.setState({error:true,isLoading:false,});
            alert('Datos Incorrectos');
        })
    }
    
    onKeyPress = (e) => {
        if (e.key === 'Enter') {
          this.handleSubmit(e);
        }
    }   

    render() {
        return this.state.isLoading === true ? <div className="content"><CircularProgress color={green400} size={100} thickness={7} /></div> : (
            <div className="App">
                <PageTitle title="Tu Fútbol"/>
                <Paper className="loginContainer" zDepth={1}>
                    <div>
                    <img className="logoMain" src={logo} width="200" alt="logo" />
                    </div>
                    <br />
                    <div>
                    <h3>INGRESO</h3>
                    </div>
                    <div>
                        <TextField 
                        value={this.state.email}
                        onChange={e => this.setState({ email: e.target.value.toLowerCase()})} 
                        floatingLabelText="Correo electrónico"/>
                    </div>
                    <div>
                        <TextField 
                        value={this.state.password}
                        onChange={e => this.setState({ password: e.target.value})} 
                        onKeyPress={e => this.onKeyPress(e)}
                        floatingLabelText="Contraseña" 
                        type="password"/>
                    </div>                    
                    <div style={{padding:20}}>
                        <RaisedButton label="Entrar" onClick={this.handleSubmit}>
                            <input type="submit" className="submit"/>
                        </RaisedButton>
                    </div>
                    {
                        this.state.error&&
                        <div>
                            <h6 style={{color:'red'}}>Error, Intente nuevamente</h6>
                        </div>
                    }
                </Paper>
            </div>
        );
    }
}


