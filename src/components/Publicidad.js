import React, { Component } from 'react';
import '../utils/Style.css';
import PageTitle from './PageTitle';
import ActionUpload from 'material-ui/svg-icons/file/cloud-upload';
import CircularProgress from 'material-ui/CircularProgress';
import FileUploader from 'react-firebase-file-uploader';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import {storage} from '../utils/Firebase';
import IconButton from 'material-ui/IconButton';
import Dialogo from '../utils/Dialogo';
import RaisedButton from 'material-ui/RaisedButton';
import {Link} from 'react-router-dom';
import {Table,TableBody,TableHeader,TableHeaderColumn,TableRow,TableRowColumn} from 'material-ui/Table';
import {getPublicidad,addPublicidad,editPublicidad,deletePublicidad} from '../utils/Controller';
import {green400} from 'material-ui/styles/colors'
import {Toolbar, ToolbarGroup,  ToolbarTitle} from 'material-ui/Toolbar';
import 'react-table/react-table.css';
import ReactTable from "react-table";
export default class Publicidad extends Component {
    constructor(props){
        super(props);
        this.state={
            current:null,
            isEditing:false,
            currentNombre:'',
            publicidad:null,
            key:null,
            addingNewone:false,
            title:"Publicidad",
            loading:true,
            foto: '',
            addedFoto:false,
            isUploading: false,
            progress:0,
            tab:null,
            fotoUploaded:false,
        };
    }
   
    back = () => {
        this.setState({
            lcurrent:null,
            isEditing:false,
            currentNombre:'',
            publicidad:null,
            key:null,
            addingNewone:false,
            title:"Publicidad",
            foto: '',
            loading:true,
            addedFoto:false,
            isUploading: false,
            progress:0,
            tab:null,
            fotoUploaded:false,
        });
        this.startOver();
    }
    startOver(){
        getPublicidad().then(value=>{
            let publicidad = value.toJSON();
            this.setState({publicidad,loading:false});
        })
    }
    handleEdit = (publici,key) =>{
        this.setState({
            publici,
            isEditing:true,
            key,
            title:"Editar",
            currentNombre:publici.nombre?publici.nombre:'',
            foto:publici.foto?publici.foto:'',
        })
    }

    handleUploadStart = () => this.setState({isUploading: true, progress: 0});
    handleProgress = (progress) => this.setState({progress});
    handleUploadError = (error) => {
        this.setState({isUploading: false});
        console.error(error);
        alert(error);
    }
    handleUploadSuccess = (filename) => {
        this.setState({foto: filename,fotoUploaded:true, progress: 100, isUploading: false});
        storage.ref('Publicidad').child(filename).getDownloadURL().then(url => this.setState({foto: url}));
    };

    deleteCurrent = () =>{
        let valid = deletePublicidad(this.state.key);
        if(valid){
            this.back();
            this.startOver();
        }
    }
    handleEditCurrent = () =>{
        let valid = false;
        this.setState({loading:true});
        if(this.state.addingNewone===true){
            let editing = {
                nombre:this.state.currentNombre,
                foto: this.state.foto
            }
            valid = addPublicidad(editing);
        }
        else{
            let editing = {
                nombre:this.state.currentNombre,
                foto: this.state.foto
            };
            valid = editPublicidad(editing,this.state.key);
        }
        if(valid){
            this.back();
        }
       this.startOver();
    }
    addNew = () =>{
        this.setState({isEditing:true,addingNewone:true});
    }
    componentWillMount(){
        this.startOver();
    }
    render() {
        let dialogButton = this.state.currentNombre!==''?<Dialogo btnLabel="Guardar" btnType="raised"  dialogText = "Está seguro que desea guardar esta información?" confirm={this.handleEditCurrent} />:'';
        let dialogButtonDelete = <Dialogo btnLabel="Eliminar" deleteButton={true} dialogText = "Está seguro que desea eliminar esta información?" confirm={this.deleteCurrent} />
        let detalle= this.state.publicidad!==null&&Object.entries(this.state.publicidad).map((item,index)=>{
            let element = item[1];
            let key = item[0];
            element.key=key;
            return element;
          });
          return this.state.loading === true ? <div className="content"><CircularProgress color={green400} size={100} thickness={7} /></div> : (
            <div className="App">
                <PageTitle 
                    title="Tu Fútbol"
                    isLoggedIn={true}
                    hasSettingsMenu ={false}
                />
                <br />
                <div style={{width: '100%'}}>
                    <Paper className="paperCanchas" zDepth={2} style={{width: '100%'}}>
                    <Toolbar style={{width: '100%',  maxWidth: '100%'}}>
                        <ToolbarGroup>
                        {
                            this.state.isEditing === true ?
                            <IconButton iconClassName="material-icons" style={{textAlign:'left',color:'#000'}} tooltip="Volver" onClick={this.back}>arrow_back_ios</IconButton>
                            :
                            <Link to={"/"}><IconButton style={{textAlign:'left',color:'#000'}} tooltip="Atras" iconClassName="material-icons">arrow_back_ios</IconButton></Link>
                        }
                            <ToolbarTitle text={this.state.title} style={{color:'#000'}} className="toolbarTitle"/>
                        </ToolbarGroup>
                        {
                            this.state.isEditing === false ?
                                <IconButton iconClassName="material-icons" style={{textAlign:'right',color:'#000'}} tooltip="Agregar" onClick={this.addNew}>add</IconButton>
                            :
                                <div>
                                {
                                    this.state.current!==null && dialogButtonDelete
                                }
                                </div>
                        }
                    </Toolbar>
                    <br />
                    {
                        this.state.isEditing === false ?
                            <ReactTable 
                                data={detalle}
                                defaultPageSize={10}
                                previousText= 'Anterior'
                                nextText= 'Siguiente'
                                loadingText= 'Cargando...'
                                noDataText= 'No hay datos'
                                pageText= 'Página'
                                ofText= 'de'
                                rowsText= 'Filas'
                                pageJumpText= 'Ir a página'
                                rowsSelectorText= 'filas por página'
                                filterable 
                                defaultFilterMethod={(filter, row) =>String(row[filter.id]).toLowerCase().includes( filter.value.toLowerCase())}
                                columns={[
                                {
                                    columns: [
                                    {
                                        Header: "Foto",
                                        accessor: "foto",
                                        width: 200,
                                        Cell: row => <img src={row.value} style={{width: 200, height: 200}} />
                                    },
                                    {
                                        Header: "Nombre",
                                        accessor: "nombre",
                                    },
                                    {
                                        Header:'Editar',
                                        accessor:'key',
                                        width:85,
                                        Cell: row => (
                                            <IconButton iconClassName="material-icons" onClick={()=>this.handleEdit(row.original,row.original.key)}>edit</IconButton>
                                        )
                                    }
                                ]
                                }
                                ]}
                                className={"-striped -highlight table"}
                            />
                        :
                        <div>
                        <Table bodyStyle={{overflow:'visible'}}  selectable={false}>
                            <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
                                <TableRow>
                                    <TableHeaderColumn className="smallWidth">Info</TableHeaderColumn>
                                    <TableHeaderColumn>Dato</TableHeaderColumn>
                                </TableRow>
                            </TableHeader>
                            <TableBody displayRowCheckbox={false}>
                                <TableRow >
                                    <TableRowColumn className="smallWidth">Nombre</TableRowColumn>
                                    <TableRowColumn>
                                        <TextField
                                            hintText="Nombre"
                                            fullWidth={true}
                                            value={this.state.currentNombre}
                                            onChange={e => this.setState({currentNombre: e.target.value})}
                                        />
                                    </TableRowColumn>
                                </TableRow>
                                <TableRow>
                                    <TableRowColumn className="smallWidth">Foto</TableRowColumn>
                                    <TableRowColumn>
                                    {
                                        this.state.isUploading &&
                                            <p>Progreso: {this.state.progress}</p>
                                    }
                                    {
                                        this.state.foto &&
                                            <div>
                                                <a target="_blank" href={this.state.foto}>
                                                    <img src={this.state.foto} style={{margin:20,width:150,height:150}} alt="img"/>
                                                </a>
                                                <br />
                                            </div>
                                    }
                                    {
                                        this.state.fotoUploaded === false &&
                                    
                                        <RaisedButton
                                            label="Foto"
                                            labelPosition="before"
                                            style={{margin:20}}
                                            containerElement="label"
                                            icon={<ActionUpload />}
                                        >   
                                            <FileUploader
                                                hidden
                                                accept="file/*"
                                                name="foto"
                                                randomizeFilename
                                                storageRef={storage.ref('Publicidad')}
                                                onUploadStart={this.handleUploadStart}
                                                onUploadError={this.handleUploadError}
                                                onUploadSuccess={this.handleUploadSuccess}
                                                onProgress={this.handleProgress}
                                            />
                                        </RaisedButton>
                                        
                                    }
                                    </TableRowColumn>
                                </TableRow>
                            </TableBody>
                        </Table>
                        {dialogButton}
                        </div>
                    }
                    </Paper>
                </div>
            </div>
        )
    }
}


