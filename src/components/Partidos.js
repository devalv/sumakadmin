import React, { Component } from 'react';
import '../utils/Style.css';
import PageTitle from './PageTitle';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import CircularProgress from 'material-ui/CircularProgress';
import Paper from 'material-ui/Paper';
import ActionUpload from 'material-ui/svg-icons/file/cloud-upload';
import Checkbox from 'material-ui/Checkbox';
import TimePicker from 'material-ui/TimePicker';
import {List, ListItem} from 'material-ui/List';
import {storage} from '../utils/Firebase';
import 'react-table/react-table.css';
import FileUploader from 'react-firebase-file-uploader';
import ReactTable from "react-table";
import TextField from 'material-ui/TextField';
import DatePicker from 'material-ui/DatePicker';
import IconButton from 'material-ui/IconButton';
import Toggle from 'material-ui/Toggle';
import RaisedButton from 'material-ui/RaisedButton';
import Dialogo from '../utils/Dialogo';
import moment from 'moment';
import {Link} from 'react-router-dom';
import DialogoGol from '../utils/DialogoGol';
import DialogoAmonestacion from '../utils/DialogoAmonestacion';
import DialogoSancion from '../utils/DialogoSancion';
import {Table,TableBody,TableHeader,TableHeaderColumn,TableRow,TableRowColumn} from 'material-ui/Table';
import {editPartido, addPartido,formatDate, getEquipos, getCampeonato,addGol, finishOtherMatches, addAmonestacion,getPartido,sancionarPartido,quitarSancion,deletePartido,deleteGol, deleteAmonestacion,notificationBegins,notificationFinish,notificationGol, campeonatosRef} from '../utils/Controller';
import {green400} from 'material-ui/styles/colors'
import nojugadoImg from '../assets/stopwatch.png';
import jugadoImg from '../assets/checked.png';
import {Toolbar, ToolbarGroup, ToolbarTitle} from 'material-ui/Toolbar';
import DeleteIcon from 'material-ui/svg-icons/action/delete';
const styles = {
    block: {
      maxWidth: 250,
    },
    checkbox: {
      marginBottom: 16,
      width: 300,
      display: 'inline-block'
    },
  };
export default class AdminDetail extends Component {
    constructor(props){
        super(props);
        this.state={
            partidos:null,
            jornadas:[],
            goles:[],
            allEquipos:[],
            amonestacionesD:[],
            keyCampeonato:null,
            current:null,
            campeonato:null,
            currentResultado:'',
            isEditing:false,
            currentLocal:null,
            currentVisitante:null,
            equipos:[],
            currentInfo:'',
            currentVocal:'',
            OnceLocal:[],
            OnceVisitante:[],
            currentSancionado:null,
            barriales:null,
            currentfecha:null,
            currentHora:null,
            currentCoordenadas:'',
            currentJugado:false,
            currentCategoría:'',
            currentJugando:false,
            currentDateAdded:'',
            key:null,
            addingNewone:false,
            title:"Partidos",
            loading:true,
            isUploading: false,
            addedAdjunto:false,
            progress:0,
            adjunto: null,
            tab:null,
            amonestaciones: null,
            jornada: null
        };
    }
    handleSancion = (equipo,descripción) =>{
        if(equipo===null && descripción===null){
            quitarSancion(this.state.keyCampeonato,this.state.key,equipo,descripción);
            this.setState({currentSancionado:null});
            this.back();
        }
        else{
            sancionarPartido(this.state.keyCampeonato,this.state.key,equipo,descripción);
            this.setState({currentSancionado:{equipo,descripción}});
            this.back();
        }
    }
    deleteGoal = (item) =>{
        deleteGol(item,this.state.keyCampeonato,this.state.key);
        let key = this.state.key;
        let keyCampeonato = this.state.keyCampeonato;
        this.back();
        this.startOver();
        this.getCurrent(keyCampeonato,key);
    }
    deleteAmonestacion = (item) =>{
        deleteAmonestacion(item,this.state.keyCampeonato,this.state.key);
        let key = this.state.key;
        let keyCampeonato = this.state.keyCampeonato;
        this.back();
        this.startOver();
        this.getCurrent(keyCampeonato,key);
    }
    handleChangeLocal =  (event, index, value) => {
        this.setState({currentLocal:value});
    }
    handleChangeVisitante =  (event, index, value) => {
        this.setState({currentVisitante:value});
    }
    
    back = () => {
        this.setState({
            partidos:null, 
            goles:[],
            amonestacionesD:[],
            allEquipos:[],
            keyCampeonato:null,
            current:null,
            campeonato:null,
            isEditing:false,
            currentLocal:null,
            currentResultado:'',
            currentVisitante:null,
            currentSancionado:null,
            currentInfo:'',
            currentVocal:'',
            OnceLocal:[],
            OnceVisitante:[],
            barriales:null,
            currentfecha:'',
            currentHora:null,
            currentCoordenadas:'',
            currentJugado:false,
            currentCategoría:'',
            currentJugando:false,
            currentDateAdded:'',
            key:null,
            addingNewone:false,
            title:"Partidos",
            loading:true,
            isUploading: false,
            addedAdjunto:false,
            progress:0,
            adjunto: null,
            tab:null,
            amonestaciones: null,
            jornada: null
        });
        this.startOver();
    }
    handleUploadStart = () => this.setState({isUploading: true, progress: 0});
    handleProgress = (progress) => this.setState({progress});
    handleUploadError = (error) => {
        this.setState({isUploading: false});
        
        alert(error);
    }
    handleUploadSuccess = (filename) => {
        this.setState({adjunto: filename,addedAdjunto:true, progress: 100, isUploading: false});
        storage.ref('Adjuntos').child(filename).getDownloadURL().then(url => this.setState({adjunto: url}));
    };
    startOver(){
        let keyCampeonato = this.props.match.params.keyCampeonato;
        this.setState({keyCampeonato});
        campeonatosRef.child(this.props.match.params.keyCampeonato+'/amonestaciones/').on('value', snapshot => {
            if(snapshot.toJSON()){
                const amonestaciones = Object.values(snapshot.toJSON());
                this.setState({amonestaciones})
            }
        })
        getCampeonato(keyCampeonato).then(val=>{
            if(val.toJSON()){
                let campeonato = val.toJSON();
                
                if (campeonato.equipos) {
                    const allEquipos = Object.values(campeonato.equipos).map(obj => obj.nombre);
                    allEquipos.push('Dirigente')
                    this.setState({allEquipos});
                }
                if(campeonato.partidos){
                    let partidos = Object.entries(campeonato.partidos);
                    delete campeonato.partidos;
                    this.setState({partidos});
                }
                let jornadas = [];
                if (campeonato.jornadas) {
                    jornadas = Object.values(campeonato.jornadas);
                }
                this.setState({jornadas, campeonato,currentCategoría:campeonato.categoría,loading:false})
                this.handleEquipos(campeonato.liga,campeonato.barrial);
            } else {
                this.setState({loading: false});
            }
        }).catch(error=>{
            console.log('error: ', error);
            this.setState({loading: false});
        })
    }
    handleEquipos = (currentBarrial,currentCategoría) =>{
        let self  =  this;
        getEquipos(currentBarrial).then(val=>{
            let v = val.toJSON();
            if(v){
                let teams = Object.values(v);
                let equipos = [];
                teams.map(element=>{
                    if(element.categoría === currentCategoría){
                        equipos.push(element);
                    }
                    return element;
                })
                self.setState({equipos})
            }
            else {
                self.setState({equipos:[]})
            }
        }).catch(error=>{
            
        });
    }
    handleEdit = (current,key) =>{
        let fecha = current.fecha?new Date(current.fecha):formatDate(new Date());
        let datetime = current.hora?new Date(current.fecha + ' ' +current.hora):null;
        var local = current.local;
        var visitante = current.visitante;
        let goles = current.goles?Object.values(current.goles):[];
        let amonestacionesD = current.amonestaciones?Object.values(current.amonestaciones):[];
        this.setState({
            current,
            isEditing:true,
            key,
            title:"Editar "+current.local.nombre+ ' vs '+current.visitante.nombre,
            currentResultado:current.resultado?current.resultado:'',
            currentInfo:current.info?current.info:'',
            currentVocal:current.vocal?current.vocal:'',
            currentSancionado:current.sancionado?current.sancionado:null,
            currentLocal:local,
            jornada: current.jornada ? current.jornada: null,
            currentVisitante:visitante,
            currentJugado:current.jugado?current.jugado:false,
            currentJugando:current.jugando?current.jugando:false,
            OnceLocal:current.OnceLocal?Object.values(current.OnceLocal):[],
            OnceVisitante:current.OnceVisitante?Object.values(current.OnceVisitante):[],
            currentfecha:fecha,
            currentHora:datetime,
            adjunto: current.adjunto?current.adjunto:null,
            currentDateAdded:current.dateAdded?current.dateAdded:'',
            goles,
            amonestacionesD,
        });
        this.updateResult(goles);
        const {campeonato, partidos} = this.state;
        if(campeonato.amonestaciones){
            const amonestacionesJSON = Object.values(campeonato.amonestaciones);
            const amonestaciones = amonestacionesJSON.map(item=>{
                const partido = partidos.filter(obj => obj[0] === item.partido)[0];
                if(partido) {
                    item.partidoObj = partido;
                    item.fecha = partido[1].fecha;
                    item.equipoLocal = partido[1].local.nombre;
                    item.equipoVisitante = partido[1].visitante.nombre;
                    item.equipoAmonestado = item.equipo === 'local' ? item.equipoLocal : item.equipoVisitante;
                }
                return item;
            })
            this.setState({amonestaciones});
        }
        else {
            this.setState({amonestaciones: null})
        }
    }
    deleteCurrent = () =>{
        let valid = deletePartido(this.state.keyCampeonato,this.state.key);
        if(valid){
            this.back();
            this.startOver();
        }
    }
    handleAddGol = (gol) =>{
        this.setState({loading:true});
        let key = this.state.key;
        let keyCampeonato = this.state.keyCampeonato;
        addGol(gol,keyCampeonato,key);
        this.golNotif(gol,keyCampeonato,key);
        this.back();
        this.startOver();
        this.getCurrent(keyCampeonato,key);
    }
    handleAddAmonestacion = (amonestacion) =>{
        this.setState({loading:true});
        let key = this.state.key;
        let jornada = this.state.jornada;
        let keyCampeonato = this.state.keyCampeonato;
        addAmonestacion(amonestacion,keyCampeonato,key, jornada);
        this.back();
        this.startOver();
        this.getCurrent(keyCampeonato,key);
    }
    golNotif = (gol,keyCampeonato,keyPartido)=>{
        let currentfecha = this.state.currentfecha.getFullYear() + "/" + (this.state.currentfecha.getMonth() + 1) + "/" + this.state.currentfecha.getDate();
        let currentHora = this.state.currentHora.toTimeString();
        let dat = new Date();
        let editing = {
            gol,
            keyCampeonato,
            keyPartido,
            editing:{
                resultado:this.state.currentResultado,
                info:this.state.currentInfo,
                vocal:this.state.currentVocal,
                local:this.state.currentLocal,
                visitante:this.state.currentVisitante,
                jugado:this.state.currentJugado,
                categoría:this.state.currentCategoría,
                jugando:this.state.currentJugando,
                OnceLocal:this.state.OnceLocal,
                OnceVisitante:this.state.OnceVisitante,
                fecha:currentfecha,
                jornada: this.state.jornada,
                goles:this.state.goles,
                amonestaciones:this.state.amonestacionesD,
                hora:currentHora,
                dateAdded:this.state.currentDateAdded,
                dateModified:dat.getTime(),
            }
        };
        notificationGol(editing);
        
    }
    updateResult = (goles) =>{
        var local=0;
        var visitante=0;
        goles.map(element=>{
            if(element.equip==="local"){
                local++
            }
            if(element.equip==="visitante"){
                visitante++;
            }
            return element;
        })
        var currentResultado = local+'-' + visitante;
        this.setState({currentResultado})
    }
    getCurrent=(keyCampeonato,key) =>{
        let self = this;
        getPartido(keyCampeonato,key).then(val=>{
            let current =(val.toJSON());
            self.handleEdit(current,key);
        }).catch(err=>{
            
        });
    }
    handleHora = (event, date) => {
        this.setState({currentHora: date});
      };
    handleEditCurrent = () =>{
        let valid = false;
        let currentfecha = this.state.currentfecha ? this.state.currentfecha.getFullYear() + "/" + (this.state.currentfecha.getMonth() + 1) + "/" + this.state.currentfecha.getDate(): '';
        let currentHora = this.state.currentHora?this.state.currentHora.toTimeString():null;
        let dat = new Date();
        this.setState({loading:true});
        if(this.state.addingNewone===true){
            if(this.state.currentLocal && this.state.currentVisitante&&currentHora){
                let editing = {
                    resultado:this.state.currentResultado,
                    local:this.state.currentLocal,
                    visitante:this.state.currentVisitante,
                    info:this.state.currentInfo,
                    vocal:this.state.currentVocal,
                    OnceLocal:this.state.OnceLocal,
                    OnceVisitante:this.state.OnceVisitante,
                    jugado: this.state.currentJugado,
                    categoría:this.state.currentCategoría,
                    jugando:this.state.currentJugando,
                    jornada: this.state.jornada,
                    fecha:currentfecha,
                    hora:currentHora,
                    goles:this.state.goles,
                    amonestaciones:this.state.amonestacionesD,
                    dateAdded:dat.getTime(),
                    adjunto: this.state.adjunto,
                    keyCampeonato: this.state.keyCampeonato
                }
                valid = addPartido(this.state.keyCampeonato,editing);
            }
            else{
                alert('Datos incorrectos');
            }
        }
        else{
            let editing = {
                resultado:this.state.currentResultado,
                info:this.state.currentInfo,
                vocal:this.state.currentVocal,
                local:this.state.currentLocal,
                visitante:this.state.currentVisitante,
                jugado:this.state.currentJugado,
                jugando:this.state.currentJugando,
                categoría:this.state.currentCategoría,
                OnceLocal:this.state.OnceLocal,
                OnceVisitante:this.state.OnceVisitante,
                sancionado:this.state.currentSancionado,
                fecha:currentfecha,
                goles:this.state.goles,
                jornada: this.state.jornada,
                amonestaciones:this.state.amonestacionesD,
                hora:currentHora,
                adjunto: this.state.adjunto,
                dateAdded:this.state.currentDateAdded,
                dateModified:dat.getTime(),
                keyCampeonato: this.state.keyCampeonato
            };
            valid = editPartido(this.state.keyCampeonato,editing,this.state.key);
        }
        if(valid){
            this.back();
        }
       this.startOver();
    }
    addNew = () =>{
        this.setState({isEditing:true,addingNewone:true});
    }
    calculateRestantes = (partidosSancion, desde, actual) => {
        if (partidosSancion) {
            const jornadasPasadas = actual - desde;
            const restante = partidosSancion - jornadasPasadas + 1;
            return restante;
        }
        return 0;
    }
    renderSancionesExistentes = () => {
        const {amonestaciones, jornada} = this.state;
        if(amonestaciones){
            return amonestaciones.filter(obj=> Number(obj.jornada.numero) < Number(jornada.numero)).map((element,index) => {
                let partidosRestantes = 1;
                if (element.partidosSancion) {
                    partidosRestantes = this.calculateRestantes(element.partidosSancion, element.jornada.numero, jornada.numero);
                }
                if (partidosRestantes >= 1) {
                    return(
                        <div key={index}>
                            <div><b>Jornada: </b>{element.jornada.numero}</div>
                            <div><b>Equipo: </b>{element.equipo === 'local' ? element.equipoLocal : element.equipoVisitante}</div>
                            <div><b>Jugador: </b>{element.jugador + ' - #' + element.numero.número}</div>
                            <div><b>Tipo: </b>{element.tipo}</div>
                            <div><b>Motivo: </b>{element.motivo}</div>
                            {
                                element.partidosSancion && 
                                    <div><b>Partidos sanción restantes: </b>{partidosRestantes}</div>
                            }
                            <br /><br />
                        </div>
                    )
                }
                return null;
            });
        }
        return null;

    }
    comenzoPartido = (event,activated) => {
        finishOtherMatches(this.state.keyCampeonato, this.state.key);
        let updated = false;
        if (activated === false) {
            this.setState({currentJugado:activated,currentJugando:activated}, ()=>{
                if (updated === false){ 
                    this.handleEditCurrent();
                    updated = true;
                }
            });
        } else{
            this.setState({currentJugando:activated}, ()=>{
                if (updated === false){ 
                    this.handleEditCurrent();
                    updated = true;
                }
            });
        }
        
        if(this.state.currentLocal && this.state.currentVisitante){
            if(activated ===true){
                let currentfecha = this.state.currentfecha.getFullYear() + "/" + (this.state.currentfecha.getMonth() + 1) + "/" + this.state.currentfecha.getDate();
                let currentHora = this.state.currentHora.toTimeString();
                let dat = new Date();
                let editing = {
                    resultado:this.state.currentResultado,
                    info:this.state.currentInfo,
                    vocal:this.state.currentVocal,
                    local:this.state.currentLocal,
                    visitante:this.state.currentVisitante,
                    jugado:this.state.currentJugado,
                    categoría:this.state.currentCategoría,
                    jugando:this.state.currentJugando,
                    OnceLocal:this.state.OnceLocal,
                    jornada: this.state.jornada,
                    OnceVisitante:this.state.OnceVisitante,
                    fecha:currentfecha,
                    goles:this.state.goles,
                    amonestaciones:this.state.amonestacionesD,
                    hora:currentHora,
                    dateAdded:this.state.currentDateAdded,
                    dateModified:dat.getTime(),
                };
                notificationBegins(editing);
                
            }
        }
        setTimeout(()=>{
            if (updated === false){ 
                this.handleEditCurrent();
                updated = true;
            }
        }, 250)
    }
    terminoPartido = (event,activated)=>{
        this.setState({currentJugado:activated}, ()=>{
            this.handleEditCurrent();
        })
        if(activated ===true){
            let currentfecha = this.state.currentfecha.getFullYear() + "/" + (this.state.currentfecha.getMonth() + 1) + "/" + this.state.currentfecha.getDate();
            let currentHora = this.state.currentHora.toTimeString();
            let dat = new Date();
            let editing = {
                resultado:this.state.currentResultado,
                info:this.state.currentInfo,
                vocal:this.state.currentVocal,
                local:this.state.currentLocal,
                visitante:this.state.currentVisitante,
                jugado:this.state.currentJugado,
                categoría:this.state.currentCategoría,
                jugando:this.state.currentJugando,
                OnceLocal:this.state.OnceLocal,
                jornada: this.state.jornada,
                OnceVisitante:this.state.OnceVisitante,
                fecha:currentfecha,
                goles:this.state.goles,
                amonestaciones:this.state.amonestacionesD,
                hora:currentHora,
                dateAdded:this.state.currentDateAdded,
                dateModified:dat.getTime(),
            };
            notificationFinish(editing);
            
        }
    }
    handleBirthDate =  (event, currentfecha) => {
        this.setState({currentfecha});
    };
    handleChangeJornada = (event, index, jornadaKey) => {
        const {jornadas} = this.state;
        const jornada = jornadas.filter(obj => obj.key === jornadaKey)[0];
        if (jornada) {
            this.setState({jornada})
        }
    }
    handleChangeVocal = (event, index, currentVocal) => {
        this.setState({currentVocal})
    }
    checkLocal = (item) => {
        let OnceLocal = (this.state.OnceLocal);
        let exists = false;
        let index = -1;
        for(var i = 0; i<OnceLocal.length;i++){
            if(OnceLocal[i].nombre===item.nombre && OnceLocal[i].número === item.número && OnceLocal[i].notas === item.notas && OnceLocal[i].foto===item.foto){
                exists = true;
                index = i;
            }
        }
        if(exists && index > -1){
            OnceLocal.splice(index, 1);
        }
        else {
            OnceLocal.push(item);
        }
        this.setState({OnceLocal});
    }
    renderAmonestaciones = () => {
        const rol = sessionStorage.getItem('rol');
        if(rol) {
            if(rol === 'sancionador' || rol==='superadmin') {
                return(
                    <TableRow>
                        <TableRowColumn className="smallWidth">Amonestar</TableRowColumn>
                        <TableRowColumn>
                            <Link to={"/amonestaciones/"+this.state.keyCampeonato}>
                                <RaisedButton label="Amonestaciones" />
                            </Link>
                        </TableRowColumn>
                    </TableRow>
                )
            }
        }
    }
    checkVisitante = (item) => {
        let OnceVisitante = (this.state.OnceVisitante);
        let exists = false;
        let index = -1;
        for(var i = 0; i<OnceVisitante.length;i++){
            if(OnceVisitante[i].nombre===item.nombre && OnceVisitante[i].número === item.número && OnceVisitante[i].notas === item.notas && OnceVisitante[i].foto===item.foto){
                exists = true;
                index = i;
            }
        }
        if(exists && index > -1){
            OnceVisitante.splice(index, 1);
        }
        else {
            OnceVisitante.push(item);
        }
        this.setState({OnceVisitante});
    }
    componentWillMount(){
        this.startOver();
    }
    checkSancion = (item) => {
        const {amonestaciones, jornada} = this.state;
        if (amonestaciones) {
            const correspondientes = amonestaciones.filter(obj=> Number(obj.jornada.numero) < Number(jornada.numero));
            return correspondientes.some(jugador => {
                if (jugador.numero.nombre) {
                    return jugador.numero.nombre === item.nombre && this.calculateRestantes(jugador.partidosSancion, jugador.jornada.numero, jornada.numero) > 0
                } else {
                    return jugador.jugador === item.nombre && this.calculateRestantes(jugador.partidosSancion, jugador.jornada.numero, jornada.numero) > 0
                }
            })
        }
        return false;
    }
    render() {
        let hasTeams = this.state.currentLocal!==null?this.state.currentVisitante!==null?true:false:false;
        let dialogButton = <Dialogo btnLabel="Guardar" btnType="raised"  dialogText = "Está seguro que desea guardar esta información?" confirm={this.handleEditCurrent} />
        let dialogButtonDelete = <Dialogo btnLabel="Eliminar" deleteButton={true} dialogText = "Está seguro que desea eliminar esta información?" confirm={this.deleteCurrent} />
        let dialogoButonGoles = <DialogoGol btnLabel="Agregar" btnType="raised" equipos={{local:this.state.currentLocal,visitante:this.state.currentVisitante}}  dialogText = "Agregar gol" saveGol={this.handleAddGol} jugadoresVisitante={this.state.currentVisitante!==null?this.state.currentVisitante.jugadores:[]} resultado ={this.state.currentResultado!==''?this.state.currentResultado:'0-0'} jugadoresLocal={this.state.currentLocal!==null?this.state.currentLocal.jugadores:[]}/>
        let dialogoButonAmonestaciones = <DialogoAmonestacion 
                                            btnLabel="Agregar" 
                                            btnType="raised" 
                                            equipos={{local:this.state.currentLocal,visitante:this.state.currentVisitante}}  
                                            dialogText = "Agregar amonestacion" 
                                            saveAmonestacion={this.handleAddAmonestacion} 
                                            jugadoresVisitante={this.state.currentVisitante!==null?this.state.currentVisitante.jugadores:[]} 
                                            jugadoresLocal={this.state.currentLocal!==null?this.state.currentLocal.jugadores:[]}
                                        />
        let locales = this.state.equipos!==null && Object.values(this.state.equipos).map((item,index)=>{
            if(this.state.currentVisitante!==item){
                return(
                    <MenuItem value={item} primaryText={item.nombre} key={index}/>
                )
            }
            return null;
        });
        let visitantes = this.state.equipos!==null && Object.values(this.state.equipos).map((item,index)=>{
            if(this.state.currentLocal!==item){
                return(
                    <MenuItem value={item} primaryText={item.nombre} key={index} />
                )
            }
            return null;
        });
        let goles = this.state.goles.length>0&&(this.state.goles).map((item,index)=>{
            return(
                <ListItem key={index} primaryText={'Equipo: '+item.equipo.nombre} secondaryText={' Número: '+item.número.número} rightIconButton={<IconButton tooltip="Eliminar Gol" onClick={()=>this.deleteGoal(item)}><DeleteIcon  /> </IconButton>} />
            )
        });
        let amonestacionesD = this.state.amonestacionesD.length>0&&(this.state.amonestacionesD).map((item,index)=>{
            if (item !== {sancionado: true}) {
                return(
                    <ListItem key={index} primaryText={'Equipo: '+item.equipo.nombre} secondaryText={'Tipo: '+item.tipo + ' - Número: '+item.número.número} rightIconButton={<IconButton tooltip="Eliminar Amonestación" onClick={()=>this.deleteAmonestacion(item)}><DeleteIcon  /> </IconButton>} />
                )
            }
            return null;
        });
        let detalle= this.state.partidos!==null&&Object.values(this.state.partidos).map((item,index)=>{
            let element = item[1];
            let key = item[0];
            element.key=key;
            return element;
          });
            let onceLocal = this.state.key!==null && this.state.currentLocal.jugadores&& Object.values(this.state.currentLocal.jugadores).map((item,index)=>{
                let exists = false;
                let players=Object.values(this.state.OnceLocal);
                for(var i = 0; i<players.length;i++){
                    if(players[i].nombre===item.nombre && players[i].dateAdded === item.dateAdded && players[i].notas === item.notas && players[i].escudo===item.escudo){
                        exists = true;
                    }
                }
                return(
                    <div key={index}>
                        <Checkbox style={styles.checkbox} label={item.número +' '+ item.nombre} onCheck={()=>this.checkLocal(item)} checked = {exists} disabled={this.checkSancion(item)}/>
                        <img src={item.foto} alt="foto" style={{width: 50, height: 50, display: 'inline-block'}}/>
                    </div>
                )
            });
            let onceVisitante = this.state.key!==null&& this.state.currentVisitante.jugadores&& Object.values(this.state.currentVisitante.jugadores).map((item,index)=>{
                let exists = false;
                let players=Object.values(this.state.OnceVisitante);
                for(var i = 0; i<players.length;i++){
                    if(players[i].nombre===item.nombre && players[i].dateAdded === item.dateAdded && players[i].notas === item.notas && players[i].escudo===item.escudo){
                        exists = true;
                    }
                }
                return(
                    <div key={index}>
                        <Checkbox style={styles.checkbox} key={index} label={item.número +' '+ item.nombre} onCheck={()=>this.checkVisitante(item)} checked = {exists} disabled={this.checkSancion(item)}/>
                        <img src={item.foto} alt="foto" style={{width: 50, height: 50, display: 'inline-block'}}/>
                    </div>
                )
            });
          return this.state.loading === true ? <div className="content"><CircularProgress color={green400} size={100} thickness={7} /></div> : (
            <div className="App">
                <PageTitle 
                    title="Tu Fútbol"
                    isLoggedIn={true}
                    hasSettingsMenu ={false}
                />
                <br />
                <div style={{width: '100%'}}>
                    <Paper className="paperCanchas" zDepth={2} style={{width: '100%'}}>
                    <Toolbar style={{width: '100%',  maxWidth: '100%'}}>
                        <ToolbarGroup>
                        {
                            this.state.isEditing === true ?
                            <IconButton iconClassName="material-icons" style={{textAlign:'left',color:'#000'}} tooltip="Volver" onClick={this.back}>arrow_back_ios</IconButton>
                            :
                            <Link to={"/campeonatos"}><IconButton style={{textAlign:'left',color:'#000'}} tooltip="Atras" iconClassName="material-icons">arrow_back_ios</IconButton></Link>
                        }
                            <ToolbarTitle text={this.state.title} style={{color:'#000'}} className="toolbarTitle"/>
                        </ToolbarGroup>
                        {
                            this.state.isEditing === false?
                            <div>
                                {
                                    sessionStorage.getItem('rol') !== 'vocal' &&
                                        <IconButton iconClassName="material-icons" style={{textAlign:'right',color:'#000'}} tooltip="Agregar" onClick={this.addNew}>add</IconButton>
                                }
                            </div>
                            :
                            <div>{this.state.current!==null&&sessionStorage.getItem('rol') !== 'vocal' &&dialogButtonDelete}</div>
                        }
                    </Toolbar>
                    <br />
                    {
                        this.state.isEditing === false ?
                           
                        <div>
                            {
                                detalle.length>0&&   
                                <ReactTable 
                                    data={detalle}
                                    defaultPageSize={10}
                                    previousText= 'Anterior'
                                    nextText= 'Siguiente'
                                    loadingText= 'Cargando...'
                                    noDataText= 'No hay datos'
                                    pageText= 'Página'
                                    ofText= 'de'
                                    rowsText= 'Filas'
                                    pageJumpText= 'Ir a página'
                                    rowsSelectorText= 'filas por página'
                                    filterable 
                                    defaultSorted={[{
                                        id   : 'jornada',
                                        desc : true,
                                    }]}
                                    defaultFilterMethod={(filter, row) =>String(row[filter.id]).toLowerCase().includes( filter.value.toLowerCase())}
                                    columns={[
                                    {
                                        columns: [
                                        {
                                            Header: "",
                                            accessor: "dateAdded",
                                            width:0,
                                            filterable: false,
                                            Cell: row=>'',
                                            headerStyle: {width: 0, margin: 0, padding: 0}
                                        },
                                        {
                                            Header: "Jornada",
                                            accessor: "jornada",
                                            width:105,
                                            sortMethod: (a,b) => {
                                                return a.numero > b.numero ? 1 : -1
                                            },
                                            Cell: row => row.original.jornada ? row.original.jornada.numero ? row.original.jornada.numero : '' : ''
                                        },
                                        {
                                            Header: "Fecha",
                                            accessor: "fecha",
                                            width:105,
                                            sortAccessor: request => {
                                                
                                                return moment(request.requestedDate[0]).valueOf();
                                            }
                                        },
                                        {
                                            Header: "Local",
                                            accessor: "local",
                                            Cell:row=>(
                                                row.original.local ? 
                                                    <div>
                                                        <img src={row.original.local.escudo} alt="local" style={{textAlign: 'center', width: 30, height: 30, margin: '0 auto,'}}/>
                                                        <br />
                                                        <span>{row.original.local.nombre || ''}</span> 
                                                    </div>
                                                : ''
                                            )
                                        },
                                        {
                                            Header: "Visitante",
                                            accessor: "visitante",
                                            Cell:row=>(
                                                row.original.visitante ? 
                                                    <div>
                                                        <img src={row.original.visitante.escudo} alt="local" style={{textAlign: 'center', width: 30, height: 30, margin: '0 auto,'}}/>
                                                        <br />
                                                        <span>{row.original.visitante.nombre || ''}</span> 
                                                    </div>
                                                : ''
                                            )
                                        },
                                        {
                                            Header: "Resultado",
                                            accessor: "resultado",
                                            filterable: false,
                                            width:100,
                                            Cell:row=>row.original.resultado===''?'No jugado': row.original.resultado
                                        },
                                        {
                                            Header:'Editar',
                                            accessor:'key',
                                            filterable: false,
                                            width:85,
                                            Cell: row => (
                                                    <IconButton iconClassName="material-icons" onClick={()=>this.handleEdit(row.original,row.original.key)}>edit</IconButton>
                                            )
                                        },
                                        {
                                            Header:'Terminado',
                                            accessor:'jugado',
                                            filterable: false,
                                            width:130,
                                            Cell: row => (
                                                <span>
                                                {
                                                    row.original.sancionado ?
                                                        'Sancionado'
                                                    :
                                                    <img src={row.original.jugado===true?jugadoImg:nojugadoImg} width="20" alt={'jugado '+row.original.jugado}></img>
                                                }
                                                </span>
                                            )
                                        }
                                    ]
                                    }
                                    ]}
                                    className={"-striped -highlight table"}
                                />
                            }
                            </div>
                        :
                        <div>
                        <Table bodyStyle={{overflow:'visible'}}  selectable={false}>
                            <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
                                <TableRow>
                                    <TableHeaderColumn className="smallWidth">Info</TableHeaderColumn>
                                    <TableHeaderColumn>Dato</TableHeaderColumn>
                                </TableRow>
                            </TableHeader>
                            <TableBody displayRowCheckbox={false}>
                                {
                                    this.state.current!==null &&
                                        <TableRow>
                                            <TableRowColumn className="smallWidth">Empezó</TableRowColumn>
                                            <TableRowColumn>
                                                <Toggle
                                                    onToggle = {this.comenzoPartido}
                                                    defaultToggled={this.state.currentJugando}
                                                />
                                            </TableRowColumn>
                                        </TableRow>
                                }
                                {
                                    this.state.current!==null && this.state.currentJugando === true &&
                                        <TableRow>
                                            <TableRowColumn className="smallWidth">Terminó</TableRowColumn>
                                            <TableRowColumn>
                                                <Toggle
                                                    onToggle = {this.terminoPartido}
                                                    defaultToggled={this.state.currentJugado}
                                                />
                                            </TableRowColumn>
                                        </TableRow>
                                }
                                {
                                    this.state.current !== null &&
                                    <TableRow>
                                        <TableRowColumn className="smallWidth">Vocal</TableRowColumn>
                                        <TableRowColumn >
                                            <SelectField
                                                fullWidth={true}
                                                floatingLabelText="Vocal"
                                                value={this.state.currentVocal}
                                                onChange={this.handleChangeVocal}
                                            >
                                                {
                                                    this.state.allEquipos.length > 0 &&
                                                        this.state.allEquipos.map((equip, index) => (
                                                            <MenuItem value={equip} primaryText={equip} key={index}/>
                                                        ))
                                                    
                                                }
                                            </SelectField>
                                        </TableRowColumn>
                                    </TableRow>    
                                }
                                {
                                    sessionStorage.getItem('rol')!=='vocal'&&hasTeams && this.state.current!== null &&
                                    <TableRow>
                                            <TableRowColumn className="smallWidth">Sancionar Partido</TableRowColumn>
                                            <TableRowColumn >
                                                <div style={{fontWeight:900}}>
                                                    {this.state.currentSancionado&& 'SANCIONADO'}
                                                </div>
                                                <div>
                                                    <DialogoSancion btnLabel="Sancionar Partido" btnType="raised" sancion={this.state.currentSancionado} dialogText = "Agregar sanción" saveSancion={this.handleSancion} />
                                                </div>
                                            </TableRowColumn>
                                    </TableRow>
                                }
                                {this.renderAmonestaciones()}
                                {
                                    this.state.current!==null &&
                                        <TableRow>
                                            <TableRowColumn className="smallWidth">Sanciones existentes</TableRowColumn>
                                            <TableRowColumn>
                                                {this.renderSancionesExistentes()}
                                            </TableRowColumn>
                                        </TableRow>
                                }
                                {
                                    sessionStorage.getItem('rol') !== 'vocal' &&
                                        <TableRow>
                                            <TableRowColumn className="smallWidth">Jornada</TableRowColumn>
                                            <TableRowColumn>
                                                <SelectField
                                                    fullWidth={true}
                                                    floatingLabelText="Jornada"
                                                    value={this.state.jornada === null ? null:this.state.jornada.key}
                                                    onChange={this.handleChangeJornada}
                                                >
                                                    {this.state.jornadas.length > 0 && this.state.jornadas.map((item, index) => (
                                                        <MenuItem value={item.key} primaryText={'#'+item.numero + '  (desde: '+item.desde + ' - hasta: '+ item.hasta+')'} key={index}/>
                                                    ))}
                                                </SelectField>
                                            </TableRowColumn>
                                        </TableRow>
                                }
                                <TableRow>
                                    <TableRowColumn className="smallWidth">Fecha</TableRowColumn>
                                    <TableRowColumn>
                                        <DatePicker 
                                            hintText="Fecha" 
                                            container="inline"
                                            fullWidth={true}
                                            value={this.state.currentfecha}
                                            onChange={this.handleBirthDate}
                                            disabled={sessionStorage.getItem('rol') === 'vocal'}
                                        />
                                    </TableRowColumn>
                                </TableRow>
                                <TableRow>
                                    <TableRowColumn className="smallWidth">Hora</TableRowColumn>
                                    <TableRowColumn>
                                        <TimePicker
                                            hintText="12hr Format"
                                            container="inline"
                                            fullWidth={true}
                                            value={this.state.currentHora}
                                            onChange={this.handleHora}
                                            disabled={sessionStorage.getItem('rol') === 'vocal'}
                                        />
                                    </TableRowColumn>
                                </TableRow>
                                <TableRow>
                                    <TableRowColumn className="smallWidth">Local</TableRowColumn>
                                    <TableRowColumn>
                                    {
                                        this.state.addingNewone===false?
                                        this.state.currentLocal.nombre
                                        :
                                        <SelectField
                                            fullWidth={true}
                                            floatingLabelText={this.state.OnceLocal.length>0?this.state.currentLocal.nombre:"Local"}
                                            disabled = {this.state.OnceLocal.length>0?true:false}
                                            value={this.state.currentLocal}
                                            onChange={this.handleChangeLocal}
                                        >
                                            {locales}
                                        </SelectField>
                                    }
                                    </TableRowColumn>
                                </TableRow>
                                {
                                    this.state.current !== null &&
                                    <TableRow>
                                        <TableRowColumn className="smallWidth">11 Local</TableRowColumn>
                                        {
                                            this.state.currentLocal.jugadores !== null&&
                                            <TableRowColumn >
                                                {onceLocal}
                                            </TableRowColumn>
                                        }
                                    </TableRow>
                                }
                                <TableRow>
                                    <TableRowColumn className="smallWidth">Visitante</TableRowColumn>
                                    <TableRowColumn>
                                    {
                                        this.state.addingNewone===false?
                                        this.state.currentVisitante.nombre
                                        :
                                        <SelectField
                                            fullWidth={true}
                                            floatingLabelText={this.state.OnceVisitante.length>0?this.state.currentVisitante.nombre:"Visitante"}
                                            disabled = {this.state.OnceVisitante.length>0?true:false}
                                            value={this.state.currentVisitante}
                                            onChange={this.handleChangeVisitante}
                                        >
                                            {visitantes}
                                        </SelectField>
                                    }
                                        
                                    </TableRowColumn>
                                </TableRow>
                                {
                                    this.state.current !== null &&
                                    <TableRow>
                                        <TableRowColumn className="smallWidth">11 Visitante</TableRowColumn>
                                        {
                                            this.state.currentVisitante.jugadores !== null&&
                                            <TableRowColumn >
                                                {onceVisitante}
                                            </TableRowColumn>
                                        }
                                    </TableRow>
                                }
                                {
                                    hasTeams && this.state.current!== null && this.state.currentJugando === true &&
                                    <TableRow>
                                    
                                            <TableRowColumn className="smallWidth">Goles</TableRowColumn>
                                            <TableRowColumn >
                                                <List>
                                                    {goles}
                                                </List>
                                                    {dialogoButonGoles}
                                                <br />
                                                <div>&nbsp;</div>
                                            </TableRowColumn>
                                    </TableRow>
                                }
                                {
                                    this.state.current!==null&&
                                        <TableRow>
                                            <TableRowColumn className="smallWidth">Resultado</TableRowColumn>
                                            <TableRowColumn>
                                                <div>
                                                    <div style={{fontWeight:900, fontSize: 20}}>
                                                        {this.state.currentResultado}
                                                    </div>
                                                    <div style={{fontWeight:900}}>
                                                        {this.state.currentSancionado && 'SANCIONADO'}
                                                    </div>
                                                </div>
                                            </TableRowColumn>
                                        </TableRow>
                                }
                                {
                                    hasTeams && this.state.current!== null && this.state.currentJugando === true &&
                                    <TableRow>
                                    
                                            <TableRowColumn className="smallWidth">Amonestaciones</TableRowColumn>
                                            <TableRowColumn >
                                                <List>
                                                    {amonestacionesD}
                                                </List>
                                                    {dialogoButonAmonestaciones}
                                                <br />
                                                <div>&nbsp;</div>
                                            </TableRowColumn>
                                    </TableRow>
                                }
                                                          
                                <TableRow>
                                    <TableRowColumn className="smallWidth">Notas</TableRowColumn>
                                    <TableRowColumn >
                                        <TextField
                                            hintText="Notas"
                                            multiLine={true}
                                            rows={2}
                                            rowsMax={5}
                                            fullWidth={true}
                                            value={this.state.currentInfo}
                                            onChange={e => this.setState({currentInfo: e.target.value})}
                                        />
                                    </TableRowColumn>
                                </TableRow>                                
                                <TableRow>
                                    <TableRowColumn className="smallWidth">Adjunto</TableRowColumn>
                                    <TableRowColumn >
                                    {
                                        this.state.isUploading &&
                                            <p>Progreso: {this.state.progress}</p>
                                    }
                                    {
                                        this.state.adjunto &&
                                            <div>
                                                <a target="_blank" href={this.state.adjunto}>
                                                    <img src={this.state.adjunto} style={{margin:20,width:150,height:150}} alt="img"/>
                                                </a>
                                                <br />
                                            </div>
                                    }
                                    {
                                        this.state.addedAdjunto === false &&
                                    
                                        <RaisedButton
                                            label="Agregar Adjunto"
                                            labelPosition="before"
                                            style={{margin:20}}
                                            containerElement="label"
                                            icon={<ActionUpload />}
                                        >   
                                            <FileUploader
                                                hidden
                                                accept="file/*"
                                                name="adjunto"
                                                randomizeFilename
                                                storageRef={storage.ref('Adjuntos')}
                                                onUploadStart={this.handleUploadStart}
                                                onUploadError={this.handleUploadError}
                                                onUploadSuccess={this.handleUploadSuccess}
                                                onProgress={this.handleProgress}
                                            />
                                        </RaisedButton>
                                        
                                    }
                                    </TableRowColumn>
                                </TableRow>
                            </TableBody>
                        </Table>
                        <br />
                        <div>&nbsp;</div>
                        {
                            (hasTeams && this.state.jornada !== null) &&
                                dialogButton
                        }
                        <br />
                        <div>&nbsp;</div>
                        </div>
                    }
                    </Paper>
                </div>
            </div>
        )
    }
}


