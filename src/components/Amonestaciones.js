import React, { Component } from 'react';
import '../utils/Style.css';
import PageTitle from './PageTitle';
import CircularProgress from 'material-ui/CircularProgress';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import IconButton from 'material-ui/IconButton';
import Dialogo from '../utils/Dialogo';
import {Link} from 'react-router-dom';
import {Table,TableBody,TableHeader,TableHeaderColumn,TableRow,TableRowColumn} from 'material-ui/Table';
import {campeonatosRef} from '../utils/Controller';
import {green400} from 'material-ui/styles/colors'
import {Toolbar, ToolbarGroup,  ToolbarTitle} from 'material-ui/Toolbar';
import 'react-table/react-table.css';
import ReactTable from "react-table";
import amonestacion from '../assets/amonestation.png';
import warning from '../assets/warning.png';
import cancel from '../assets/cancel.png';
import redCard from '../assets/red-card.png';
import { SelectField, MenuItem, Toggle, DatePicker, RaisedButton, Checkbox } from 'material-ui';

export default class Amonestaciones extends Component {
    constructor(props){
        super(props);
        this.state={
            current:null,
            jornadas:[],
            isEditing:false,
            borraSanciones:false,
            currentJugador:'',
            currentNumero:'',
            currentPartido:'',
            currentMotivo:'',
            currentEquipo: '',
            currentHasFecha: false,
            currentFechaSancion: '',
            currentPartidosSancion: '',
            currentTipo:'',
            currentPartidoObj:null,
            amonestaciones:null,
            partidos:null,
            key:null,
            addingNewone:false,
            title:"Amonestaciones",
            isLoading:true,
            indexAmonestacion: '',
            authorized: false,
            jornada: null
        };
    }

    componentWillMount() {
        this.startOver();
    }
    
    startOver = () => {
        const rol = sessionStorage.getItem('rol');
        if(rol === 'sancionador' || rol === 'superadmin') {
            const keyCampeonato = this.props.match.params.keyCampeonato;
            if(keyCampeonato) {
                let partidos = [];
                campeonatosRef.child(keyCampeonato).on('value', snapshot=>{
                    const campeonato = snapshot.toJSON();
                    let jornadas = [];
                    if (campeonato.jornadas) {
                        jornadas = Object.values(campeonato.jornadas);
                    }
                    if(campeonato.partidos){
                        partidos = Object.entries(campeonato.partidos);
                        if(campeonato.amonestaciones){
                            const amonestacionesJSON = Object.values(campeonato.amonestaciones);
                            const amonestaciones = amonestacionesJSON.map(item=>{
                                const partido = partidos.filter(obj => obj[0] === item.partido)[0];
                                if(partido){
                                    item.partidoObj = partido;
                                    item.fecha = partido[1].fecha;
                                    item.equipoLocal = partido[1].local.nombre;
                                    item.equipoVisitante = partido[1].visitante.nombre;
                                    item.equipoAmonestado = item.equipo === 'local' ? item.equipoLocal : item.equipoVisitante;
                                    item.número = item.equipo === 'local' ? partido[1].local.jugadores[item.jugador].número ? partido[1].local.jugadores[item.jugador].número : '' : partido[1].visitante.jugadores[item.jugador].número ? partido[1].visitante.jugadores[item.jugador].número : '';
                                }
                                return item;
                            })
                            this.setState({amonestaciones, partidos, isLoading: false, authorized: true, jornadas});
                        }
                        else {
                            this.setState({amonestaciones: [], partidos, isLoading: false, authorized: true, jornadas})
                        }
                        
                    }
                })
            }
        }
    }
   
    back = () => {
        this.setState({
            current:null,
            isEditing:false,
            jornadas: [],
            currentJugador:'',
            borraSanciones: false,
            currentNumero:'',
            jornada: null,
            indexAmonestacion:'',
            currentPartido:'',
            currentMotivo: '',
            currentTipo: '',
            currentPartidoObj:null,
            currentEquipo:'',
            currentHasFecha:false,
            currentFechaSancion:'',
            currentPartidosSancion:'',
            amonestaciones:null,
            key:null,
            addingNewone:false,
            title:"Amonestaciones",
            isLoading:true,
        });
        this.startOver();
    }

    handleChangeJornada = (event, index, jornadaKey) => {
        const {jornadas} = this.state;
        const jornada = jornadas.filter(obj => obj.key === jornadaKey)[0];
        if (jornada) {
            this.setState({jornada})
        }
    }
    
    handleEdit = (current,key) =>{
        this.setState({
            current,
            isEditing:true,
            key,
            borraSanciones: current.borraSanciones ? current.borraSanciones : false,
            title:"Editar amonestaciones",
            currentJugador:current.jugador?current.jugador:'',
            currentNumero:current.numero?current.numero:'',
            indexAmonestacion:current.indexAmonestacion?current.indexAmonestacion:'',
            currentPartido:current.partido?current.partido:'',
            jornada: current.jornada ? current.jornada: null,
            currentPartidoObj:current.partidoObj?current.partidoObj:null,
            currentEquipo:current.equipo?current.equipo:'',
            currentHasFecha:current.hasFecha?current.hasFecha:false,
            currentFechaSancion:current.fechaSancion?current.fechaSancion:'',
            currentPartidosSancion:current.partidosSancion?current.partidosSancion:'',
            currentMotivo:current.motivo?current.motivo:'',
            currentTipo:current.tipo?current.tipo:'',
        })
    }
    handleChangeAmonestar =(e, checked) => {
        const value = e.target.value;
        
        this.setState({indexAmonestacion: value})
    }
    handleChangePartido = (e, index, currentPartido) => {
        const {partidos} = this.state;
        const currentPartidoObj = partidos.filter(obj => obj[0] === currentPartido)[0];
        if(currentPartidoObj){
            this.setState({currentPartido, currentPartidoObj})
        }
    }
    deleteCurrent = () =>{
        const {key} = this.state;
        const keyCampeonato = this.props.match.params.keyCampeonato;
        if(keyCampeonato && key) {
            campeonatosRef.child(keyCampeonato+'/amonestaciones/'+key +'/').remove();
            this.back();
            this.startOver();
        }
    }
    handleEditCurrent = () =>{
        this.setState({isLoading:true});
        const keyCampeonato = this.props.match.params.keyCampeonato;
        if(keyCampeonato) {
            if(this.state.addingNewone===true){
                const key = campeonatosRef.child(keyCampeonato+'/amonestaciones/').push().key;
                let newAmonestacion = {
                    jugador:this.state.currentJugador,
                    numero: this.state.currentNumero,
                    indexAmonestacion:this.state.indexAmonestacion,
                    tipo: this.state.currentTipo,
                    borraSanciones: this.state.borraSanciones,
                    motivo: this.state.currentMotivo,
                    partido: this.state.currentPartido,
                    equipo: this.state.currentEquipo,
                    hasFecha: this.state.currentHasFecha,
                    jornada: this.state.jornada,
                    fechaSancion: this.state.currentFechaSancion,
                    partidosSancion: this.state.currentPartidosSancion,
                    sancionado: true,
                    key
                }
                campeonatosRef.child(keyCampeonato+'/amonestaciones/'+key).set(newAmonestacion);
                if(this.state.indexAmonestacion !== ''){
                    campeonatosRef.child(keyCampeonato+'/partidos/'+this.state.currentPartido+'/amonestaciones/').once('value', snapshot => {
                        const amonestations = Object.entries(snapshot.toJSON());
                        const amonestation = amonestations.filter(obj => obj[1].key === this.state.indexAmonestacion)[0];
                        if (amonestation) {
                            campeonatosRef.child(keyCampeonato+'/partidos/'+this.state.currentPartido+'/amonestaciones/'+amonestation[0]+'/sancionado/').set(true);
                        }
                    })
                }
            }
            else{
                const {key} = this.state;
                let editingAmonestacion = {
                    jugador:this.state.currentJugador,
                    numero:this.state.currentNumero,
                    jornada: this.state.jornada,
                    borraSanciones: this.state.borraSanciones,
                    indexAmonestacion:this.state.indexAmonestacion,
                    tipo: this.state.currentTipo,
                    motivo: this.state.currentMotivo,
                    partido: this.state.currentPartido,
                    equipo: this.state.currentEquipo,
                    fechaSancion: this.state.currentFechaSancion,
                    hasFecha: this.state.currentHasFecha,
                    partidosSancion: this.state.currentPartidosSancion,
                    sancionado: true,
                    key
                };
                campeonatosRef.child(keyCampeonato+'/amonestaciones/'+key).set(editingAmonestacion);
                if (editingAmonestacion.borraSanciones === true) {
                    campeonatosRef.child(keyCampeonato+'/amonestaciones/').once('value', snapshot =>{
                        const amonestacionesObj = snapshot.toJSON();
                        const amonestaciones = Object.values(amonestacionesObj);
                        const existentes = amonestaciones.filter(obj => obj.jugador === editingAmonestacion.jugador);
                        if (existentes) {
                            const amarillas = existentes.filter(obj => obj.tipo === 'Tarjeta amarilla');
                            const acumuladas = amarillas.length;
                            if (acumuladas >= 3) {
                                amarillas.forEach(obj => {
                                    campeonatosRef.child(keyCampeonato+'/amonestaciones/'+obj.key).remove();
                                })
                            }
                        }
                    });
                }
            }
            this.back();
            this.startOver();
        }
    }
    addNew = () =>{
        this.setState({isEditing:true,addingNewone:true});
    }
    renderAmonestaciones = () => {
        const {currentPartidoObj, indexAmonestacion} = this.state;
        if(currentPartidoObj){
            if(currentPartidoObj[1].amonestaciones){
                const amonestaciones =  Object.values(currentPartidoObj[1].amonestaciones).map((element,index)=>{
                    return(
                        <div key={index}>
                            <Checkbox
                                value={element.key}
                                label="Sancionar"
                                disabled={element.sancionado === true}
                                onCheck={this.handleChangeAmonestar}
                                checked={element.sancionado === true ? true : indexAmonestacion.toString() === element.key.toString() ? true : false}
                            />
                            <div><b>Equipo: </b>{element.equipo.nombre}</div>
                            <div><b>Jugador: </b>{element.número.nombre + ' - #'+element.número.número}</div>
                            <div>
                                <b>Tipo: </b>{element.tipo} 
                                <img src={element.tipo==='TR'?amonestacion:element.tipo==='TRD'?redCard:amonestacion} alt="amonestacion" style={{width:20, height:20, display: 'inline-block'}}/>
                                {
                                    element.tipo==='TA2 (TR)' &&
                                        <div style={{display: 'inline-block'}}>
                                            <img src={amonestacion} alt="amonestacion" style={{width:20, height:20, display: 'inline-block'}}/>
                                            <img src={redCard} alt="amonestacion" style={{width:20, height:20, display: 'inline-block'}}/>
                                        </div>
                                }
                            </div>
                            <div><b>Motivo: </b>{element.descripción}</div>
                            <br /><br />
                        </div>
                    )
                });
                return (
                    <TableRow >
                        <TableRowColumn className="smallWidth">Amonestaciones</TableRowColumn>
                        <TableRowColumn>
                            {amonestaciones}
                        </TableRowColumn>
                    </TableRow>
                )
            }
        }
        return null;
    
    }
    handleChangeJugador = (e,index, currentJugador, currentPartidoObj, currentEquipo) => {
        const currentNumero = Object.values(currentPartidoObj[1][currentEquipo].jugadores).filter(item => item.nombre === currentJugador)[0];
        this.setState({currentJugador, currentNumero});

    }
    renderPosiblePlayers = () => {
        const {currentPartidoObj, currentEquipo} = this.state;
        if(currentPartidoObj && currentEquipo) {
            if (currentPartidoObj) {
                if (currentPartidoObj[1]) {
                    if (currentPartidoObj[1][currentEquipo]) {
                        if(currentPartidoObj[1][currentEquipo].jugadores) {
                            const items =  Object.values(currentPartidoObj[1][currentEquipo].jugadores).map((item, index) => {
                                return (
                                    <MenuItem key={index} value={item.nombre} primaryText={item.nombre + ' #'+item.número} />
                                )
                            });
                            if(items.length > 0){
                                return (
                                    <SelectField
                                        fullWidth={true}
                                        floatingLabelText="Jugador"
                                        value={this.state.currentJugador}
                                        onChange={(e, index, currentJugador) => this.handleChangeJugador(e, index, currentJugador, currentPartidoObj, currentEquipo)}
                                    >
                                    {items}
                                    </SelectField>
                                )
                            }
                        }
                    }
                }
            } 
        }
        return null;
    }

    render() {
        const {amonestaciones, partidos, isLoading, authorized, addingNewone, jornada} = this.state;
        
        
        
        let dialogButton = this.state.currentJugador!==''?
        this.state.currentPartido !== '' ? 
        this.state.currentEquipo !== '' ? 
        this.state.currentTipo !== ''? 
        this.state.currentMotivo !== '' ?
            <Dialogo btnLabel="Guardar" btnType="raised"  dialogText = "Está seguro que desea guardar esta información?" confirm={this.handleEditCurrent} />:'':'':'' : '' : '';
        let dialogButtonDelete = <Dialogo btnLabel="Eliminar" deleteButton={true} dialogText = "Está seguro que desea eliminar esta información?" confirm={this.deleteCurrent} />
        return isLoading === true ? <div className="content"><CircularProgress color={green400} size={100} thickness={7} /></div> :
         authorized === false ?  
            <div>No tiene permiso para acceder a este módulo</div>
        :(
            <div className="App">
                <PageTitle 
                    title="Tu Fútbol"
                    isLoggedIn={true}
                    hasSettingsMenu ={false}
                />
                <br />
                <div style={{width: '100%'}}>
                    <Paper className="paperCanchas" zDepth={2} style={{width: '100%'}}>
                    <Toolbar style={{width: '100%',  maxWidth: '100%'}}>
                        <ToolbarGroup>
                        {
                            this.state.isEditing === true ?
                            <IconButton iconClassName="material-icons" style={{textAlign:'left',color:'#000'}} tooltip="Volver" onClick={this.back}>arrow_back_ios</IconButton>
                            :
                            <IconButton style={{textAlign:'left',color:'#000'}} tooltip="Atras" iconClassName="material-icons" onClick={()=>window.history.back()}>arrow_back_ios</IconButton>
                        }
                            <ToolbarTitle text={this.state.title} style={{color:'#000'}} className="toolbarTitle"/>
                        </ToolbarGroup>
                        {
                            this.state.isEditing === false ?
                                <IconButton iconClassName="material-icons" style={{textAlign:'right',color:'#000'}} tooltip="Agregar" onClick={this.addNew}>add</IconButton>
                            :
                                <div>
                                {
                                    this.state.current!==null && dialogButtonDelete
                                }
                                </div>
                        }
                    </Toolbar>
                    <br />
                    {
                        this.state.isEditing === false ?
                            <ReactTable 
                                data={amonestaciones}
                                defaultPageSize={10}
                                previousText= 'Anterior'
                                nextText= 'Siguiente'
                                loadingText= 'Cargando...'
                                noDataText= 'No hay datos'
                                pageText= 'Página'
                                ofText= 'de'
                                rowsText= 'Filas'
                                pageJumpText= 'Ir a página'
                                rowsSelectorText= 'filas por página'
                                filterable 
                                defaultFilterMethod={(filter, row) =>String(row[filter.id]).toLowerCase().includes( filter.value.toLowerCase())}
                                columns={[
                                {
                                    columns: [
                                    {
                                        Header: "Jornada",
                                        accessor: "jornada",
                                        width:105,
                                        sortMethod: (a,b) => {
                                            return a.numero > b.numero ? 1 : -1
                                        },
                                        Cell: row => row.original.jornada ? row.original.jornada.numero ? row.original.jornada.numero : '' : ''
                                    },
                                    {
                                        Header: "Fecha",
                                        accessor: "fecha",
                                        width: 120,
                                    },
                                    {
                                        Header: "Jugador",
                                        accessor: "jugador"
                                    },
                                    {
                                        Header: "Número",
                                        accessor: "número",
                                        width:85,
                                        sortable: false
                                    },
                                    {
                                        Header: "Equipo",
                                        accessor: "equipoAmonestado",
                                    },
                                    {
                                        Header: "Tipo",
                                        accessor: "tipo",
                                        width: 150,
                                        Cell: row => (
                                            <div>
                                                <img 
                                                    alt={row.value}
                                                    style={{width: 25, height: 25}}
                                                    src={
                                                        row.value === 'Tarjeta amarilla' ? 
                                                            amonestacion 
                                                        : row.value === 'Tarjeta roja' ?
                                                            redCard 
                                                        : row.value === 'Pendiente' ?
                                                            warning
                                                        :
                                                            cancel
                                                    }
                                                />
                                                <br />
                                                <span>{row.value}</span>
                                            </div>
                                        )
                                    },
                                    {
                                        Header: "Partido",
                                        accessor: "partidoObj",
                                        Cell: row => (
                                            <span>{row.value && row.value[1].local.nombre + ' vs ' + row.value[1].visitante.nombre}</span>
                                        )
                                    },
                                    {
                                        Header:'Detalle',
                                        accessor:'key',
                                        width:85,
                                        Cell: row => (
                                            <IconButton iconClassName="material-icons" onClick={()=>this.handleEdit(row.original,row.original.key)}>edit</IconButton>
                                        )
                                    }
                                ]
                                }
                                ]}
                                className={"-striped -highlight table"}
                            />
                        :
                        <div>
                        <Table bodyStyle={{overflow:'visible'}}  selectable={false}>
                            <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
                                <TableRow>
                                    <TableHeaderColumn className="smallWidth">Info</TableHeaderColumn>
                                    <TableHeaderColumn>Dato</TableHeaderColumn>
                                </TableRow>
                            </TableHeader>
                            <TableBody displayRowCheckbox={false}>
                                {
                                    this.state.currentPartido !== '' &&
                                    <TableRow >
                                        <TableRowColumn className="smallWidth">Sancionar partido</TableRowColumn>
                                        <TableRowColumn>
                                            <Link to={"/partidos/"+this.props.match.params.keyCampeonato} ><RaisedButton label="Ir a sancionar partido" /></Link>
                                        </TableRowColumn>
                                    </TableRow>
                                }
                                <TableRow>
                                    <TableRowColumn className="smallWidth">Jornada</TableRowColumn>
                                    <TableRowColumn>
                                        <SelectField
                                            fullWidth={true}
                                            floatingLabelText="Jornada"
                                            value={this.state.jornada === null ? null:this.state.jornada.key}
                                            onChange={this.handleChangeJornada}
                                        >
                                            {this.state.jornadas.length > 0 && this.state.jornadas.map((item, index) => (
                                                <MenuItem value={item.key} primaryText={'#'+item.numero + '  (desde: '+item.desde + ' - hasta: '+ item.hasta+')'} key={index}/>
                                            ))}
                                        </SelectField>
                                    </TableRowColumn>
                                </TableRow>
                                {
                                    this.state.jornada !== null &&
                                    <TableRow >
                                        <TableRowColumn className="smallWidth">Partido</TableRowColumn>
                                        <TableRowColumn>
                                            <SelectField
                                                fullWidth={true}
                                                floatingLabelText="Partido"
                                                value={this.state.currentPartido}
                                                onChange={this.handleChangePartido}
                                            >
                                                {
                                                    partidos&& partidos.length>0&& partidos.filter(obj => obj[1].jornada).filter(obj => obj[0] !== null && obj[0] !== 'null' && obj[1].jornada.key === jornada.key).map((item, index) =>(
                                                        <MenuItem 
                                                            key={index} 
                                                            value={item[0]} 
                                                            primaryText={
                                                                item[1].local ?
                                                                    item[1].visitante ? 
                                                                        item[1].local.nombre + ' vs ' + item[1].visitante.nombre
                                                                    : ''
                                                                : ''
                                                            } 
                                                        />
                                                    ))
                                                }
                                            </SelectField>
                                        </TableRowColumn>
                                    </TableRow>
                                }
                                {addingNewone && this.state.currentPartido !== '' &&this.renderAmonestaciones()}
                                {
                                    this.state.currentPartido !== '' &&
                                    <TableRow >
                                        <TableRowColumn className="smallWidth">Equipo</TableRowColumn>
                                        <TableRowColumn>
                                            <SelectField
                                                fullWidth={true}
                                                floatingLabelText="Equipo"
                                                value={this.state.currentEquipo}
                                                onChange={(e,index, currentEquipo)=>this.setState({currentEquipo})}
                                            >
                                                <MenuItem value={'local'} primaryText={this.state.currentPartidoObj[1].local.nombre} />
                                                <MenuItem value={'visitante'} primaryText={this.state.currentPartidoObj[1].visitante.nombre} />
                                            </SelectField>
                                        </TableRowColumn>
                                    </TableRow>
                                }
                                
                                {
                                    this.state.currentEquipo !== '' &&
                                    <TableRow >
                                        <TableRowColumn className="smallWidth">Jugador</TableRowColumn>
                                        <TableRowColumn>
                                            {this.renderPosiblePlayers()}
                                        </TableRowColumn>
                                    </TableRow>
                                }
                                {
                                    this.state.currentJugador !== '' &&
                                    <TableRow >
                                        <TableRowColumn className="smallWidth">Tipo</TableRowColumn>
                                        <TableRowColumn>
                                            <SelectField
                                                fullWidth={true}
                                                floatingLabelText="Tipo"
                                                value={this.state.currentTipo}
                                                onChange={(e,index, currentTipo)=>this.setState({currentTipo})}
                                            >
                                                <MenuItem value={'Tarjeta amarilla'} primaryText={'Tarjeta amarilla'} />
                                                <MenuItem value={'Tarjeta roja'} primaryText={'Tarjeta roja'} />
                                                <MenuItem value={'Sanción'} primaryText={'Sanción'} />
                                            </SelectField>
                                        </TableRowColumn>
                                    </TableRow>
                                }
                                {
                                    this.state.currentTipo === 'Sanción' &&
                                    <TableRow >
                                        <TableRowColumn className="smallWidth">Tiempo</TableRowColumn>
                                        <TableRowColumn>
                                            <div>
                                                <Toggle
                                                    label="Fecha"
                                                    onToggle = {(event,currentHasFecha)=>this.setState({currentHasFecha:currentHasFecha})}
                                                    defaultToggled={this.state.currentHasFecha}
                                                />
                                                <br />
                                                {
                                                    this.state.currentHasFecha ? 
                                                        <DatePicker 
                                                            hintText="Sanción hasta" 
                                                            container="inline"
                                                            fullWidth={true}
                                                            value={this.state.currentFechaSancion}
                                                            onChange={(event, currentFechaSancion) => this.setState({currentFechaSancion})}
                                                        />
                                                    :
                                                        <SelectField
                                                            fullWidth={true}
                                                            floatingLabelText="Número de partidos de sanción"
                                                            value={this.state.currentPartidosSancion}
                                                            onChange={(e,index, currentPartidosSancion)=>this.setState({currentPartidosSancion})}
                                                        >
                                                            <MenuItem value={1} primaryText={1} />
                                                            <MenuItem value={2} primaryText={2} />
                                                            <MenuItem value={3} primaryText={3} />
                                                            <MenuItem value={4} primaryText={4} />
                                                            <MenuItem value={5} primaryText={5} />
                                                            <MenuItem value={6} primaryText={6} />
                                                            <MenuItem value={7} primaryText={7} />
                                                            <MenuItem value={8} primaryText={8} />
                                                            <MenuItem value={9} primaryText={9} />
                                                            <MenuItem value={10} primaryText={10} />
                                                            <MenuItem value={11} primaryText={11} />
                                                            <MenuItem value={12} primaryText={12} />
                                                            <MenuItem value={13} primaryText={13} />
                                                            <MenuItem value={14} primaryText={14} />
                                                            <MenuItem value={15} primaryText={15} />
                                                            <MenuItem value={16} primaryText={16} />
                                                            <MenuItem value={17} primaryText={17} />
                                                            <MenuItem value={18} primaryText={18} />
                                                            <MenuItem value={19} primaryText={19} />
                                                            <MenuItem value={20} primaryText={20} />
                                                            <MenuItem value={21} primaryText={21} />
                                                            <MenuItem value={22} primaryText={22} />
                                                            <MenuItem value={23} primaryText={23} />
                                                            <MenuItem value={24} primaryText={24} />
                                                            <MenuItem value={25} primaryText={25} />
                                                        </SelectField>
                                                }
                                            </div>
                                        </TableRowColumn>
                                    </TableRow>
                                }
                                {
                                    this.state.currentJugador !== '' &&
                                    <TableRow >
                                        <TableRowColumn className="smallWidth">Motivo</TableRowColumn>
                                        <TableRowColumn>
                                            <TextField
                                                fullWidth
                                                floatingLabelText="Ingrese el motivo de la sanción"
                                                multiLine={true}
                                                rows={4}
                                                value={this.state.currentMotivo}
                                                onChange={e=>this.setState({currentMotivo: e.target.value})}
                                            />
                                        </TableRowColumn>
                                    </TableRow>
                                }
                            </TableBody>
                        </Table>
                        {dialogButton}
                        <br />
                        <br />
                        <br />
                        <br />
                        </div>
                    }
                    </Paper>
                </div>
            </div>
        )
    }
}
