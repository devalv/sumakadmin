import React from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import IconButton from 'material-ui/IconButton';
import RaisedButton from 'material-ui/RaisedButton';

export default class Dialogo extends React.Component {
  state = {
    open: false,
  };

  handleOpen = () => {
    this.setState({open: true});
  };

  handleClose = () => {
    this.setState({open: false});
    this.props.confirm();
  };
  handleCancel = () => {
    this.setState({open: false});
  };
  render() {
    const actions = [
      <FlatButton
        label="Cancelar"
        secondary={true}
        onClick={this.handleCancel}
      />,
      <FlatButton
        label="Aceptar"
        secondary={true}
        onClick={this.handleClose}
      />,
    ];

    return (
      <div>
      {
        this.props.deleteButton === true?
        <IconButton iconClassName="material-icons" style={{textAlign:'right',color:'#000'}} tooltip="Eliminar" onClick={this.handleOpen}>delete</IconButton>
      :
        <div>
        {
          this.props.btnType==="raised"?
            <RaisedButton style={{marginTop: 20, marginBottom: 20}} label={this.props.btnLabel} labelPosition="before" icon={this.props.icon} onClick={this.handleOpen} />
          :
            <FlatButton style={{marginTop: 20, marginBottom: 20}} label={this.props.btnLabel} labelPosition="before" icon={this.props.icon} onClick={this.handleOpen} />
        }
        </div>
      }
        <Dialog
          title="Confirmar"
          actions={actions}
          modal={true}
          
          open={this.state.open}
        >
          {this.props.dialogText}
        </Dialog>
      </div>
    );
  }
}
