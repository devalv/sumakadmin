import {db, auth, logout} from './Firebase.js';
import moment from 'moment';

const escapeRegExp = (str) => str.replace(/[-[\]/{}()*+?.\\^$|]/g, '\\$&');
const chars = '.$[]#/%'.split('');
const charCodes = chars.map((c) => `%${c.charCodeAt(0).toString(16).toUpperCase()}`);
const charToCode = {};
const codeToChar = {};
chars.forEach((c, i) => {
  charToCode[c] = charCodes[i];
  codeToChar[charCodes[i]] = c;
});
const charsRegex = new RegExp(`[${escapeRegExp(chars.join(''))}]`, 'g');
// const charCodesRegex = new RegExp(charCodes.join('|'), 'g');
export const encode = (str) => str.replace(charsRegex, (match) => charToCode[match]);
// const decode = (str) => str.replace(charCodesRegex, (match) => codeToChar[match]);
export const usersRef = db.ref('users');
export const ligasRef = db.ref('ligas');
export const campeonatosRef = db.ref('campeonatos');
export const logRef = db.ref('log');

export const resetPasswordEmail = (mail) => {
    return auth.sendPasswordResetEmail(mail);
}

export function isNumber(number) {
    if (number) {
      if (number === '') {
        return true;
      }
          
      const re = /^[0-9.]+$/;
      const test = re.test(String(number));
      return test;
    }
      
    return true;
  }
export function getCampeonatos(){
    let ref=db.ref('campeonatos/');
    return ref.once('value',snapshot=>{
        return snapshot.toJSON(); 
    });
}
export function getLogs(){
    let ref=db.ref('log/');
    return ref.once('value',snapshot=>{
        return snapshot.toJSON(); 
    });
}

export function getCampeonato(key){
    let ref=db.ref('campeonatos/'+key);
    return ref.once('value',snapshot=>{
        return snapshot.toJSON(); 
    });
}
export function addGol(gol,keyCampeonato,key){
    let ref=db.ref('campeonatos/'+keyCampeonato+'/partidos/'+key+'/goles/');
    try{
        ref.push(gol);
        let reff = db.ref('campeonatos/'+keyCampeonato+'/partidos/'+key+'/resultado/');
        reff.set(gol.resultado);
        return true;
    }
    catch(e){
        
        return false;
    }
}

export const finishOtherMatches = (keyCampeonato, keyPartido) => {
    console.log('entré');
    let ref=db.ref('campeonatos/'+keyCampeonato);
    ref.on('value', snapshot => {
        const campeonato = snapshot.toJSON();
        const canchas = Number(campeonato.canchas)
        if (canchas) {
            let partidos = Object.entries(campeonato.partidos).filter(obj => obj[0] !== keyPartido);
            if (partidos.length > 0) {
                partidos = partidos.sort((a,b) => (Number(a[1].dateModified) > Number(b[1].dateModified)) ? 1 : ((Number(b[1].dateModified) > Number(a.dateModified)) ? -1 : 0)); 
                console.log('canchas: ', canchas);
                for (let i = 0; i < canchas; i += 1) {
                    console.log('en el bucle '+ i);
                    console.log(i, partidos[i][0]);
                    if (partidos[i][1].jugando === true) {
                        console.log('modificando');
                        db.ref('campeonatos/'+keyCampeonato+'/partidos/'+partidos[i][0]+'/jugado/').set(true);
                        db.ref('campeonatos/'+keyCampeonato+'/partidos/'+partidos[i][0]+'/jugando/').set(false);
                    }
                }
            }
        }
    })
}

export function addAmonestacion(amonestacion,keyCampeonato,key, jornada){
    let ref=db.ref('campeonatos/'+keyCampeonato+'/partidos/'+key+'/amonestaciones/');
    const pushkey = ref.push().key;
    amonestacion.key = pushkey;
    amonestacion.jornada = jornada;
    try{
        ref.child(pushkey).set(amonestacion);
        db.ref('campeonatos/'+keyCampeonato+'/amonestaciones').once('value', snapshot =>{
            const amonestacionesObj = snapshot.toJSON();
            if (amonestacionesObj) {
                const amonestaciones = Object.values(amonestacionesObj);
                const existentes = amonestaciones.filter(obj => obj.jugador === amonestacion.número.nombre);
                if (existentes) {
                    const amarillas = existentes.filter(obj => obj.tipo === 'Tarjeta amarilla');
                    const acumuladas = amarillas.length;
                    if (acumuladas >= 3) {
                        const keySancion = db.ref('campeonatos/'+keyCampeonato+'/amonestaciones').push().key;
                        const sancionPorAmarillasAcumuladas = {
                            jugador: amonestacion.número.nombre,
                            numero: amonestacion.número.número,
                            jornada,
                            indexAmonestacion: pushkey,
                            tipo: 'Pendiente',
                            motivo: '4 Amarillas acumuladas',
                            partido: key,
                            equipo: amonestacion.equip,
                            fechaSancion: moment().format('YYYY-MM-DD'),
                            hasFecha: false,
                            partidosSancion: 1,
                            sancionado: true,
                            key: keySancion,
                            borraSanciones: true,
                        }
                        db.ref('campeonatos/'+keyCampeonato+'/amonestaciones/'+keySancion).set(sancionPorAmarillasAcumuladas);
                    }
                }
            }
        });
        return true;

    }
    catch(e){
        
        return false;
    }
}
export function getPartido(keyCampeonato,key){
    let ref=db.ref('campeonatos/'+keyCampeonato+'/partidos/'+key);
    return ref.once('value',snapshot=>{
        return snapshot.toJSON(); 
    });
}
export function timeConverter(UNIX_timestamp){
    var a = new Date(UNIX_timestamp);
    var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    var year = a.getFullYear();
    var month = months[a.getMonth()];
    var date = a.getDate();
    var hour = a.getHours();
    var min = a.getMinutes();
    var sec = a.getSeconds();
    var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec ;
    return time;
} 
export function getLigas(){
    let ref=db.ref('ligas');
    return ref.once('value',snapshot=>{
        return snapshot.toJSON(); 
    });
}
export function getBarriales() {
    let ref=db.ref('ligas');
    return ref.once('value',snapshot=>{
        return snapshot.toJSON(); 
    });
}
export function getEquipos(keyLiga){
    let stringRef = 'ligas/'+keyLiga+'/equipos/';
    let ref=db.ref(stringRef);
    return ref.once('value',snapshot=>{
        return snapshot.toJSON(); 
    });
}
export function getPartidos(keyCampeonato){
    let stringRef = 'campeonatos/'+keyCampeonato+'/partidos/';
    let ref=db.ref(stringRef);
    return ref.once('value',snapshot=>{
        return snapshot.toJSON(); 
    });
}
export function getEquipoDetail(keyLiga,keyEquipo){
    let stringRef = 'ligas/'+keyLiga+'/equipos/'+encode(keyEquipo);
    let ref=db.ref(stringRef);
    return ref.once('value',snapshot=>{
        return snapshot.toJSON(); 
    });
}
export function getProvincias(){
    let ref=db.ref('provincias');
    return ref.once('value',snapshot=>{
        return snapshot.toJSON(); 
    });
}
export function getCategorías(){
    let ref=db.ref('categorías');
    return ref.once('value',snapshot=>{
        return snapshot.toJSON(); 
    });
}
export function getPublicidad(){
    let ref=db.ref('publicidad');
    return ref.once('value',snapshot=>{
        return snapshot.toJSON(); 
    });
}
export function getUsers(){
    let ref=db.ref('users');
    return ref.once('value',snapshot=>{
        return snapshot.toJSON(); 
    });
}

export function editCategoría(editing,key){
    let ref=db.ref('categorías/'+key);
    try{
        ref.set(editing);
        return true;
    }
    catch(e){
        
        return false;
    }
}

export function editPublicidad(editing,key){
    let ref=db.ref('publicidad/'+key);
    try{
        ref.set(editing);
        return true;
    }
    catch(e){
        
        return false;
    }
}

export function editUser(editing,key){
    let ref=db.ref('users/'+key);
    try{
        ref.set(editing);
        return true;
    }
    catch(e){
        console.log('e: ', e);
        
        return false;
    }
}

export function addCategoría(toAdd){
    let ref=db.ref('categorías/'+encode(toAdd.nombre));
    try{
        ref.set(toAdd);
        return true;
    }
    catch(e){
        return false;
    }
}
export function addPublicidad(toAdd){
    let ref=db.ref('publicidad/'+encode(toAdd.nombre));
    try{
        ref.set(toAdd);
        return true;
    }
    catch(e){
        return false;
    }
}
export function campeonatoExisteEnLiga(campeonato, liga) {
    return db.ref('campeonatos/'+campeonato + '/liga/'+liga).on('value', snapshot=> {
        return snapshot.toJSON();
    })
}
export function addUser(toAdd){
    const rol = sessionStorage.getItem('rol');
    const ligasAsignadas = sessionStorage.getItem('ligasAsignadas');
    const campeonatosAsignados = sessionStorage.getItem('campeonatosAsignados');
    const e = sessionStorage.getItem('e');
    const p = sessionStorage.getItem('p');
    if( e && p) {
        try{
            return auth.createUserWithEmailAndPassword(toAdd.email, toAdd.password).then(value=>{
                let ref=db.ref('users/'+value.user.uid);
                toAdd.uid = value.user.uid;
                ref.set(toAdd);
                return logout().then(val=>{
                    return auth.signInWithEmailAndPassword(e, p).then(data=>{
                        let uid = data.user.uid;
                        sessionStorage.setItem('uid', uid);
                        sessionStorage.setItem('rol', rol);
                        sessionStorage.setItem('ligasAsignadas', ligasAsignadas);
                        sessionStorage.setItem('campeonatosAsignados', campeonatosAsignados);
                        sessionStorage.setItem('e',e);
                        sessionStorage.setItem('p',p);
                        alert('Usuario agregado correctamente');
                    }).catch(er=>{
                        
                        alert(er.toString())
                        return false;
                    })
                }).catch(err=>{
                    alert(err.toString())
                    
                    return false;
                })
            }).catch(error=>{
                alert(error.toString())
                
                return false;
            })
        }
        catch(e){
            return false;
        }
    }
}
export function sancionarPartido (keyCampeonato,keyPartido,equipo,descripción){
    
    
    try{
        let sancion={
            equipo,
            descripción: descripción? descripción: 'Sin descripción'
        }
        
        let ref=db.ref('campeonatos/'+keyCampeonato+'/partidos/'+keyPartido+'/sancionado/');
        ref.set(sancion);
        let refff=db.ref('campeonatos/'+keyCampeonato+'/partidos/'+keyPartido+'/jugado/');
        refff.set(true);
        let reefff=db.ref('campeonatos/'+keyCampeonato+'/partidos/'+keyPartido+'/jugando/');
        reefff.set(true);
        return true;
    }   
    catch(e){
        return false;
    }
}
export function quitarSancion (keyCampeonato,keyPartido,equipo,descripción){
    try{
        let ref=db.ref('campeonatos/'+keyCampeonato+'/partidos/'+keyPartido+'/sancionado/');
        ref.remove();
        let refff=db.ref('campeonatos/'+keyCampeonato+'/partidos/'+keyPartido+'/jugado/');
        refff.set(true);
        let reefff=db.ref('campeonatos/'+keyCampeonato+'/partidos/'+keyPartido+'/jugando/');
        reefff.set(true);
        return true;
    }   
    catch(e){
        return false;
    }
}
export function deleteGol(gol,keyCampeonato,keyPartido){
    
    
    let ref=db.ref('campeonatos/'+keyCampeonato+'/partidos/'+keyPartido+'/goles/');
    try{
        ref.once('value',snapshot=>{
            let val = Object.entries(snapshot.toJSON());
            for(var i = 0;i<val.length;i++){
                if(val[i][1].descripción === gol.descripción && val[i][1].equip=== gol.equip && val[i][1].minuto === gol.minuto && val[i][1].número.número === gol.número.número){
                    let rf = db.ref('campeonatos/'+keyCampeonato+'/partidos/'+keyPartido+'/goles/'+val[i][0]);
                    rf.remove();
                    return true;
                }
            }
        })
        return false;
    }
    catch(e){
        return false;
    }
}
export function deleteAmonestacion(amonestacion,keyCampeonato,keyPartido){
    
    let ref=db.ref('campeonatos/'+keyCampeonato+'/partidos/'+keyPartido+'/amonestaciones/');
    try{
        ref.once('value',snapshot=>{
            let val = Object.entries(snapshot.toJSON());
            for(var i = 0;i<val.length;i++){
                if(val[i][1].key === amonestacion.key){
                    let rf = db.ref('campeonatos/'+keyCampeonato+'/partidos/'+keyPartido+'/amonestacion/'+val[i][0]);
                    rf.remove();
                    return true;
                }
            }
        })
        return false;
    }
    catch(e){
        return false;
    }
}
export function deletePublicidad(deleteKey){
    try{
        db.ref('publicidad/'+deleteKey).remove();
        return true;
    }
    catch(e){
        return false;
    }
}
export function deleteCategory(deleteKey){
    try{
        db.ref('categorías/'+deleteKey).remove();
        return true;
    }
    catch(e){
        return false;
    }
}
export function deleteUser(deleteKey){
    try{
        db.ref('users/'+deleteKey).remove();
        return true;
    }
    catch(e){
        return false;
    }
}
export function editEquipo(keyLiga,editing,key){
    
    let ref=db.ref('ligas/'+keyLiga+'/equipos/'+key);
    try{
        ref.set(editing);
        return true;
    }
    catch(e){
        
        return false;
    }
}

export function addEquipo(keyLiga,toAdd){
    let ref=db.ref('ligas/'+keyLiga+'/equipos/'+encode(toAdd.nombre));
    try{
        ref.set(toAdd);
        return true;
    }
    catch(e){
        return false;
    }
}
export function deleteEquipo(keyLiga,deleteKey){
    try{
        db.ref('ligas/'+keyLiga+'/equipos/'+deleteKey).remove();
        return true;
    }
    catch(e){
        return false;
    }
}
export function editPartido(keyLiga,editing,key){
    
    let ref=db.ref('campeonatos/'+keyLiga+'/partidos/'+key);
    try{
        ref.set(editing);
        return true;
    }
    catch(e){
        
        return false;
    }
}
export function notificationBegins(begin){
    let ref=db.ref('partidoComienza/');
    try{
        ref.push(begin);
        return true;
    }
    catch(e){
        return false;
    }
}
export function notificationGol(editing){
    let ref=db.ref('notificacionGol/');
    try{
        ref.push(editing);
        return true;
    }
    catch(e){
        return false;
    }
}
export function notificationFinish(begin){
    let ref=db.ref('partidoTermina/');
    try{
        ref.push(begin);
        return true;
    }
    catch(e){
        return false;
    }
}
export function addPartido(keyLiga,toAdd){
    let ref=db.ref('campeonatos/'+keyLiga+'/partidos/'+encode(toAdd.local.nombre)+'vs'+encode(toAdd.visitante.nombre)+toAdd.dateAdded);
    try{
        ref.set(toAdd);
        return true;
    }
    catch(e){
        return false;
    }
}
export function deletePartido(keyLiga,deleteKey){
    try{
        db.ref('campeonatos/'+keyLiga+'/partidos/'+deleteKey).remove();
        return true;
    }
    catch(e){
        return false;
    }
}
export function editJugador(keyLiga,keyEquipo,editing,key){
    let ref=db.ref('ligas/'+keyLiga+'/equipos/'+encode(keyEquipo)+'/jugadores/'+key);
    try{
        ref.set(editing);
        return true;
    }
    catch(e){
        
        return false;
    }
}

export function addJugador(keyLiga,keyEquipo,toAdd){
    let ref=db.ref('ligas/'+keyLiga+'/equipos/'+encode(keyEquipo)+'/jugadores/'+encode(toAdd.nombre));
    try{
        ref.set(toAdd);
        return true;
    }
    catch(e){
        return false;
    }
}
export function deleteJugador(keyLiga,keyEquipo,deleteKey){
    try{
        db.ref('ligas/'+keyLiga+'/equipos/'+encode(keyEquipo)+'/jugadores/'+deleteKey).remove();
        return true;
    }
    catch(e){
        return false;
    }
}
export function editLiga(editing,key){
    let ref=db.ref('ligas/'+key);
    try{
        ref.set(editing);
        return true;
    }
    catch(e){
        
        return false;
    }
}

export function addLiga(toAdd){
    let ref=db.ref('ligas/'+encode(toAdd.nombre));
    try{
        ref.set(toAdd);
        return true;
    }
    catch(e){
        return false;
    }
}

export function deleteLiga(deleteKey){
    try{
        db.ref('ligas/'+deleteKey).remove();
        return true;
    }
    catch(e){
        return false;
    }
}
export function editCampeonato(editing,key){
    let ref=db.ref('campeonatos/'+key);
    try{
        ref.set(editing);
        return true;
    }
    catch(e){
        
        return false;
    }
}
export function deleteCampeonato(deleteKey){
    try{
        db.ref('campeonatos/'+deleteKey).remove();
        return true;
    }
    catch(e){
        return false;
    }
}
export function addCampeonato(toAdd){
    let ref=db.ref('campeonatos/'+encode(toAdd.nombre));
    try{
        ref.set(toAdd);
        return true;
    }
    catch(e){
        return false;
    }
}

export function formatDate(today){
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!

    var yyyy = today.getFullYear();
    if(dd<10){
        dd='0'+dd;
    } 
    if(mm<10){
        mm='0'+mm;
    } 
    return dd+'/'+mm+'/'+yyyy;
}

export function activateDisactivate(newValue,key){
    let ref=db.ref('campeonatos/'+key+'/activo/');
    ref.set(newValue);
}
export function activateDisactivateEquipos(newValue,key){
    let ref=db.ref('equipos/'+key+'/activo/');
    ref.set(newValue);
}
export function activateDisactivateLigas(newValue,key){
    let ref=db.ref('ligas/'+key+'/activo/');
    ref.set(newValue);
}
