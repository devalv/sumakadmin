import React from 'react';
import './Style.css';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import Paper from 'material-ui/Paper';
import {Table,TableBody,TableRow,TableRowColumn} from 'material-ui/Table';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import IconButton from 'material-ui/IconButton/IconButton';

export default class DialogoGol extends React.Component {
  state = {
    open: false,
    descripción: '',
    equipo:null,
  };
  handleOpen = () => {
    this.setState({open: true});
  };
  handleClose = () => {
    this.setState({open: false});
    this.props.saveSancion(this.state.equipo,this.state.descripción);
  };
  handleCancel = () => {
    this.setState({open: false});
  };
  componentDidMount(){
    const {sancion} = this.props;
    if(sancion){
      this.setState({descripción:sancion.descripción,equipo:sancion.equipo})
    }
    else{
      this.setState({descripción:null,equipo:null})
    }
    
  }
  removeSancion = () => {
    this.props.saveSancion(null,null);
  }
  handleChangeEquipo = (event,index,value) => {this.setState({equipo:value})}
  render() {
    const actions = this.state.equipo!==null?this.state.descripción!==''?[
      <FlatButton
        label="Cancelar"
        secondary={true}
        onClick={this.handleCancel}
      />,
      <FlatButton
        label="Aceptar"
        secondary={true}
        onClick={this.handleClose}
      />,
    ]:'':'';
    return (
      <div>
        <RaisedButton label={this.props.btnLabel} labelPosition="before" style={{marginTop: 20, marginBottom: 20}} icon={this.props.icon} onClick={this.handleOpen} />
        <Dialog
          title="Sancionar partido"
          actions={actions}
          modal={true}
          open={this.state.open}
        >
        <IconButton
            iconClassName="material-icons"
            tooltip="Salir"
            onClick = {this.handleCancel}
            className="floatRight"
          >
          close
        </IconButton>
        <Paper zDepth={1} style={{margin:5}}>
            <Table bodyStyle={{overflow:'visible'}}  selectable={false}>
            <TableBody displayRowCheckbox={false}>
                <TableRow>
                <TableRowColumn className="diagnostic">Equipo</TableRowColumn>
                <TableRowColumn >
                    <SelectField
                        fullWidth={true}
                        floatingLabelText={"Equipo"}
                        value={this.state.equipo}
                        onChange={this.handleChangeEquipo}
                    >
                        <MenuItem key={0} value={"ambos"} primaryText={'Los dos equipos'}></MenuItem>
                        <MenuItem key={1} value={"local"} primaryText={'Local'}></MenuItem>
                        <MenuItem key={2} value={"visitante"} primaryText={'Visitante'}></MenuItem>
                    </SelectField>
                </TableRowColumn>
                </TableRow>
                <TableRow>
                <TableRowColumn className="diagnostic">Descripción</TableRowColumn>
                <TableRowColumn >
                    <TextField 
                    id={'descripción'}
                    fullWidth={true}
                    value={this.state.descripción}
                    onChange={e => this.setState({ descripción: e.target.value})} 
                    />
                </TableRowColumn>
                </TableRow>
                { 
                  this.state.equipo!==null&&
                  <TableRow>
                      <TableRowColumn className="diagnostic">Quitar sanción</TableRowColumn>
                      <TableRowColumn >
                        <RaisedButton label={"Quitar Sanción"} labelPosition="before" icon={this.props.icon} onClick={this.removeSancion} />
                      </TableRowColumn>
                  </TableRow>
                }
                
            </TableBody>
            </Table>
        </Paper>  
        </Dialog>
      </div>
    );
  }
}