import firebase from 'firebase';

const config = {
    apiKey: "AIzaSyC7y7-XB3a7TZs6XD4w5IR7UWyqGDdfG7g",
    authDomain: "millenial-futbol.firebaseapp.com",
    databaseURL: "https://millenial-futbol.firebaseio.com",
    projectId: "millenial-futbol",
    storageBucket: "millenial-futbol.appspot.com",
    messagingSenderId: "811708006106"
};
export const firebaseApp = firebase.initializeApp(config);
export const db = firebaseApp.database();
export const storage = firebaseApp.storage();
export const auth = firebaseApp.auth(); 
export const storageKey = 'KEY_FOR_LOCAL_STORAGE';
export function logout(){
    sessionStorage.clear();
    return auth.signOut();
}
