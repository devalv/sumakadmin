import React from 'react';
import './Style.css';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import Paper from 'material-ui/Paper';
import {Table,TableBody,TableRow,TableRowColumn} from 'material-ui/Table';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import IconButton from 'material-ui/IconButton/IconButton';

export default class DialogoGol extends React.Component {
  state = {
    open: false,
    descripción: '',
    minuto: '',
    número:null,
    equipo:null,
    minutos:[ 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,
              31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,
              61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,
            ],
    equipoLocal:null,
    equipoVisitante:null,
    jugadoresVisitante:[],
    jugadoresLocal:[],
    equip:'',
    resultado:'0-0'
  };
  handleOpen = () => {
    this.setState({open: true});
  };

  handleClose = () => {
    this.setState({open: false});
    let resultadoArr = this.state.resultado.replace('-',',');
    var resultado = resultadoArr.split(',');
    if(this.state.equip==='local'){
      resultado[0]=Number(resultado[0])+1;
    }
    else if(this.state.equip==='visitante')
    {
      resultado[1]=Number(resultado[1])+1;
    }
    resultado = resultado.join('-');
    var gol = {
        'descripción': this.state.descripción,
        'minuto': this.state.minuto,
        'número': this.state.número,
        'equipo':this.state.equipo,
        'equip':this.state.equip,
        resultado
    }
    this.props.saveGol(gol);
  };
  handleCancel = () => {
    this.setState({open: false});
  };
   handleChangeEquipo =  (event, index, value) => {
     if(this.state.equipoLocal===value){
      this.setState({equipo:value,equip:'local'});
     }
     if(this.state.equipoVisitante===value){
      this.setState({equipo:value,equip:'visitante'});
     }
  }
  handleChangeMinuto = (event,index,value) => {this.setState({minuto:value})}
  handleChangeNúmero = (event,index,value) => {this.setState({número:value})}
  componentWillMount = () => {
    var jugadoresLocal = this.props.jugadoresLocal?this.props.jugadoresLocal:[];
    var jugadoresVisitante = this.props.jugadoresVisitante?this.props.jugadoresVisitante:[];
    var equipoLocal=this.props.equipos.local;
    var equipoVisitante=this.props.equipos.visitante;
    var resultado = this.props.resultado;
    this.setState({jugadoresLocal,jugadoresVisitante,equipoLocal,equipoVisitante,resultado});
  }
  
  render() {
    const actions = this.state.número!==null?this.state.equipo!==null?this.state.equip!==''?[
      <FlatButton
        label="Cancelar"
        secondary={true}
        onClick={this.handleCancel}
      />,
      <FlatButton
        label="Aceptar"
        secondary={true}
        onClick={this.handleClose}
      />,
    ]:'':'':'';
    let locales = Object.values(this.state.jugadoresLocal).map((element,index)=>{
      return(
        <MenuItem key={index} value={element} primaryText={element.número+' '+element.nombre}></MenuItem>
      )
    })
    let visitantes = Object.values(this.state.jugadoresVisitante).map((element,index)=>{
      return(
        <MenuItem key={index} value={element} primaryText={element.número+' '+element.nombre}></MenuItem>
      )
    })
    return (
      <div>
        <RaisedButton label={this.props.btnLabel} labelPosition="before" icon={this.props.icon} style={{marginTop: 20, marginBottom: 20}} onClick={this.handleOpen} />
        <Dialog
          title="Agregar Gol"
          actions={actions}
          modal={true}
          open={this.state.open}
        >
        <IconButton
            iconClassName="material-icons"
            tooltip="Salir"
            onClick = {this.handleCancel}
            className="floatRight"
          >
          close
        </IconButton>
        <Paper zDepth={1} style={{margin:5}}>
            <Table bodyStyle={{overflow:'visible'}}  selectable={false}>
            <TableBody displayRowCheckbox={false}>
                {/* <TableRow>
                <TableRowColumn className="diagnostic">Minuto</TableRowColumn>
                <TableRowColumn >
                    <SelectField
                        fullWidth={true}
                        floatingLabelText={"Minuto"}
                        value={this.state.minuto}
                        onChange={this.handleChangeMinuto}
                    >
                        {itemMinutos}
                    </SelectField> 
                </TableRowColumn>
                </TableRow> */}
                <TableRow>
                <TableRowColumn className="diagnostic">Descripción</TableRowColumn>
                <TableRowColumn >
                    <TextField 
                    id={'descripción'}
                    fullWidth={true}
                    value={this.state.descripción}
                    onChange={e => this.setState({ descripción: e.target.value})} 
                    />
                </TableRowColumn>
                </TableRow>
                {
                  this.state.equipoLocal!==null&&
                  <TableRow>
                      <TableRowColumn className="diagnostic">Equipo</TableRowColumn>
                      {
                        this.state.equipoVisitante!==null&&
                        <TableRowColumn >
                          <SelectField
                              fullWidth={true}
                              floatingLabelText="Equipo"
                              value={this.state.equipo}
                              onChange={this.handleChangeEquipo}
                          >
                            <MenuItem primaryText={'Local ('+this.state.equipoLocal.nombre+')'} value={this.state.equipoLocal}/>  
                            <MenuItem primaryText={'Visitante ('+this.state.equipoVisitante.nombre+')'} value={this.state.equipoVisitante} />
                          </SelectField>
                        </TableRowColumn>
                        }
                  </TableRow>
                }
                {
                  this.state.equipo!==null&&
                  <TableRow>
                  <TableRowColumn className="diagnostic">Número</TableRowColumn>
                    <TableRowColumn >
                      <SelectField
                          fullWidth={true}
                          floatingLabelText="Número"
                          value={this.state.número}
                          onChange={this.handleChangeNúmero}
                      >
                        <MenuItem value={'Sin asignar'} primaryText={'Sin asignar'} />
                        {this.state.equipo===this.state.equipoLocal ?locales:visitantes}
                      </SelectField>
                    </TableRowColumn>
                  </TableRow>
                }
            </TableBody>
            </Table>
        </Paper>  
        </Dialog>
      </div>
    );
  }
}