import React, { Component } from 'react';
import CircularProgress from 'material-ui/CircularProgress';
import './Style.css';
import Login from '../components/Login';
import Main from '../components/Main';
import Campeonatos from '../components/Campeonatos';
import Publicidad from '../components/Publicidad';
import Ligas from '../components/Ligas';
import Partidos from '../components/Partidos';
import Amonestaciones from '../components/Amonestaciones';
import Equipos from '../components/Equipos';
import Jugadores from '../components/Jugadores';
import Jornadas from '../components/Jornadas';
import Manage from '../components/Manage';
import Users from '../components/Users';
import Categorías from '../components/Categorías';
import Logs from '../components/Logs';
import {auth} from './Firebase';
import { Route, Redirect, Switch, BrowserRouter } from 'react-router-dom'
import { green400 } from 'material-ui/styles/colors';

function UserRoute ({component: Component, ...rest}) {
    return (
      <Route
        {...rest}
        render={(props) => <Component {...props} />}
      />
    )
}

function PrivateRoute ({component: Component, authed, ...rest}) {
    return (
      <Route
        {...rest}
        render={(props) => authed === true
          ? <Component {...props} />
          : <Redirect to={{pathname: '/login', state: {from: props.location}}} />}
      />
    )
}
  
  function PublicRoute ({component: Component, authed, superuser, ...rest}) {
    return (
      <Route
        {...rest}
        render={(props) => 
          authed === false? 
            <Component {...props} />
          :  
            <Redirect to='/main' />
        }
      />
    )
  }

export default class Routes extends Component {
    constructor(props){
        super(props);
        this.state={
            loading:true,
            authed:false,
            store:null,
        }
    }
    componentDidMount () {
        this.removeListener = auth.onAuthStateChanged((user) => {
          if (user) {
            this.setState({
                authed: true,
                loading: false,
            })
          } else {
            this.setState({
                authed: false,
                loading: false,
            })
          }
        })
    }
    componentWillUnmount () {
        this.removeListener()
    }
    render() {
    return this.state.loading === true ? <div className="content"><CircularProgress color={green400} size={100} thickness={7} /></div> : (
            <BrowserRouter>
                <Switch>
                    <PrivateRoute path='/' authed={this.state.authed}  exact component={Main} />
                    <PublicRoute authed={this.state.authed} path='/login' component={Login} />
                    <PrivateRoute authed={this.state.authed} path='/main' component={Main} />
                    <PrivateRoute authed={this.state.authed} path='/manage' component={Manage} />
                    <PrivateRoute authed={this.state.authed} path='/campeonatos' component={Campeonatos} />
                    <PrivateRoute authed={this.state.authed} path='/jornadas/:keyCampeonato' component={Jornadas} />
                    <PrivateRoute authed={this.state.authed} path='/partidos/:keyCampeonato' component={Partidos} />
                    <PrivateRoute authed={this.state.authed} path='/amonestaciones/:keyCampeonato' component={Amonestaciones} />
                    <PrivateRoute authed={this.state.authed} path='/ligas' component={Ligas} />
                    <PrivateRoute authed={this.state.authed} path='/equipos/:keyLiga' component={Equipos} />
                    <PrivateRoute authed={this.state.authed} path='/jugadores/:keyLiga/:keyEquipo' component={Jugadores} />
                    <PrivateRoute authed={this.state.authed} path='/categorias' component={Categorías} />
                    <PrivateRoute authed={this.state.authed} path='/publicidad' component={Publicidad} />
                    <PrivateRoute authed={this.state.authed} path='/logs' component={Logs} />
                    <UserRoute path='/usuarios' component={Users} />
                    <Route render={() => <h6>Error 404 No Encontrado</h6>} />
                </Switch>
          </BrowserRouter>
          );
  }
}
