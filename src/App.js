import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Routes from './utils/Routes.js';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import { darkBlack } from 'material-ui/styles/colors';
import { white } from 'material-ui/styles/colors';
import { green400, green300 } from 'material-ui/styles/colors';
import { grey300 } from 'material-ui/styles/colors';
import { fullBlack } from 'material-ui/styles/colors';

export default class App extends Component {
  render() {
    const muiTheme = getMuiTheme({
      fontFamily: 'Lato',
      palette: {
        primary1Color: green400,
        accent1Color: green300,
        textColor: darkBlack,
        alternateTextColor: white,
        canvasColor: white,
        borderColor: grey300,
        pickerHeaderColor: green400,
        shadowColor: fullBlack,
      },
    });
    return (
      <MuiThemeProvider muiTheme={getMuiTheme(muiTheme)}>
        <Routes />
      </MuiThemeProvider>
    );
  }
}
